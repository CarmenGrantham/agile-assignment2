﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Stronghold
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RoundLabel = New System.Windows.Forms.Label()
        Me.DetailsGroupBox = New System.Windows.Forms.GroupBox()
        Me.RobotTextBox = New System.Windows.Forms.TextBox()
        Me.RobotLabel = New System.Windows.Forms.Label()
        Me.RoundTextBox = New System.Windows.Forms.TextBox()
        Me.AutonomousGroupBox = New System.Windows.Forms.GroupBox()
        Me.HighGoalCheckbox = New System.Windows.Forms.CheckBox()
        Me.LowGoalCheckbox = New System.Windows.Forms.CheckBox()
        Me.DefenseCrossedCheckbox = New System.Windows.Forms.CheckBox()
        Me.DefenseReachedCheckbox = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TeleopGroupBox = New System.Windows.Forms.GroupBox()
        Me.TeleopHighGoalsCountTextbox = New System.Windows.Forms.TextBox()
        Me.TeleopHighGoalsCountLabel = New System.Windows.Forms.Label()
        Me.ClimbCheckbox = New System.Windows.Forms.CheckBox()
        Me.TeleopHighGoalsCheckbox = New System.Windows.Forms.CheckBox()
        Me.TeleopLowGoalsCheckbox = New System.Windows.Forms.CheckBox()
        Me.PickupCheckbox = New System.Windows.Forms.CheckBox()
        Me.DefenseCheckBox4 = New System.Windows.Forms.CheckBox()
        Me.DefenseCheckBox3 = New System.Windows.Forms.CheckBox()
        Me.DefenseCheckBox2 = New System.Windows.Forms.CheckBox()
        Me.DefenseCheckBox1 = New System.Windows.Forms.CheckBox()
        Me.DefenseCheckBox0 = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TabController = New System.Windows.Forms.TabControl()
        Me.roundTabPage = New System.Windows.Forms.TabPage()
        Me.RoundNextButton = New System.Windows.Forms.Button()
        Me.DefensesGroupBox = New System.Windows.Forms.GroupBox()
        Me.SallyPortCheckbox = New System.Windows.Forms.CheckBox()
        Me.RoughTerrainCheckbox = New System.Windows.Forms.CheckBox()
        Me.RockWallCheckbox = New System.Windows.Forms.CheckBox()
        Me.RampartsCheckbox = New System.Windows.Forms.CheckBox()
        Me.PortcullisCheckbox = New System.Windows.Forms.CheckBox()
        Me.MoatCheckbox = New System.Windows.Forms.CheckBox()
        Me.DrawbridgeCheckbox = New System.Windows.Forms.CheckBox()
        Me.ChevalDeFriseCheckbox = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.autonomousTabPage = New System.Windows.Forms.TabPage()
        Me.AutonomousNextButton = New System.Windows.Forms.Button()
        Me.teleopTabPage = New System.Windows.Forms.TabPage()
        Me.TeleopNextButton = New System.Windows.Forms.Button()
        Me.summaryTabPage = New System.Windows.Forms.TabPage()
        Me.RestartButton = New System.Windows.Forms.Button()
        Me.ReportLabel = New System.Windows.Forms.Label()
        Me.DetailsGroupBox.SuspendLayout()
        Me.AutonomousGroupBox.SuspendLayout()
        Me.TeleopGroupBox.SuspendLayout()
        Me.TabController.SuspendLayout()
        Me.roundTabPage.SuspendLayout()
        Me.DefensesGroupBox.SuspendLayout()
        Me.autonomousTabPage.SuspendLayout()
        Me.teleopTabPage.SuspendLayout()
        Me.summaryTabPage.SuspendLayout()
        Me.SuspendLayout()
        '
        'RoundLabel
        '
        Me.RoundLabel.Location = New System.Drawing.Point(6, 27)
        Me.RoundLabel.Name = "RoundLabel"
        Me.RoundLabel.Size = New System.Drawing.Size(50, 20)
        Me.RoundLabel.TabIndex = 0
        Me.RoundLabel.Text = "Round"
        Me.RoundLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'DetailsGroupBox
        '
        Me.DetailsGroupBox.Controls.Add(Me.RobotTextBox)
        Me.DetailsGroupBox.Controls.Add(Me.RobotLabel)
        Me.DetailsGroupBox.Controls.Add(Me.RoundTextBox)
        Me.DetailsGroupBox.Controls.Add(Me.RoundLabel)
        Me.DetailsGroupBox.Location = New System.Drawing.Point(8, 6)
        Me.DetailsGroupBox.Name = "DetailsGroupBox"
        Me.DetailsGroupBox.Size = New System.Drawing.Size(275, 90)
        Me.DetailsGroupBox.TabIndex = 1
        Me.DetailsGroupBox.TabStop = False
        Me.DetailsGroupBox.Text = "Details"
        '
        'RobotTextBox
        '
        Me.RobotTextBox.Location = New System.Drawing.Point(62, 53)
        Me.RobotTextBox.Name = "RobotTextBox"
        Me.RobotTextBox.Size = New System.Drawing.Size(207, 20)
        Me.RobotTextBox.TabIndex = 2
        '
        'RobotLabel
        '
        Me.RobotLabel.Location = New System.Drawing.Point(6, 53)
        Me.RobotLabel.Name = "RobotLabel"
        Me.RobotLabel.Size = New System.Drawing.Size(50, 20)
        Me.RobotLabel.TabIndex = 2
        Me.RobotLabel.Text = "Robot"
        Me.RobotLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'RoundTextBox
        '
        Me.RoundTextBox.Location = New System.Drawing.Point(62, 27)
        Me.RoundTextBox.Name = "RoundTextBox"
        Me.RoundTextBox.Size = New System.Drawing.Size(207, 20)
        Me.RoundTextBox.TabIndex = 1
        '
        'AutonomousGroupBox
        '
        Me.AutonomousGroupBox.Controls.Add(Me.HighGoalCheckbox)
        Me.AutonomousGroupBox.Controls.Add(Me.LowGoalCheckbox)
        Me.AutonomousGroupBox.Controls.Add(Me.DefenseCrossedCheckbox)
        Me.AutonomousGroupBox.Controls.Add(Me.DefenseReachedCheckbox)
        Me.AutonomousGroupBox.Controls.Add(Me.Label1)
        Me.AutonomousGroupBox.Location = New System.Drawing.Point(8, 6)
        Me.AutonomousGroupBox.Name = "AutonomousGroupBox"
        Me.AutonomousGroupBox.Size = New System.Drawing.Size(275, 122)
        Me.AutonomousGroupBox.TabIndex = 2
        Me.AutonomousGroupBox.TabStop = False
        Me.AutonomousGroupBox.Text = "Autonomous"
        '
        'HighGoalCheckbox
        '
        Me.HighGoalCheckbox.AutoSize = True
        Me.HighGoalCheckbox.Location = New System.Drawing.Point(9, 92)
        Me.HighGoalCheckbox.Name = "HighGoalCheckbox"
        Me.HighGoalCheckbox.Size = New System.Drawing.Size(73, 17)
        Me.HighGoalCheckbox.TabIndex = 4
        Me.HighGoalCheckbox.Text = "High Goal"
        Me.HighGoalCheckbox.UseVisualStyleBackColor = True
        '
        'LowGoalCheckbox
        '
        Me.LowGoalCheckbox.AutoSize = True
        Me.LowGoalCheckbox.Location = New System.Drawing.Point(9, 69)
        Me.LowGoalCheckbox.Name = "LowGoalCheckbox"
        Me.LowGoalCheckbox.Size = New System.Drawing.Size(71, 17)
        Me.LowGoalCheckbox.TabIndex = 3
        Me.LowGoalCheckbox.Text = "Low Goal"
        Me.LowGoalCheckbox.UseVisualStyleBackColor = True
        '
        'DefenseCrossedCheckbox
        '
        Me.DefenseCrossedCheckbox.AutoSize = True
        Me.DefenseCrossedCheckbox.Location = New System.Drawing.Point(9, 46)
        Me.DefenseCrossedCheckbox.Name = "DefenseCrossedCheckbox"
        Me.DefenseCrossedCheckbox.Size = New System.Drawing.Size(107, 17)
        Me.DefenseCrossedCheckbox.TabIndex = 2
        Me.DefenseCrossedCheckbox.Text = "Defense Crossed"
        Me.DefenseCrossedCheckbox.UseVisualStyleBackColor = True
        '
        'DefenseReachedCheckbox
        '
        Me.DefenseReachedCheckbox.AutoSize = True
        Me.DefenseReachedCheckbox.Location = New System.Drawing.Point(9, 23)
        Me.DefenseReachedCheckbox.Name = "DefenseReachedCheckbox"
        Me.DefenseReachedCheckbox.Size = New System.Drawing.Size(113, 17)
        Me.DefenseReachedCheckbox.TabIndex = 1
        Me.DefenseReachedCheckbox.Text = "Defense Reached"
        Me.DefenseReachedCheckbox.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(6, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        '
        'TeleopGroupBox
        '
        Me.TeleopGroupBox.Controls.Add(Me.TeleopHighGoalsCountTextbox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopHighGoalsCountLabel)
        Me.TeleopGroupBox.Controls.Add(Me.ClimbCheckbox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopHighGoalsCheckbox)
        Me.TeleopGroupBox.Controls.Add(Me.TeleopLowGoalsCheckbox)
        Me.TeleopGroupBox.Controls.Add(Me.PickupCheckbox)
        Me.TeleopGroupBox.Controls.Add(Me.DefenseCheckBox4)
        Me.TeleopGroupBox.Controls.Add(Me.DefenseCheckBox3)
        Me.TeleopGroupBox.Controls.Add(Me.DefenseCheckBox2)
        Me.TeleopGroupBox.Controls.Add(Me.DefenseCheckBox1)
        Me.TeleopGroupBox.Controls.Add(Me.DefenseCheckBox0)
        Me.TeleopGroupBox.Controls.Add(Me.Label2)
        Me.TeleopGroupBox.Location = New System.Drawing.Point(8, 6)
        Me.TeleopGroupBox.Name = "TeleopGroupBox"
        Me.TeleopGroupBox.Size = New System.Drawing.Size(275, 303)
        Me.TeleopGroupBox.TabIndex = 3
        Me.TeleopGroupBox.TabStop = False
        Me.TeleopGroupBox.Text = "Operator Control"
        '
        'TeleopHighGoalsCountTextbox
        '
        Me.TeleopHighGoalsCountTextbox.Location = New System.Drawing.Point(60, 233)
        Me.TeleopHighGoalsCountTextbox.Name = "TeleopHighGoalsCountTextbox"
        Me.TeleopHighGoalsCountTextbox.Size = New System.Drawing.Size(49, 20)
        Me.TeleopHighGoalsCountTextbox.TabIndex = 9
        '
        'TeleopHighGoalsCountLabel
        '
        Me.TeleopHighGoalsCountLabel.Location = New System.Drawing.Point(4, 233)
        Me.TeleopHighGoalsCountLabel.Name = "TeleopHighGoalsCountLabel"
        Me.TeleopHighGoalsCountLabel.Size = New System.Drawing.Size(50, 20)
        Me.TeleopHighGoalsCountLabel.TabIndex = 15
        Me.TeleopHighGoalsCountLabel.Text = "Count"
        Me.TeleopHighGoalsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ClimbCheckbox
        '
        Me.ClimbCheckbox.AutoSize = True
        Me.ClimbCheckbox.Location = New System.Drawing.Point(9, 272)
        Me.ClimbCheckbox.Name = "ClimbCheckbox"
        Me.ClimbCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ClimbCheckbox.Size = New System.Drawing.Size(51, 17)
        Me.ClimbCheckbox.TabIndex = 10
        Me.ClimbCheckbox.Text = "Climb"
        Me.ClimbCheckbox.UseVisualStyleBackColor = True
        '
        'TeleopHighGoalsCheckbox
        '
        Me.TeleopHighGoalsCheckbox.AutoSize = True
        Me.TeleopHighGoalsCheckbox.Location = New System.Drawing.Point(10, 210)
        Me.TeleopHighGoalsCheckbox.Name = "TeleopHighGoalsCheckbox"
        Me.TeleopHighGoalsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TeleopHighGoalsCheckbox.Size = New System.Drawing.Size(78, 17)
        Me.TeleopHighGoalsCheckbox.TabIndex = 8
        Me.TeleopHighGoalsCheckbox.Text = "High Goals"
        Me.TeleopHighGoalsCheckbox.UseVisualStyleBackColor = True
        '
        'TeleopLowGoalsCheckbox
        '
        Me.TeleopLowGoalsCheckbox.AutoSize = True
        Me.TeleopLowGoalsCheckbox.Location = New System.Drawing.Point(10, 187)
        Me.TeleopLowGoalsCheckbox.Name = "TeleopLowGoalsCheckbox"
        Me.TeleopLowGoalsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TeleopLowGoalsCheckbox.Size = New System.Drawing.Size(76, 17)
        Me.TeleopLowGoalsCheckbox.TabIndex = 7
        Me.TeleopLowGoalsCheckbox.Text = "Low Goals"
        Me.TeleopLowGoalsCheckbox.UseVisualStyleBackColor = True
        '
        'PickupCheckbox
        '
        Me.PickupCheckbox.AutoSize = True
        Me.PickupCheckbox.Location = New System.Drawing.Point(10, 152)
        Me.PickupCheckbox.Name = "PickupCheckbox"
        Me.PickupCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PickupCheckbox.Size = New System.Drawing.Size(59, 17)
        Me.PickupCheckbox.TabIndex = 6
        Me.PickupCheckbox.Text = "Pickup"
        Me.PickupCheckbox.UseVisualStyleBackColor = True
        '
        'DefenseCheckBox4
        '
        Me.DefenseCheckBox4.Location = New System.Drawing.Point(10, 119)
        Me.DefenseCheckBox4.Name = "DefenseCheckBox4"
        Me.DefenseCheckBox4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DefenseCheckBox4.Size = New System.Drawing.Size(150, 17)
        Me.DefenseCheckBox4.TabIndex = 5
        Me.DefenseCheckBox4.Text = "Defense 4"
        Me.DefenseCheckBox4.UseVisualStyleBackColor = True
        '
        'DefenseCheckBox3
        '
        Me.DefenseCheckBox3.Location = New System.Drawing.Point(10, 96)
        Me.DefenseCheckBox3.Name = "DefenseCheckBox3"
        Me.DefenseCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DefenseCheckBox3.Size = New System.Drawing.Size(150, 17)
        Me.DefenseCheckBox3.TabIndex = 4
        Me.DefenseCheckBox3.Text = "Defense 3"
        Me.DefenseCheckBox3.UseVisualStyleBackColor = True
        '
        'DefenseCheckBox2
        '
        Me.DefenseCheckBox2.Location = New System.Drawing.Point(10, 73)
        Me.DefenseCheckBox2.Name = "DefenseCheckBox2"
        Me.DefenseCheckBox2.Size = New System.Drawing.Size(150, 17)
        Me.DefenseCheckBox2.TabIndex = 3
        Me.DefenseCheckBox2.Text = "Defense 2"
        Me.DefenseCheckBox2.UseVisualStyleBackColor = True
        '
        'DefenseCheckBox1
        '
        Me.DefenseCheckBox1.Location = New System.Drawing.Point(10, 50)
        Me.DefenseCheckBox1.Name = "DefenseCheckBox1"
        Me.DefenseCheckBox1.Size = New System.Drawing.Size(150, 17)
        Me.DefenseCheckBox1.TabIndex = 2
        Me.DefenseCheckBox1.Text = "Defense 1"
        Me.DefenseCheckBox1.UseVisualStyleBackColor = True
        '
        'DefenseCheckBox0
        '
        Me.DefenseCheckBox0.Location = New System.Drawing.Point(10, 27)
        Me.DefenseCheckBox0.Name = "DefenseCheckBox0"
        Me.DefenseCheckBox0.Size = New System.Drawing.Size(150, 17)
        Me.DefenseCheckBox0.TabIndex = 1
        Me.DefenseCheckBox0.Text = "Defense 0"
        Me.DefenseCheckBox0.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(121, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 0
        '
        'TabController
        '
        Me.TabController.Controls.Add(Me.roundTabPage)
        Me.TabController.Controls.Add(Me.autonomousTabPage)
        Me.TabController.Controls.Add(Me.teleopTabPage)
        Me.TabController.Controls.Add(Me.summaryTabPage)
        Me.TabController.Location = New System.Drawing.Point(0, 0)
        Me.TabController.Name = "TabController"
        Me.TabController.SelectedIndex = 0
        Me.TabController.Size = New System.Drawing.Size(300, 413)
        Me.TabController.TabIndex = 5
        '
        'roundTabPage
        '
        Me.roundTabPage.Controls.Add(Me.RoundNextButton)
        Me.roundTabPage.Controls.Add(Me.DefensesGroupBox)
        Me.roundTabPage.Controls.Add(Me.DetailsGroupBox)
        Me.roundTabPage.Location = New System.Drawing.Point(4, 22)
        Me.roundTabPage.Name = "roundTabPage"
        Me.roundTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.roundTabPage.Size = New System.Drawing.Size(292, 387)
        Me.roundTabPage.TabIndex = 0
        Me.roundTabPage.Text = "Round Details"
        Me.roundTabPage.UseVisualStyleBackColor = True
        '
        'RoundNextButton
        '
        Me.RoundNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RoundNextButton.Location = New System.Drawing.Point(206, 353)
        Me.RoundNextButton.Name = "RoundNextButton"
        Me.RoundNextButton.Size = New System.Drawing.Size(75, 23)
        Me.RoundNextButton.TabIndex = 11
        Me.RoundNextButton.Text = "Next"
        Me.RoundNextButton.UseVisualStyleBackColor = True
        '
        'DefensesGroupBox
        '
        Me.DefensesGroupBox.Controls.Add(Me.SallyPortCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.RoughTerrainCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.RockWallCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.RampartsCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.PortcullisCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.MoatCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.DrawbridgeCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.ChevalDeFriseCheckbox)
        Me.DefensesGroupBox.Controls.Add(Me.Label4)
        Me.DefensesGroupBox.Location = New System.Drawing.Point(8, 102)
        Me.DefensesGroupBox.Name = "DefensesGroupBox"
        Me.DefensesGroupBox.Size = New System.Drawing.Size(275, 219)
        Me.DefensesGroupBox.TabIndex = 6
        Me.DefensesGroupBox.TabStop = False
        Me.DefensesGroupBox.Text = "Defenses"
        '
        'SallyPortCheckbox
        '
        Me.SallyPortCheckbox.AutoSize = True
        Me.SallyPortCheckbox.Location = New System.Drawing.Point(10, 189)
        Me.SallyPortCheckbox.Name = "SallyPortCheckbox"
        Me.SallyPortCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.SallyPortCheckbox.Size = New System.Drawing.Size(70, 17)
        Me.SallyPortCheckbox.TabIndex = 10
        Me.SallyPortCheckbox.Text = "Sally Port"
        Me.SallyPortCheckbox.UseVisualStyleBackColor = True
        '
        'RoughTerrainCheckbox
        '
        Me.RoughTerrainCheckbox.AutoSize = True
        Me.RoughTerrainCheckbox.Location = New System.Drawing.Point(10, 166)
        Me.RoughTerrainCheckbox.Name = "RoughTerrainCheckbox"
        Me.RoughTerrainCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RoughTerrainCheckbox.Size = New System.Drawing.Size(94, 17)
        Me.RoughTerrainCheckbox.TabIndex = 9
        Me.RoughTerrainCheckbox.Text = "Rough Terrain"
        Me.RoughTerrainCheckbox.UseVisualStyleBackColor = True
        '
        'RockWallCheckbox
        '
        Me.RockWallCheckbox.AutoSize = True
        Me.RockWallCheckbox.Location = New System.Drawing.Point(10, 143)
        Me.RockWallCheckbox.Name = "RockWallCheckbox"
        Me.RockWallCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RockWallCheckbox.Size = New System.Drawing.Size(76, 17)
        Me.RockWallCheckbox.TabIndex = 8
        Me.RockWallCheckbox.Text = "Rock Wall"
        Me.RockWallCheckbox.UseVisualStyleBackColor = True
        '
        'RampartsCheckbox
        '
        Me.RampartsCheckbox.AutoSize = True
        Me.RampartsCheckbox.Location = New System.Drawing.Point(10, 120)
        Me.RampartsCheckbox.Name = "RampartsCheckbox"
        Me.RampartsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RampartsCheckbox.Size = New System.Drawing.Size(71, 17)
        Me.RampartsCheckbox.TabIndex = 7
        Me.RampartsCheckbox.Text = "Ramparts"
        Me.RampartsCheckbox.UseVisualStyleBackColor = True
        '
        'PortcullisCheckbox
        '
        Me.PortcullisCheckbox.AutoSize = True
        Me.PortcullisCheckbox.Location = New System.Drawing.Point(10, 97)
        Me.PortcullisCheckbox.Name = "PortcullisCheckbox"
        Me.PortcullisCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PortcullisCheckbox.Size = New System.Drawing.Size(68, 17)
        Me.PortcullisCheckbox.TabIndex = 6
        Me.PortcullisCheckbox.Text = "Portcullis"
        Me.PortcullisCheckbox.UseVisualStyleBackColor = True
        '
        'MoatCheckbox
        '
        Me.MoatCheckbox.AutoSize = True
        Me.MoatCheckbox.Location = New System.Drawing.Point(10, 74)
        Me.MoatCheckbox.Name = "MoatCheckbox"
        Me.MoatCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.MoatCheckbox.Size = New System.Drawing.Size(50, 17)
        Me.MoatCheckbox.TabIndex = 5
        Me.MoatCheckbox.Text = "Moat"
        Me.MoatCheckbox.UseVisualStyleBackColor = True
        '
        'DrawbridgeCheckbox
        '
        Me.DrawbridgeCheckbox.AutoSize = True
        Me.DrawbridgeCheckbox.Location = New System.Drawing.Point(10, 50)
        Me.DrawbridgeCheckbox.Name = "DrawbridgeCheckbox"
        Me.DrawbridgeCheckbox.Size = New System.Drawing.Size(80, 17)
        Me.DrawbridgeCheckbox.TabIndex = 4
        Me.DrawbridgeCheckbox.Text = "Drawbridge"
        Me.DrawbridgeCheckbox.UseVisualStyleBackColor = True
        '
        'ChevalDeFriseCheckbox
        '
        Me.ChevalDeFriseCheckbox.AutoSize = True
        Me.ChevalDeFriseCheckbox.Location = New System.Drawing.Point(10, 27)
        Me.ChevalDeFriseCheckbox.Name = "ChevalDeFriseCheckbox"
        Me.ChevalDeFriseCheckbox.Size = New System.Drawing.Size(99, 17)
        Me.ChevalDeFriseCheckbox.TabIndex = 3
        Me.ChevalDeFriseCheckbox.Text = "Cheval de Frise"
        Me.ChevalDeFriseCheckbox.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(121, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 0
        '
        'autonomousTabPage
        '
        Me.autonomousTabPage.Controls.Add(Me.AutonomousNextButton)
        Me.autonomousTabPage.Controls.Add(Me.AutonomousGroupBox)
        Me.autonomousTabPage.Location = New System.Drawing.Point(4, 22)
        Me.autonomousTabPage.Name = "autonomousTabPage"
        Me.autonomousTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.autonomousTabPage.Size = New System.Drawing.Size(292, 387)
        Me.autonomousTabPage.TabIndex = 1
        Me.autonomousTabPage.Text = "Autonomous"
        Me.autonomousTabPage.UseVisualStyleBackColor = True
        '
        'AutonomousNextButton
        '
        Me.AutonomousNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AutonomousNextButton.Location = New System.Drawing.Point(206, 353)
        Me.AutonomousNextButton.Name = "AutonomousNextButton"
        Me.AutonomousNextButton.Size = New System.Drawing.Size(75, 23)
        Me.AutonomousNextButton.TabIndex = 5
        Me.AutonomousNextButton.Text = "Next"
        Me.AutonomousNextButton.UseVisualStyleBackColor = True
        '
        'teleopTabPage
        '
        Me.teleopTabPage.Controls.Add(Me.TeleopNextButton)
        Me.teleopTabPage.Controls.Add(Me.TeleopGroupBox)
        Me.teleopTabPage.Location = New System.Drawing.Point(4, 22)
        Me.teleopTabPage.Name = "teleopTabPage"
        Me.teleopTabPage.Size = New System.Drawing.Size(292, 387)
        Me.teleopTabPage.TabIndex = 2
        Me.teleopTabPage.Text = "Teleop"
        Me.teleopTabPage.UseVisualStyleBackColor = True
        '
        'TeleopNextButton
        '
        Me.TeleopNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.TeleopNextButton.Location = New System.Drawing.Point(206, 353)
        Me.TeleopNextButton.Name = "TeleopNextButton"
        Me.TeleopNextButton.Size = New System.Drawing.Size(75, 23)
        Me.TeleopNextButton.TabIndex = 11
        Me.TeleopNextButton.Text = "Next"
        Me.TeleopNextButton.UseVisualStyleBackColor = True
        '
        'summaryTabPage
        '
        Me.summaryTabPage.Controls.Add(Me.ReportLabel)
        Me.summaryTabPage.Controls.Add(Me.RestartButton)
        Me.summaryTabPage.Location = New System.Drawing.Point(4, 22)
        Me.summaryTabPage.Name = "summaryTabPage"
        Me.summaryTabPage.Size = New System.Drawing.Size(292, 387)
        Me.summaryTabPage.TabIndex = 3
        Me.summaryTabPage.Text = "Summary"
        Me.summaryTabPage.UseVisualStyleBackColor = True
        '
        'RestartButton
        '
        Me.RestartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.RestartButton.Location = New System.Drawing.Point(8, 354)
        Me.RestartButton.Name = "RestartButton"
        Me.RestartButton.Size = New System.Drawing.Size(75, 23)
        Me.RestartButton.TabIndex = 8
        Me.RestartButton.Text = "Restart"
        Me.RestartButton.UseVisualStyleBackColor = True
        '
        'ReportLabel
        '
        Me.ReportLabel.Location = New System.Drawing.Point(12, 15)
        Me.ReportLabel.Name = "ReportLabel"
        Me.ReportLabel.Size = New System.Drawing.Size(266, 330)
        Me.ReportLabel.TabIndex = 9
        Me.ReportLabel.Text = "Label3"
        '
        'Stronghold
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(297, 411)
        Me.Controls.Add(Me.TabController)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "Stronghold"
        Me.Text = "Stronghold"
        Me.DetailsGroupBox.ResumeLayout(False)
        Me.DetailsGroupBox.PerformLayout()
        Me.AutonomousGroupBox.ResumeLayout(False)
        Me.AutonomousGroupBox.PerformLayout()
        Me.TeleopGroupBox.ResumeLayout(False)
        Me.TeleopGroupBox.PerformLayout()
        Me.TabController.ResumeLayout(False)
        Me.roundTabPage.ResumeLayout(False)
        Me.DefensesGroupBox.ResumeLayout(False)
        Me.DefensesGroupBox.PerformLayout()
        Me.autonomousTabPage.ResumeLayout(False)
        Me.teleopTabPage.ResumeLayout(False)
        Me.summaryTabPage.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents RoundLabel As Label
    Friend WithEvents DetailsGroupBox As GroupBox
    Friend WithEvents AutonomousGroupBox As GroupBox
    Friend WithEvents LowGoalCheckbox As CheckBox
    Friend WithEvents DefenseCrossedCheckbox As CheckBox
    Friend WithEvents DefenseReachedCheckbox As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TeleopGroupBox As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents HighGoalCheckbox As CheckBox
    Friend WithEvents RobotTextBox As TextBox
    Friend WithEvents RobotLabel As Label
    Friend WithEvents RoundTextBox As TextBox
    Friend WithEvents ClimbCheckbox As CheckBox
    Friend WithEvents TeleopHighGoalsCheckbox As CheckBox
    Friend WithEvents TeleopLowGoalsCheckbox As CheckBox
    Friend WithEvents PickupCheckbox As CheckBox
    Friend WithEvents DefenseCheckBox4 As CheckBox
    Friend WithEvents DefenseCheckBox3 As CheckBox
    Friend WithEvents DefenseCheckBox2 As CheckBox
    Friend WithEvents DefenseCheckBox1 As CheckBox
    Friend WithEvents DefenseCheckBox0 As CheckBox
    Friend WithEvents TabController As TabControl
    Friend WithEvents roundTabPage As TabPage
    Friend WithEvents DefensesGroupBox As GroupBox
    Friend WithEvents SallyPortCheckbox As CheckBox
    Friend WithEvents RoughTerrainCheckbox As CheckBox
    Friend WithEvents RockWallCheckbox As CheckBox
    Friend WithEvents RampartsCheckbox As CheckBox
    Friend WithEvents PortcullisCheckbox As CheckBox
    Friend WithEvents MoatCheckbox As CheckBox
    Friend WithEvents DrawbridgeCheckbox As CheckBox
    Friend WithEvents ChevalDeFriseCheckbox As CheckBox
    Friend WithEvents Label4 As Label
    Friend WithEvents autonomousTabPage As TabPage
    Friend WithEvents teleopTabPage As TabPage
    Friend WithEvents summaryTabPage As TabPage
    Friend WithEvents TeleopHighGoalsCountTextbox As TextBox
    Friend WithEvents TeleopHighGoalsCountLabel As Label
    Friend WithEvents RoundNextButton As Button
    Friend WithEvents AutonomousNextButton As Button
    Friend WithEvents TeleopNextButton As Button
    Friend WithEvents RestartButton As Button
    Friend WithEvents ReportLabel As Label
End Class
