﻿Public Class Stronghold

#Region "Initialise"

    ' Arrays to hold the defense types and checkboxes
    Private defenseTypes(8) As CheckBox
    Private defensesChosen As ArrayList
    Private defensesUsed(5) As CheckBox

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()

        ' Set up the arrays to hold the defense checkboxes that we can choose
        Me.defenseTypes = New CheckBox() {Me.ChevalDeFriseCheckbox, Me.DrawbridgeCheckbox, Me.MoatCheckbox, Me.PortcullisCheckbox, Me.RampartsCheckbox, Me.RockWallCheckbox, Me.RoughTerrainCheckbox, Me.SallyPortCheckbox}

        ' Arraylist of the names of selected defenses
        Me.defensesChosen = New ArrayList()
        ' The low goal is always present
        Me.defensesChosen.Add("Low Goal")

        ' Set up the arrays to hold the defense checkboxes that have been chosen
        Me.defensesUsed = New CheckBox() {Me.DefenseCheckBox0, Me.DefenseCheckBox1, Me.DefenseCheckBox2, Me.DefenseCheckBox3, Me.DefenseCheckBox4}

        ' Initialise the tabs
        Me.checkEnableTabs()
    End Sub

#End Region

#Region "Tab Management"

    ' Change to the next tab
    Private Sub NextTab_Click(sender As Object, e As EventArgs) Handles RoundNextButton.Click, TeleopNextButton.Click, AutonomousNextButton.Click
        Me.TabController.SelectTab(Me.TabController.SelectedIndex + 1)
    End Sub

    ' Checks to see if the tabs can be displayed, hides them if not.
    Private Sub checkEnableTabs()
        ' Tabs are enabled if we have a robot, round, and four defenses selected.

        ' Last step. If the user has selected four defenses (+ the low goal) we can enable the tabs. If not 
        ' they should be disabled.
        If (Me.defensesChosen.Count = 5 And Not Me.RobotTextBox.Text = "" And Not Me.RoundTextBox.Text = "") Then
            Me.TabController.TabPages.Insert(1, Me.autonomousTabPage)
            Me.TabController.TabPages.Insert(2, Me.teleopTabPage)
            Me.TabController.TabPages.Insert(3, Me.summaryTabPage)
            Me.RoundNextButton.Enabled = True
        Else
            Me.TabController.TabPages.Remove(Me.autonomousTabPage)
            Me.TabController.TabPages.Remove(Me.teleopTabPage)
            Me.TabController.TabPages.Remove(Me.summaryTabPage)
            Me.RoundNextButton.Enabled = False
            Me.TabController.SelectedIndex = 0
        End If
    End Sub

    Private Sub TabController_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabController.SelectedIndexChanged
        ' Time to generate a report? It is if we just entered the last tab.

        If (TabController.SelectedIndex = 3) Then
            Me.generateReport()
        End If
    End Sub

#End Region

#Region "Round and Robot"

    ' Not much here. Just a trigger to display tabs when text and defenses are ready.
    Private Sub RoundTextBox_TextChanged(sender As Object, e As EventArgs) Handles RoundTextBox.TextChanged, RobotTextBox.TextChanged
        Me.checkEnableTabs()
    End Sub

#End Region

#Region "Defenses"

    ' defenseselection_CheckedChanged handles the initial selection of defenses, making the selected defenses avaialble in teleop mode

    Private Sub defenseselection_CheckedChanged(sender As Object, e As EventArgs) Handles ChevalDeFriseCheckbox.CheckedChanged, SallyPortCheckbox.CheckedChanged, RoughTerrainCheckbox.CheckedChanged, RockWallCheckbox.CheckedChanged, RampartsCheckbox.CheckedChanged, PortcullisCheckbox.CheckedChanged, MoatCheckbox.CheckedChanged, DrawbridgeCheckbox.CheckedChanged
        ' Cast the sender to a CheckBox so we can grab the Text property and if it is checked
        Dim changedCheckBox As CheckBox = CType(sender, CheckBox)

        ' Has it just been selected? If so, we need to store it as a selected defense
        ' If not, remove it from the list of defenses
        If (changedCheckBox.Checked) Then
            ' Store the name only if we don't already have it
            If (Not Me.defensesChosen.Contains(changedCheckBox.Text)) Then
                Me.defensesChosen.Add(changedCheckBox.Text)
            End If
        Else
            ' Remove the name if it was deselected
            Me.defensesChosen.Remove(changedCheckBox.Text)
        End If

        ' Update the list of active defenses

        ' Index to keep track of which checkbox to update
        Dim defenseIndex As Integer = 0

        ' Put the defenses in alphabetical order
        Me.defensesChosen.Sort()

        ' Go through each defense, and add it as an option in teleop modde
        For Each defenseName As String In Me.defensesChosen
            ' Name the defense
            defensesUsed(defenseIndex).Text = defenseName
            ' Make sure it isn't selected
            defensesUsed(defenseIndex).Checked = False
            ' Increase the index for the next defense
            defenseIndex += 1
        Next

        ' If five defenses are chosen (the low goal being one of them) we need to allow access to the tab page
        ' and disable defenses which can't be used. If five haven't been chosen, ensure that all defense options
        ' are available
        For Each defenseOption As CheckBox In Me.defenseTypes
            If (Me.defensesChosen.Count = 5) Then
                If (Not defenseOption.Checked) Then
                    defenseOption.Enabled = False
                End If
            Else
                defenseOption.Enabled = True
            End If
        Next

        ' Finally, as we've made changes, check to see if we need the tabs now.
        Me.checkEnableTabs()
    End Sub

#End Region

#Region "Autonomous"

    ' Minor problem. If the defenses were crossed, they must have been reached. If not reached, couldn't have been crossed.
    Private Sub DefenseCrossedCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles DefenseCrossedCheckbox.CheckedChanged
        If (Me.DefenseCrossedCheckbox.Checked) Then
            Me.DefenseReachedCheckbox.Checked = True
        End If
    End Sub

    Private Sub DefenseReachedCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles DefenseReachedCheckbox.CheckedChanged
        If (Not Me.DefenseReachedCheckbox.Checked) Then
            Me.DefenseCrossedCheckbox.Checked = False
        End If
    End Sub

#End Region

#Region "Number of Goals Scored"

    Private Sub TeleopHighGoalsCountTextbox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TeleopHighGoalsCountTextbox.KeyPress

        ' Checking if the key is a number or something like a backspace, tab etc. Only let it through if it is.

        If (Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub TeleopHighGoalsCountTextbox_TextChanged(sender As Object, e As EventArgs) Handles TeleopHighGoalsCountTextbox.TextChanged

        ' If there's a value greater than 0, by definition we can shoot high goal

        If (Not TeleopHighGoalsCountTextbox.Text = "" And Not TeleopHighGoalsCountTextbox.Text = "0") Then
            Me.TeleopHighGoalsCheckbox.Checked = True
        End If

        ' If there's a value of 0, by definition we didn't shoot a high goal

        If (TeleopHighGoalsCountTextbox.Text = "0") Then
            Me.TeleopHighGoalsCheckbox.Checked = False
        End If

        ' Note - I'm allowing for it to be left blank.
    End Sub

    ' To avoid a possible bug, if the number of goals is not blank or 0, high goals needs to be checked

    Private Sub TeleopHighGoalsCheckbox_CheckedChanged(sender As Object, e As EventArgs) Handles TeleopHighGoalsCheckbox.CheckedChanged
        If (TeleopHighGoalsCountTextbox.Text = "0") Then
            Me.TeleopHighGoalsCheckbox.Checked = False
        ElseIf (Not TeleopHighGoalsCountTextbox.Text = "") Then
            Me.TeleopHighGoalsCheckbox.Checked = True
        End If
    End Sub

#End Region

#Region "Restart"

    Private Sub RestartButton_Click(sender As Object, e As EventArgs) Handles RestartButton.Click

        ' Clear the general information

        Me.RoundTextBox.Text = ""
        Me.RobotTextBox.Text = ""

        For Each defenseOption As CheckBox In Me.defenseTypes
            defenseOption.Checked = False
            defenseOption.Enabled = True
        Next

        ' Clear autonomous

        Me.DefenseReachedCheckbox.Checked = False
        Me.DefenseCrossedCheckbox.Checked = False
        Me.LowGoalCheckbox.Checked = False
        Me.HighGoalCheckbox.Checked = False

        ' Clear Teleop

        For Each defenseOption As CheckBox In Me.defensesUsed
            defenseOption.Checked = False
        Next

        Me.TeleopLowGoalsCheckbox.Checked = False
        Me.TeleopHighGoalsCheckbox.Checked = False
        Me.TeleopHighGoalsCountTextbox.Text = ""

        Me.PickupCheckbox.Checked = False
        Me.ClimbCheckbox.Checked = False

        ' Hide tabs
        Me.checkEnableTabs()
    End Sub

#End Region

#Region "Generate Report"

    Private Sub generateReport()
        Dim reportText As String

        ' Basic Details

        reportText = "Round: " & Me.RoundTextBox.Text & vbCrLf
        reportText += "Robot:  " & Me.RobotTextBox.Text & vbCrLf & vbCrLf

        ' Autonomous

        reportText += "Autonomous: "

        Dim autoText As String = ""

        If (Me.DefenseCrossedCheckbox.Checked) Then
            autoText += "Reached and crossed a defence. "
        ElseIf (Me.DefenseReachedCheckbox.Checked And Not Me.DefenseCrossedCheckbox.Checked) Then
            autoText += "Reached a defence but didn't cross. "
        End If

        If (Me.LowGoalCheckbox.Checked) Then
            autoText += "Scored a low goal. "
        End If

        If (Me.HighGoalCheckbox.Checked) Then
            autoText += "Scored a high goal. "
        End If

        If (autoText = "") Then
            autoText = "Did not score"
        End If

        reportText += autoText & vbCrLf & vbCrLf

        ' Teleop defenses

        reportText += "Teleop: "

        Dim teleopText As String = ""

        Dim defensesPassed As ArrayList = New ArrayList()


        For Each defenseOption As CheckBox In Me.defensesUsed
            ' If the checkbox is selected, store the name of the defense that was passed
            If (defenseOption.Checked) Then
                defensesPassed.Add(defenseOption.Text)
            End If
        Next

        ' How many defenses were crossed?

        If (defensesPassed.Count = 0) Then
            teleopText += "No defenses were crossed. "
        ElseIf (defensesPassed.Count = 1) Then
            teleopText += "The " & defensesPassed(0) & " was crossed. "
        Else
            teleopText += "The "
            For index As Integer = 0 To defensesPassed.Count - 2
                teleopText += defensesPassed(index)
                If (index = defensesPassed.Count - 2) Then
                    teleopText += " "
                Else
                    teleopText += ", "
                End If
            Next
            teleopText += "and " & defensesPassed(defensesPassed.Count - 1) & " were crossed. "
        End If

        reportText += teleopText & vbCrLf & vbCrLf

        ' How about the shooting?

        If (Me.PickupCheckbox.Checked) Then
            teleopText = "Can pick up, "
        Else
            teleopText = "Did not pick up, "
        End If

        ' Did they shoot high or low?

        If (Not Me.TeleopLowGoalsCheckbox.Checked And Not Me.TeleopHighGoalsCheckbox.Checked) Then
            teleopText += "did not shoot high or low goals."
        Else
            ' So they shot at least a goal.

            teleopText += "can shoot "

            ' Was it a low goal?

            If (Me.TeleopLowGoalsCheckbox.Checked) Then
                teleopText += "low goals"
            End If

            ' Did they do both?

            If (Me.TeleopLowGoalsCheckbox.Checked And Me.TeleopHighGoalsCheckbox.Checked) Then
                teleopText += " and "
            End If

            ' Did they get high goals?

            If (Me.TeleopHighGoalsCheckbox.Checked) Then
                ' Score is optional. If it was entered, display it. Note that they can't have a 0 and have the checkbox checked
                If (Not Me.TeleopHighGoalsCountTextbox.Text = "") Then
                    teleopText += "(" & Me.TeleopHighGoalsCountTextbox.Text & ") "
                End If
                teleopText += "high goals"
            End If

            teleopText += "."
        End If

        reportText += teleopText & vbCrLf & vbCrLf

        ' Finally, did it climb?

        If (Me.ClimbCheckbox.Checked) Then
            teleopText = "The robot can climb."
        Else
            teleopText = "The robot did not climb."
        End If

        reportText += teleopText & vbCrLf & vbCrLf

        Me.ReportLabel.Text = reportText
    End Sub

#End Region

End Class
