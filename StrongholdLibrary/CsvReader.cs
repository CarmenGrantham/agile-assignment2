﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Reads a CSV file that can be read
    // This is based on some code located at http://www.blackbeltcoder.com/Articles/files/reading-and-writing-csv-files-in-c
    // and modified to my needs.
    public class CsvReader : CsvHandler
    {
        private StreamReader Reader;
        private string currLine;
        private int currPos;

        public CsvReader(string path) { 
            Reader = new StreamReader(path);
        }

        // Reads a row of columns from the current CSV file. Returns false if no
        // more data could be read because the end of the file was reached.
        public bool ReadRow(List<string> columns)
        {
            // Verify required argument
            if (columns == null)
            {
                throw new ArgumentNullException("columns");
            }

            // Read next line from the file
            currLine = Reader.ReadLine();
            currPos = 0;
            // Test for end of file
            if (currLine == null)
            {
                return false;
            }
                
            // Test for empty line
            if (currLine.Length == 0)
            {
                columns.Clear();
                return true;
            }

            // Parse line
            string column;
            int numColumns = 0;
            while (true)
            {
                // Read next column
                column = ReadColumn();
                // Add column to list
                if (numColumns < columns.Count)
                {
                    columns[numColumns] = column;
                } else
                {
                    columns.Add(column);
                }

                numColumns++;

                // Break if we reached the end of the line
                if (currLine == null || currPos == currLine.Length)
                {

                    break;
                }
                // Otherwise skip delimiter
                currPos++;
            }
            // Remove any unused columns from collection
            if (numColumns < columns.Count)
            {
                columns.RemoveRange(numColumns, columns.Count - numColumns);
            }
                
            // Indicate success
            return true;
        }

        // Reads column by reading from the current line until a
        // delimiter is found or the end of the line is reached. On return, the
        // current position points to the delimiter or the end of the current
        // line.
        private string ReadColumn()
        {
            int startPos = currPos;
            currPos = currLine.IndexOf(Delimiter, currPos);
            if (currPos == -1)
            {
                currPos = currLine.Length;
            }
                
            if (currPos > startPos)
            {
                return currLine.Substring(startPos, currPos - startPos);
            }
                
            return String.Empty;
        }


        // Propagate Dispose to StreamReader
        public void Dispose()
        {
            Reader.Dispose();

        }
    }
}
