﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    public abstract class AbstractCsvConverterFrom<T> : ICsvConverterFrom<T>
    {

        public abstract T FromCsv(List<string> csvData);

        // Given the csvData list get the string element at provided index
        public String NextCsvDataString(List<string> csvData, int index)
        {
            if (index < csvData.Count())
            {
                return csvData.ElementAt(index);
            }
            return null;
        }

        // Given the csvData list get the int element at provided index
        public int NextCsvDataInt(List<string> csvData, int index)
        {
            if (index < csvData.Count())
            {
                return Convert.ToInt16(csvData.ElementAt(index));
            }
            return -1;
        }

        // Given the csvData list get the bool element at provided index
        public bool NextCsvDataBool(List<string> csvData, int index)
        {
            return Convert.ToBoolean(csvData.ElementAt(index));
        }
    }
}
