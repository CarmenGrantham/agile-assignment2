﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    public class CsvList : List<String>
    {
        // Add boolean to String list requires conversion
        public void Add(bool value)
        {
            base.Add(Convert.ToString(value));
        }

        // Add int to String list 
        public void Add(int value)
        {
            base.Add(Convert.ToString(value));
        }

        // Add Enum to String list
        public void Add(Enum value)
        {
            base.Add(value.ToString());
        }
            
    }
}
