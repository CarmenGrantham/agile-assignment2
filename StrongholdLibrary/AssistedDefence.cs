﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Robot Round Defences that are crossed with assistance or not, and assistance
    // was provided to other robots.
    public class AssistedDefence : Defence
    {
        public enum Assistance { Unassisted, Assisted, NoAttempt };
        public Assistance AssistanceRequired { get; set; }
        public bool ProvidedAssistance { get; set; }

        public static bool isValidAssistance(String value)
        {
            return Assistance.IsDefined(typeof(Assistance), value);
        }

        public AssistedDefence(String name) : base(name)
        {

        }

        public AssistedDefence(String name, bool autoCrossed, int teleopCrossedCount)
            : base(name, autoCrossed, teleopCrossedCount)
        {
        }

        public AssistedDefence(String name, bool autoCrossed, int teleopCrossedCount,
            Assistance assistanceRequired, bool providedAssistance) : base(name, autoCrossed, teleopCrossedCount)
        {
            this.AssistanceRequired = assistanceRequired;
            this.ProvidedAssistance = providedAssistance;
        }


        public void MarkUnassisted()
        {
            AssistanceRequired = Assistance.Unassisted;
        }

        public void MarkWithAssistance()
        {
            AssistanceRequired = Assistance.Assisted;
        }

        public void MarkNoAttempt()
        {
            AssistanceRequired = Assistance.NoAttempt;
        }

        public override bool ExactMatch(Defence OtherDefence)
        {
            AssistedDefence otherAssistedDefence = (AssistedDefence)OtherDefence;
            return base.ExactMatch(OtherDefence)
                && AssistanceRequired == otherAssistedDefence.AssistanceRequired
                && ProvidedAssistance == otherAssistedDefence.ProvidedAssistance;

        }

        public override string ToString()
        {
            return base.ToString() + ", Assistance Required = " + AssistanceRequired + ", Provided Assistance = " + ProvidedAssistance;
        }

        public override string Summary()
        {
            if (TeleopCrossedCount > 0)
            {
                return Name + " - " + AssistanceRequired;
            }
            else
            {
                return Name + " - Fail";
            }
        }
    
    }
}
