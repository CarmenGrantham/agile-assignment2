﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Use Defence class to create data to go in CSV file
    public class DefenceCsvConverter : ICsvConverterTo<Defence>
    {
        
        public DefenceCsvConverter()
        {
        }

        public CsvList ToCsv(Defence ThisDefence)
        {
            // Add all the fields from Defence class to go into CSV file
            CsvList list = new CsvList();
            list.Add(ThisDefence.Name);
            list.Add(ThisDefence.AutoCrossed);
            list.Add(ThisDefence.TeleopCrossedCount);

            // TODO - Is this the best way of accomplishing this???
            // Add fields specific to AssistedDefence
            if (ThisDefence is AssistedDefence)
            {
                list.Add(((AssistedDefence)ThisDefence).AssistanceRequired);
                list.Add(((AssistedDefence)ThisDefence).ProvidedAssistance);
            }
            return list;
        }

    }
}
