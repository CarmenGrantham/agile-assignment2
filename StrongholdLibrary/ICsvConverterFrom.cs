﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Interface to be use for objects that want to provide method to convert
    // data to an object instance.
    interface ICsvConverterFrom<T>
    {
        T FromCsv(List<string> csvData);
    }
}
