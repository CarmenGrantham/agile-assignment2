﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // A CSV Converter for Robot Round so that data can be saved to a CSV file
    // and then retrieved from a CSV to create a Robot Round instance.
    public class RobotRoundCsvConverter : AbstractCsvConverterFrom<RobotRound>, ICsvConverterTo<RobotRound>
    {
        // CSV converter to use for Defence children.
        private DefenceCsvConverter thisDefenceCsvConverter;
        public RobotRoundCsvConverter()
        {
            thisDefenceCsvConverter = new DefenceCsvConverter();
        }

        // Get the data to use when constructing CSV files
        public CsvList ToCsv(RobotRound thisRobotRound)
        {
            CsvList columns = new CsvList();
            columns.Add(thisRobotRound.RoundNumber);
            columns.Add(thisRobotRound.RobotNumber);

            // Add Autonomous Mode Settings to CSV list
            columns.Add(thisRobotRound.AutoDefenceReached);
            columns.Add(thisRobotRound.AutoLowGoal);
            columns.Add(thisRobotRound.AutoHighGoal);

            // Add Teleop Mode Settings to CSV list
            columns.Add(thisRobotRound.TeleopPickup);
            columns.Add(thisRobotRound.TeleopClimb);
            columns.Add(thisRobotRound.TeleopChallenge);
            columns.Add(thisRobotRound.TeleopLowGoalsMissed);
            columns.Add(thisRobotRound.TeleopLowGoalsScored);
            columns.Add(thisRobotRound.TeleopHighGoalsMissed);
            columns.Add(thisRobotRound.TeleopHighGoalsScored);

            // Add Ratings to CSV List
            columns.Add(thisRobotRound.RatingAutonomous);
            columns.Add(thisRobotRound.RatingCrossing);
            columns.Add(thisRobotRound.RatingScoring);
            columns.Add(thisRobotRound.RatingAssistance);

            // Add Defences to CSV List
            foreach (Defence defence in thisRobotRound.DefencesChosen)
            {
                columns.AddRange(thisDefenceCsvConverter.ToCsv(defence));
            }

            return columns;
        }

        // Convert CSV data to a RobotRound instance
        public override RobotRound FromCsv(List<string> csvData)
        {
            int index = 0;
            RobotRound robotRound = new RobotRound();
            
            robotRound.RoundNumber = NextCsvDataString(csvData, index++);
            robotRound.RobotNumber = NextCsvDataString(csvData, index++);

            // Add Autonomous Mode Settings to CSV list
            robotRound.AutoDefenceReached = NextCsvDataBool(csvData, index++);
            robotRound.AutoLowGoal = NextCsvDataBool(csvData, index++);
            robotRound.AutoHighGoal = NextCsvDataBool(csvData, index++);

            // Add Teleop Mode Settings to CSV list
            robotRound.TeleopPickup = NextCsvDataBool(csvData, index++);
            robotRound.TeleopClimb = NextCsvDataBool(csvData, index++);
            robotRound.TeleopChallenge = NextCsvDataBool(csvData, index++);

            robotRound.TeleopLowGoalsMissed = NextCsvDataInt(csvData, index++);
            robotRound.TeleopLowGoalsScored = NextCsvDataInt(csvData, index++);
            robotRound.TeleopHighGoalsMissed = NextCsvDataInt(csvData, index++);
            robotRound.TeleopHighGoalsScored = NextCsvDataInt(csvData, index++);

            // Add Ratings to CSV List
            robotRound.RatingAutonomous = NextCsvDataInt(csvData, index++);
            robotRound.RatingCrossing = NextCsvDataInt(csvData, index++);
            robotRound.RatingScoring = NextCsvDataInt(csvData, index++);
            robotRound.RatingAssistance = NextCsvDataInt(csvData, index++);

            // Add Defences to CSV List
            while (index < csvData.Count())
            {
                string name = NextCsvDataString(csvData, index++);
                bool autoCrossed = NextCsvDataBool(csvData, index++);
                int teleopCrossedCount = NextCsvDataInt(csvData, index++);

                string next = NextCsvDataString(csvData, index++);
                if (next != null && AssistedDefence.isValidAssistance(next))
                {
                    // Not at the end of the line
                    // Need to determine if it's an AssistedDefence or Defence record
                    // An AssistedDefence will have a value from enum Assistance

                    AssistedDefence.Assistance assistance = (AssistedDefence.Assistance)Enum.Parse(typeof(AssistedDefence.Assistance), next);
                    bool providedAssistance = NextCsvDataBool(csvData, index++);

                    // Create AssistedDefence
                    AssistedDefence assistedDefence = new AssistedDefence(name, autoCrossed, teleopCrossedCount, assistance, providedAssistance);
                    robotRound.addDefence(assistedDefence);
                }
                else
                {
                    // At end of the line or not AssistedDefence (and all the extra fields)

                    // Create Defence
                    Defence defence = new Defence(name, autoCrossed, teleopCrossedCount);
                    robotRound.addDefence(defence);

                    // If not at the end of the list
                    if (index < csvData.Count() - 1)
                    {
                        // Not an AssistedDefence record, but a Defence so back up the index
                        index--;
                    }
                }
            }
            return robotRound;
        }
    }
}
