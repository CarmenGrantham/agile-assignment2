﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Abstract class to store session data for applications.
    public abstract class Session
    {
        public List<RobotRound> AllRobots { get; set; }

        public abstract bool Add(RobotRound robotRound);

        // Add a list of RobotRounds
        public void Add(List<RobotRound> RobotRounds)
        {
            foreach(RobotRound robotRound in RobotRounds) {
                Add(robotRound);
            }
        }
    }
}
