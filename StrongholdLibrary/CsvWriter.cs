﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Writes data to a CSV file
    // This is based on some code located at http://www.blackbeltcoder.com/Articles/files/reading-and-writing-csv-files-in-c
    // and modified to my needs.
    public class CsvWriter : CsvHandler
    {
        private StreamWriter Writer;
        private string oneQuote = null;
        private string twoQuotes = null;
        private string quotedFormat = null;

        public CsvWriter(string path)
        {
            Writer = new StreamWriter(path);
        }


        public bool Write(CsvList columns)
        {

            // Verify required argument
            if (columns == null)
            {
                throw new ArgumentNullException("columns");
            }
                

            // Write each column
            int index = 0;
            foreach (string column in columns)
            {
                // Add delimiter if this isn't the first column
                if (index > 0)
                {
                    Writer.Write(Delimiter);
                }

                // Ensure we're using current quote character
                if (oneQuote == null || oneQuote[0] != Quote)
                {
                    oneQuote = String.Format("{0}", Quote);
                    twoQuotes = String.Format("{0}{0}", Quote);
                    quotedFormat = String.Format("{0}{{0}}{0}", Quote);
                }

                
                if (column != null)
                {
                    // Write this column
                    if (column.IndexOfAny(SpecialChars) == -1)
                    {
                        Writer.Write(column);
                    }
                    else
                    {
                        Writer.Write(quotedFormat, column.Replace(oneQuote, twoQuotes));
                    }
                }
                index++;
                    
            }
            Writer.WriteLine();
            return true;
        }

        // Propagate Dispose to StreamWriter
        public void Dispose()
        {
            Writer.Dispose();
        }

    }

}
