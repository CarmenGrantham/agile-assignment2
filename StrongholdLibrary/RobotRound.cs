﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // A Robot Round, ie data that is entered by students.
    public class RobotRound
    {
        public String RoundNumber { get; set; }
        public String RobotNumber { get; set; }
        // RobotRounds are given a unique ID, this keeps track of which one to assign
        public static int GlobalUniqueID { get; set; }
        // When same Robot Number and Round Number exist with different data
        // keep both RobotRound objects but use version number to indicate differences
        // When averages of RobotRounds are used only use those with version # of 1
        public int VersionNumber { get; set; }
        public List<Defence> DefencesChosen { get; set; }

        // Autonomous Mode elements
        public bool AutoDefenceReached { get; set; }
        public bool AutoLowGoal { get; set; }
        public bool AutoHighGoal { get; set; }

        // Teleop Mode elements
        public bool TeleopPickup { get; set; }
        public bool TeleopClimb { get; set; }
        public bool TeleopChallenge { get; set; }
        public int TeleopLowGoalsMissed { get; set; }
        public int TeleopLowGoalsScored { get; set; }
        public int TeleopHighGoalsMissed { get; set; }
        public int TeleopHighGoalsScored { get; set; }

        // Robot Rating
        public int RatingAutonomous { get; set; }
        public int RatingCrossing { get; set; }
        public int RatingScoring { get; set; }
        public int RatingAssistance { get; set; }

        // Used to indicate excluded robots during reporting.
        public bool Excluded { get; set; }

        // Used to sort robots when 2 robots have same performance.
        public int RobotID { get; set; }

        public RobotRound()
        {
            Excluded = false;
            VersionNumber = 1;
            DefencesChosen = new List<Defence>();
            RobotID = GlobalUniqueID++;
        }

        // Get the sum of all rating values.
        public int getTotalRating()
        {
            return RatingAssistance + RatingAutonomous + RatingCrossing + RatingScoring;
        }

        // Get the number of Teleop crossings.
        public int getTotalCrossings()
        {
            return DefencesChosen[0].TeleopCrossedCount +
                DefencesChosen[1].TeleopCrossedCount +
                DefencesChosen[2].TeleopCrossedCount +
                DefencesChosen[3].TeleopCrossedCount;
        }

        // Add the defence to the RobotRound
        public void addDefence(Defence defence)
        {
            DefencesChosen.Add(defence);
        }

        // Override the toString method to get all the details about the Robot Round
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            String newLine = "\n";

            sb.Append("Round: " + RoundNumber + newLine);
            sb.Append("Robot: " + RobotNumber + newLine + newLine);
            sb.Append("Autonomous Mode" + newLine);
            sb.Append("===============" + newLine);
            sb.Append("Defence Reached: " + AutoDefenceReached + newLine);
            sb.Append("Low Goal: " + AutoLowGoal + newLine);
            sb.Append("High Goal: " + AutoHighGoal + newLine);
            sb.Append("Defences Crossed:" + newLine);
            foreach (Defence defence in DefencesChosen)
            {
                sb.Append("    " + defence.Name + ": " + defence.AutoCrossed + newLine);
            }
            sb.Append(newLine);
            sb.Append("Teleop Mode" + newLine);
            sb.Append("===========" + newLine);
            sb.Append("Pickup: " + TeleopPickup + newLine);
            sb.Append("Climb: " + TeleopClimb + newLine);
            sb.Append("Low Goals Missed: " + TeleopLowGoalsMissed + newLine);
            sb.Append("Low Goals Scored: " + TeleopLowGoalsScored + newLine);
            sb.Append("High Goals Missed: " + TeleopHighGoalsMissed + newLine);
            sb.Append("High Goals Scored: " + TeleopHighGoalsScored + newLine);
            sb.Append("Successful Challenge: " + TeleopChallenge + newLine);
            sb.Append("Defences:" + newLine);

            foreach(Defence defence in DefencesChosen)
            {
                sb.Append("    " + defence.ToString() + newLine);
            }
            sb.Append(newLine);

            // Robot Rating
            sb.Append("Robot Rating" + newLine);
            sb.Append("============" + newLine);
            sb.Append("Autonomous performance: " + RatingAutonomous + newLine);
            sb.Append("Crossing ability: " + RatingCrossing + newLine);
            sb.Append("Scoring ability: " + RatingScoring + newLine);
            sb.Append("Willingness to assist other robots: " + RatingAssistance + newLine);

            return sb.ToString();
        }

        // Get the defence at a particular index position in the list.
        public Defence getDefence(int index)
        {
            if (index < DefencesChosen.Count())
            {
                return DefencesChosen.ElementAt(index);
            }
            return null;
        }

        // Determine if Sally Port defence (if selected) actually provided
        // assistance
        public bool SallyportProvidedAssistance()
        {
            return DefenceProvidedAssistance("Sally Port");
        }

        // Determine if Drawbridge defence (if selected) actually provided
        // assistance
        public bool DrawbridgeProvidedAssistance()
        {
            return DefenceProvidedAssistance("Drawbridge");
        }

        // Determine if Assisted Defence (if selected) actually provided
        // assistance
        private bool DefenceProvidedAssistance(string name)
        {
            Defence defence = (Defence)DefencesChosen.Find(x => x.Name.Contains(name));
            if (defence != null && defence is AssistedDefence)
            {
                return ((AssistedDefence)defence).ProvidedAssistance;
            }
            return false;

        }

        // Determine if RobotRound has Sally Port defence
        public bool hasSallyPort()
        {
            return hasDefence("Sally Port");
        }

        // Determine if RobotRound has Drawbridge defence
        public bool hasDrawbridge()
        {
            return hasDefence("Drawbridge");
        }

        // Determine if a particular defence in RobotRound
        public bool hasDefence(string name)
        {
            return getDefence(name) != null;
        }

        // Get the defence with the given name
        private Defence getDefence(string name)
        {
            return DefencesChosen.Find(x => x.Name.Contains(name));
        }

        // Determine if any of the Autonomous Mode defences were crossed.
        public bool AutoDefenceCrossed()
        {
            foreach (Defence defence in DefencesChosen)
            {
                if (defence.AutoCrossed)
                {
                    return true;
                }
            }
            return false;
        }

        public String RoundAndVersionNumber()
        {
            if (VersionNumber > 1)
            {
                return RoundNumber + "(" + (VersionNumber-1) + ")";
            }
            return RoundNumber;
        }

        // Check if ThisRobotRound has exactly the same fields (except
        // version number) as this object.
        public bool ExactMatch(RobotRound thisRobotRound)
        {
            // Check Robot and Round Number
            if (!RoundNumber.Equals(thisRobotRound.RoundNumber) 
                || !RobotNumber.Equals(thisRobotRound.RobotNumber))
            {
                return false;
            }

            // Check autonomous mode settings
            if (AutoDefenceReached != thisRobotRound.AutoDefenceReached
                || AutoLowGoal != thisRobotRound.AutoLowGoal
                || AutoHighGoal != thisRobotRound.AutoHighGoal)
            {
                return false;
            }

            // Check teleop mode settings
            if (TeleopPickup != thisRobotRound.TeleopPickup
                || TeleopClimb != thisRobotRound.TeleopClimb
                || TeleopChallenge != thisRobotRound.TeleopChallenge
                || TeleopLowGoalsMissed != thisRobotRound.TeleopLowGoalsMissed
                || TeleopLowGoalsScored != thisRobotRound.TeleopLowGoalsScored
                || TeleopHighGoalsMissed != thisRobotRound.TeleopHighGoalsMissed
                || TeleopHighGoalsScored != thisRobotRound.TeleopHighGoalsScored)
            {
                return false;
            }

            // Check ratings settings
            if (RatingAutonomous != thisRobotRound.RatingAutonomous
                || RatingCrossing != thisRobotRound.RatingCrossing
                || RatingScoring != thisRobotRound.RatingScoring
                || RatingAssistance != thisRobotRound.RatingAssistance)
            {
                return false;
            }

            // Check defences in both RobotRounds
            foreach (Defence defence in DefencesChosen)
            {
                Defence otherDefence = thisRobotRound.getDefence(defence.Name);
                if (otherDefence == null)
                {
                    return false;
                }
                else if (!defence.ExactMatch(otherDefence))
                {
                    return false;
                }
            }

            // checked all fields and they all match so return true;
            return true;
        }

        // Check if object is a RobotRound with the same round and robot
        // number as this one.
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (obj is RobotRound)
            {
                RobotRound otherRobotRound = (RobotRound)obj;
                return otherRobotRound.RoundNumber.Equals(RoundNumber) 
                    && otherRobotRound.RobotNumber.Equals(RobotNumber);
            }
            return false;
        }

        // Get the hash code for this object
        public override int GetHashCode()
        {
            return RoundNumber.GetHashCode()
                + 31 * RobotNumber.GetHashCode()
                + 7 * VersionNumber.GetHashCode();
        }
    }
}
