﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // An interface to use by objects that want to be converted to Csv data
    interface ICsvConverterTo<T>
    {
        CsvList ToCsv(T obj);

    }
}
