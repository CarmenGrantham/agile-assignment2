﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Super class for handling conversion to and from CSV
    // This is based on some code located at http://www.blackbeltcoder.com/Articles/files/reading-and-writing-csv-files-in-c
    // and modified to my needs.

    public abstract class CsvHandler
    {
        // These are special characters in CSV files. If a column contains any
        // of these characters, the entire column is wrapped in double quotes.
        protected char[] SpecialChars = new char[] { ',', '"', '\r', '\n' };

        // Indexes into SpecialChars for characters with specific meaning
        private const int DelimiterIndex = 0;
        private const int QuoteIndex = 1;

        // Gets/sets the character used for column delimiters.
        public char Delimiter
        {
            get { return SpecialChars[DelimiterIndex]; }
            set { SpecialChars[DelimiterIndex] = value; }
        }

        // Gets/sets the character used for column quotes.
        public char Quote
        {
            get { return SpecialChars[QuoteIndex]; }
            set { SpecialChars[QuoteIndex] = value; }
        }
    }
}
