﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Store data in Reporting application session.
    public class ReportingSession : Session
    {
        public ReportingSession(){
            AllRobots = new List<RobotRound>();
        }
        
        // Add RobotRound to list, but only if it doesn't already exist
        public override bool Add(RobotRound thisRobotRound)
        {
            if (RobotRoundExists(thisRobotRound))
            {
                // Robot and Round Number exist in list, now check if
                // all fields are exactly the same. If they are don't add.
                if (!RobotRoundExactMatch(thisRobotRound))
                {
                    // This instance of RobotRound differs slightly to all other 
                    // Robot rounds with the same Robot and Round Number.
                    // So, add a version number to identify it.
                    int largestVersionNumber = LargestVersionNumber(thisRobotRound);
                    thisRobotRound.VersionNumber = largestVersionNumber + 1;
                    AllRobots.Add(thisRobotRound);
                    return true;
                }
                // RobotRound not added
                return false;
            } else
            {
                // This Robot and Round Number does not exist in list yet
                // so add it.
                AllRobots.Add(thisRobotRound);
                return true;
            }
        }

        // Check if Robot and Round Number already exists in the RobotRound list.
        private bool RobotRoundExists(RobotRound ThisRobotRound)
        {
            // Are there any other RobotRound's with the same Robot Number 
            // and Round Number?
            return AllRobots.Contains(ThisRobotRound);
            
        }

        // Check if there is a RobotRound that is an exact match with ThisRobotRound
        // in the RobotRound list. ie all fields are exactly the same.
        private bool RobotRoundExactMatch(RobotRound ThisRobotRound)
        {
            foreach (RobotRound robotRound in AllRobots)
            {
                if (robotRound.Equals(ThisRobotRound) && robotRound.ExactMatch(ThisRobotRound))
                {
                    return true;
                }
            }
            return false;
        }

        // Get the largest version number from the list of RobotRounds
        // that have the same robot and round number as ThisRobotRound.
        private int LargestVersionNumber(RobotRound ThisRobotRound)
        {
            int versionNumber = 1;
            foreach(RobotRound robotRound in AllRobots)
            {
                if (robotRound.Equals(ThisRobotRound))
                {
                    if (robotRound.VersionNumber > versionNumber)
                    {
                        versionNumber = robotRound.VersionNumber;
                    }
                }
            }

            return versionNumber;
        }
    }
}
