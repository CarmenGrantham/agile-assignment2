﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Robot Round Defences
    public class Defence
    {
        public String Name { get; set; }
        public bool AutoCrossed { get; set; }
        public int TeleopCrossedCount { get; set; }

        public Defence(String name)
        {
            this.Name = name;
        }

        public Defence(String name, bool autoCrossed, int teleopCrossedCount)
        {
            this.Name = name;
            this.AutoCrossed = autoCrossed;
            this.TeleopCrossedCount = teleopCrossedCount;
        }

        public virtual bool ExactMatch(Defence otherDefence)
        {
            return Name.Equals(otherDefence.Name)
                && AutoCrossed.Equals(otherDefence.AutoCrossed)
                && TeleopCrossedCount.Equals(otherDefence.TeleopCrossedCount);
        }


        public override string ToString()
        {
            return Name + ": Auto Mode Crossed = " + AutoCrossed + ", Teleop Mode Crossed Count = " + TeleopCrossedCount;
        }

        public virtual string Summary()
        {
            if (TeleopCrossedCount > 0)
            {
                return Name + " - " + TeleopCrossedCount;
            } else
            {
                return Name + " - Fail";
            }
        }
    }
}
