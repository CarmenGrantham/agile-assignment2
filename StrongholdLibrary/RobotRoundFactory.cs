﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrongholdLibrary
{
    // Create RobotRound objects
    public class RobotRoundFactory
    {
        
        // Create a list of temporary RobotRound objects for testing purposes.
        public static List<RobotRound> CreateTempRobotRounds()
        { 

            List<RobotRound> allRobots = new List<RobotRound>();
            Defence def1 = new Defence("Low Bar");
            Defence def2 = new Defence("Portcullis");
            Defence def3 = new Defence("Moat");
            Defence def4 = new Defence("Moat");
            Defence def5 = new Defence("Ramparts");
            Defence def6 = new Defence("Rock Wall");
            Defence def7 = new Defence("Rock Wall");
            Defence def8 = new Defence("Ramparts");
            Defence def9 = new Defence("Cheval de Frise");
            Defence def10 = new Defence("Rough Terrain");

            def1.AutoCrossed = false;
            def2.AutoCrossed = false;
            def3.AutoCrossed = false;
            def4.AutoCrossed = true;
            def5.AutoCrossed = true;
            def6.AutoCrossed = true;
            def7.AutoCrossed = false;
            def8.AutoCrossed = false;
            def9.AutoCrossed = false;
            def10.AutoCrossed = false;

            def1.TeleopCrossedCount = 0;
            def2.TeleopCrossedCount = 0;
            def3.TeleopCrossedCount = 1;
            def4.TeleopCrossedCount = 2;
            def5.TeleopCrossedCount = 3;
            def6.TeleopCrossedCount = 3;
            def7.TeleopCrossedCount = 1;
            def8.TeleopCrossedCount = 1;
            def9.TeleopCrossedCount = 1;
            def10.TeleopCrossedCount = 1;

            AssistedDefence asDef1 = new AssistedDefence("Sally Port");
            AssistedDefence asDef2 = new AssistedDefence("Sally Port");
            AssistedDefence asDef3 = new AssistedDefence("Sally Port");
            AssistedDefence asDef4 = new AssistedDefence("Sally Port");
            AssistedDefence asDef5 = new AssistedDefence("Drawbridge");
            AssistedDefence asDef6 = new AssistedDefence("Drawbridge");
            AssistedDefence asDef7 = new AssistedDefence("Drawbridge");
            AssistedDefence asDef8 = new AssistedDefence("Drawbridge");

            asDef1.AssistanceRequired = AssistedDefence.Assistance.NoAttempt;
            asDef2.AssistanceRequired = AssistedDefence.Assistance.Assisted;
            asDef3.AssistanceRequired = AssistedDefence.Assistance.Unassisted;
            asDef4.AssistanceRequired = AssistedDefence.Assistance.Unassisted;
            asDef5.AssistanceRequired = AssistedDefence.Assistance.NoAttempt;
            asDef6.AssistanceRequired = AssistedDefence.Assistance.Assisted;
            asDef7.AssistanceRequired = AssistedDefence.Assistance.Unassisted;
            asDef8.AssistanceRequired = AssistedDefence.Assistance.Unassisted;

            asDef1.ProvidedAssistance = true;
            asDef2.ProvidedAssistance = false;
            asDef3.ProvidedAssistance = true;
            asDef4.ProvidedAssistance = false;
            asDef5.ProvidedAssistance = false;
            asDef6.ProvidedAssistance = true;
            asDef7.ProvidedAssistance = false;
            asDef8.ProvidedAssistance = true;

            RobotRound robo1 = new RobotRound();
            RobotRound robo2 = new RobotRound();
            RobotRound robo3 = new RobotRound();
            RobotRound robo4 = new RobotRound();
            RobotRound robo5 = new RobotRound();
            RobotRound robo6 = new RobotRound();
            RobotRound robo7 = new RobotRound();
            RobotRound robo8 = new RobotRound();
            RobotRound robo9 = new RobotRound();

            allRobots.Add(robo1);
            allRobots.Add(robo2);
            allRobots.Add(robo3);
            allRobots.Add(robo4);
            allRobots.Add(robo5);
            allRobots.Add(robo6);
            allRobots.Add(robo7);
            allRobots.Add(robo8);
            allRobots.Add(robo9);

            robo1.RobotNumber = "1000";
            robo1.RoundNumber = "37";

            robo1.DefencesChosen = new List<Defence>();
            robo1.DefencesChosen.Add(def1);
            robo1.DefencesChosen.Add(asDef2);
            robo1.DefencesChosen.Add(def3);
            robo1.DefencesChosen.Add(asDef6);
            robo1.DefencesChosen.Add(def4);

            robo1.AutoDefenceReached = true;
            robo1.AutoLowGoal = false;
            robo1.AutoHighGoal = false;
            robo1.TeleopPickup = true;
            robo1.TeleopChallenge = false;
            robo1.TeleopClimb = false;

            robo1.TeleopLowGoalsMissed = 2;
            robo1.TeleopLowGoalsScored = 1;
            robo1.TeleopHighGoalsMissed = 1;
            robo1.TeleopHighGoalsScored = 0;

            robo1.RatingAssistance = 2;
            robo1.RatingAutonomous = 2;
            robo1.RatingCrossing = 3;
            robo1.RatingScoring = 3;

            robo2.RobotNumber = "2000";
            robo2.RoundNumber = "60";

            robo2.DefencesChosen = new List<Defence>();
            robo2.DefencesChosen.Add(def1);
            robo2.DefencesChosen.Add(def2);
            robo2.DefencesChosen.Add(asDef6);
            robo2.DefencesChosen.Add(asDef5);
            robo2.DefencesChosen.Add(def9);

            robo2.AutoDefenceReached = true;
            robo2.AutoLowGoal = true;
            robo2.AutoHighGoal = false;
            robo2.TeleopPickup = true;
            robo2.TeleopChallenge = true;
            robo2.TeleopClimb = false;

            robo2.TeleopLowGoalsMissed = 1;
            robo2.TeleopLowGoalsScored = 2;
            robo2.TeleopHighGoalsMissed = 2;
            robo2.TeleopHighGoalsScored = 3;

            robo2.RatingAssistance = 1;
            robo2.RatingAutonomous = 3;
            robo2.RatingCrossing = 2;
            robo2.RatingScoring = 5;

            robo3.RobotNumber = "3000";
            robo3.RoundNumber = "44";

            robo3.DefencesChosen = new List<Defence>();
            robo3.DefencesChosen.Add(asDef6);
            robo3.DefencesChosen.Add(def7);
            robo3.DefencesChosen.Add(def8);
            robo3.DefencesChosen.Add(def1);
            robo3.DefencesChosen.Add(def10);

            robo3.AutoDefenceReached = true;
            robo3.AutoLowGoal = false;
            robo3.AutoHighGoal = false;
            robo3.TeleopPickup = false;
            robo3.TeleopChallenge = true;
            robo3.TeleopClimb = false;

            robo3.TeleopLowGoalsMissed = 0;
            robo3.TeleopLowGoalsScored = 0;
            robo3.TeleopHighGoalsMissed = 0;
            robo3.TeleopHighGoalsScored = 0;

            robo3.RatingAssistance = 4;
            robo3.RatingAutonomous = 3;
            robo3.RatingCrossing = 5;
            robo3.RatingScoring = 1;

            robo4.RobotNumber = "4000";
            robo4.RoundNumber = "48";

            robo4.DefencesChosen = new List<Defence>();
            robo4.DefencesChosen.Add(def1);
            robo4.DefencesChosen.Add(asDef8);
            robo4.DefencesChosen.Add(def2);
            robo4.DefencesChosen.Add(asDef2);
            robo4.DefencesChosen.Add(asDef7);

            robo4.AutoDefenceReached = false;
            robo4.AutoLowGoal = false;
            robo4.AutoHighGoal = false;
            robo4.TeleopPickup = true;
            robo4.TeleopChallenge = false;
            robo4.TeleopClimb = false;

            robo4.TeleopLowGoalsMissed = 3;
            robo4.TeleopLowGoalsScored = 1;
            robo4.TeleopHighGoalsMissed = 0;
            robo4.TeleopHighGoalsScored = 0;

            robo4.RatingAssistance = 1;
            robo4.RatingAutonomous = 2;
            robo4.RatingCrossing = 4;
            robo4.RatingScoring = 3;

            robo5.RobotNumber = "5000";
            robo5.RoundNumber = "12";

            robo5.DefencesChosen = new List<Defence>();
            robo5.DefencesChosen.Add(def1);
            robo5.DefencesChosen.Add(asDef6);
            robo5.DefencesChosen.Add(def2);
            robo5.DefencesChosen.Add(def9);
            robo5.DefencesChosen.Add(asDef8);

            robo5.AutoDefenceReached = false;
            robo5.AutoLowGoal = false;
            robo5.AutoHighGoal = false;
            robo5.TeleopPickup = true;
            robo5.TeleopChallenge = false;
            robo5.TeleopClimb = false;

            robo5.TeleopLowGoalsMissed = 3;
            robo5.TeleopLowGoalsScored = 1;
            robo5.TeleopHighGoalsMissed = 0;
            robo5.TeleopHighGoalsScored = 0;

            robo5.RatingAssistance = 1;
            robo5.RatingAutonomous = 2;
            robo5.RatingCrossing = 4;
            robo5.RatingScoring = 3;

            robo6.RobotNumber = "6000";
            robo6.RoundNumber = "18";

            robo6.DefencesChosen = new List<Defence>();
            robo6.DefencesChosen.Add(def1);
            robo6.DefencesChosen.Add(def2);
            robo6.DefencesChosen.Add(asDef6);
            robo6.DefencesChosen.Add(def9);
            robo6.DefencesChosen.Add(asDef2);

            robo6.AutoDefenceReached = false;
            robo6.AutoLowGoal = false;
            robo6.AutoHighGoal = false;
            robo6.TeleopPickup = true;
            robo6.TeleopChallenge = false;
            robo6.TeleopClimb = false;

            robo6.TeleopLowGoalsMissed = 2;
            robo6.TeleopLowGoalsScored = 0;
            robo6.TeleopHighGoalsMissed = 0;
            robo6.TeleopHighGoalsScored = 0;

            robo6.RatingAssistance = 1;
            robo6.RatingAutonomous = 1;
            robo6.RatingCrossing = 2;
            robo6.RatingScoring = 2;

            robo7.RobotNumber = "7000";
            robo7.RoundNumber = "3";

            robo7.DefencesChosen = new List<Defence>();
            robo7.DefencesChosen.Add(def1);
            robo7.DefencesChosen.Add(def7);
            robo7.DefencesChosen.Add(def8);
            robo7.DefencesChosen.Add(def9);
            robo7.DefencesChosen.Add(def10);

            robo7.AutoDefenceReached = true;
            robo7.AutoLowGoal = true;
            robo7.AutoHighGoal = false;
            robo7.TeleopPickup = true;
            robo7.TeleopChallenge = true;
            robo7.TeleopClimb = true;

            robo7.TeleopLowGoalsMissed = 2;
            robo7.TeleopLowGoalsScored = 2;
            robo7.TeleopHighGoalsMissed = 0;
            robo7.TeleopHighGoalsScored = 0;

            robo7.RatingAssistance = 3;
            robo7.RatingAutonomous = 4;
            robo7.RatingCrossing = 3;
            robo7.RatingScoring = 3;

            robo8.RobotNumber = "4000";
            robo8.RoundNumber = "48";

            robo8.DefencesChosen = new List<Defence>();
            robo8.DefencesChosen.Add(def1);
            robo8.DefencesChosen.Add(def2);
            robo8.DefencesChosen.Add(asDef6);
            robo8.DefencesChosen.Add(def8);
            robo8.DefencesChosen.Add(def5);

            robo8.AutoDefenceReached = false;
            robo8.AutoLowGoal = false;
            robo8.AutoHighGoal = false;
            robo8.TeleopPickup = true;
            robo8.TeleopChallenge = true;
            robo8.TeleopClimb = false;

            robo8.TeleopLowGoalsMissed = 2;
            robo8.TeleopLowGoalsScored = 7;
            robo8.TeleopHighGoalsMissed = 3;
            robo8.TeleopHighGoalsScored = 2;

            robo8.RatingAssistance = 4;
            robo8.RatingAutonomous = 4;
            robo8.RatingCrossing = 2;
            robo8.RatingScoring = 5;

            robo9.RobotNumber = "4000";
            robo9.RoundNumber = "70";

            robo9.DefencesChosen = new List<Defence>();
            robo9.DefencesChosen.Add(def1);
            robo9.DefencesChosen.Add(def3);
            robo9.DefencesChosen.Add(def2);
            robo9.DefencesChosen.Add(asDef7);
            robo9.DefencesChosen.Add(def4);

            robo9.AutoDefenceReached = true;
            robo9.AutoLowGoal = true;
            robo9.AutoHighGoal = false;
            robo9.TeleopPickup = true;
            robo9.TeleopChallenge = false;
            robo9.TeleopClimb = true;

            robo9.TeleopLowGoalsMissed = 1;
            robo9.TeleopLowGoalsScored = 1;
            robo9.TeleopHighGoalsMissed = 1;
            robo9.TeleopHighGoalsScored = 1;

            robo9.RatingAssistance = 3;
            robo9.RatingAutonomous = 3;
            robo9.RatingCrossing = 2;
            robo9.RatingScoring = 1;

            return allRobots;
        }
    }
}
