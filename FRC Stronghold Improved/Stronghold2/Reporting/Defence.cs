﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting
{
    class Defence
    {
        public String Name { get; set; }
        public bool AutoCrossed { get; set; }
        public int TeleopCrossedCount { get; set; }

        public Defence(String name)
        {
            this.Name = name;
        }


        public override string ToString()
        {
            return Name + ": Auto Mode Crossed = " + AutoCrossed + ", Teleop Mode Crossed Count = " + TeleopCrossedCount;
        }
    }
}
