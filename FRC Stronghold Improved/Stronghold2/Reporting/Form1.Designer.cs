﻿namespace Reporting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Summary = new System.Windows.Forms.TabPage();
            this.Scoring = new System.Windows.Forms.TabPage();
            this.Crossing = new System.Windows.Forms.TabPage();
            this.Autonomous = new System.Windows.Forms.TabPage();
            this.Assistance = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Summary);
            this.tabControl1.Controls.Add(this.Scoring);
            this.tabControl1.Controls.Add(this.Crossing);
            this.tabControl1.Controls.Add(this.Autonomous);
            this.tabControl1.Controls.Add(this.Assistance);
            this.tabControl1.Location = new System.Drawing.Point(43, 69);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1020, 520);
            this.tabControl1.TabIndex = 0;
            // 
            // Summary
            // 
            this.Summary.Location = new System.Drawing.Point(4, 22);
            this.Summary.Name = "Summary";
            this.Summary.Padding = new System.Windows.Forms.Padding(3);
            this.Summary.Size = new System.Drawing.Size(1012, 494);
            this.Summary.TabIndex = 0;
            this.Summary.Text = "Summary";
            this.Summary.ToolTipText = "Sorts by Overall Performance";
            this.Summary.UseVisualStyleBackColor = true;
            // 
            // Scoring
            // 
            this.Scoring.Location = new System.Drawing.Point(4, 22);
            this.Scoring.Name = "Scoring";
            this.Scoring.Padding = new System.Windows.Forms.Padding(3);
            this.Scoring.Size = new System.Drawing.Size(1012, 494);
            this.Scoring.TabIndex = 1;
            this.Scoring.Text = "Scoring";
            this.Scoring.ToolTipText = "Sorts by Scoring Rating";
            this.Scoring.UseVisualStyleBackColor = true;
            // 
            // Crossing
            // 
            this.Crossing.Location = new System.Drawing.Point(4, 22);
            this.Crossing.Name = "Crossing";
            this.Crossing.Size = new System.Drawing.Size(1012, 494);
            this.Crossing.TabIndex = 2;
            this.Crossing.Text = "Crossing";
            this.Crossing.ToolTipText = "Sorts by Defence Crossing Rating";
            this.Crossing.UseVisualStyleBackColor = true;
            // 
            // Autonomous
            // 
            this.Autonomous.Location = new System.Drawing.Point(4, 22);
            this.Autonomous.Name = "Autonomous";
            this.Autonomous.Size = new System.Drawing.Size(1012, 494);
            this.Autonomous.TabIndex = 3;
            this.Autonomous.Text = "Autonomous";
            this.Autonomous.ToolTipText = "Sorts by Autonomous Rating";
            this.Autonomous.UseVisualStyleBackColor = true;
            // 
            // Assistance
            // 
            this.Assistance.Location = new System.Drawing.Point(4, 22);
            this.Assistance.Name = "Assistance";
            this.Assistance.Size = new System.Drawing.Size(1012, 494);
            this.Assistance.TabIndex = 4;
            this.Assistance.Text = "Assistance";
            this.Assistance.ToolTipText = "Sorts by Assistance Rating";
            this.Assistance.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Reporting";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LimeGreen;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button1.FlatAppearance.BorderSize = 3;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Green;
            this.button1.Location = new System.Drawing.Point(932, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Import";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1086, 601);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Reporting";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Summary;
        private System.Windows.Forms.TabPage Scoring;
        private System.Windows.Forms.TabPage Crossing;
        private System.Windows.Forms.TabPage Autonomous;
        private System.Windows.Forms.TabPage Assistance;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}

