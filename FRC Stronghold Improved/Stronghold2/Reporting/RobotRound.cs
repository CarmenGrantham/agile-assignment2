﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting
{
    class RobotRound
    {
        private String roundNumber;
        private String robotNumber;
        private List<Defence> defencesChosen = new List<Defence>();

        // Autonomous Mode elements
        private bool autoDefenceReached;
        private bool autoDefenceCrossed;
        private bool autoLowGoal;
        private bool autoHighGoal;

        // Teleop Mode elements
        private bool teleopPickup;
        private bool teleopClimb;
        private bool teleopChallenge;
        private int teleopLowGoalsMissed;
        private int teleopLowGoalsScored;
        private int teleopHighGoalsMissed;
        private int teleopHighGoalsScored;

        // Robot Rating
        private int ratingAutonomous;
        private int ratingCrossing;
        private int ratingScoring;
        private int ratingAssistance;

        public string RoundNumber
        {
            get
            {
                return roundNumber;
            }

            set
            {
                roundNumber = value;
            }
        }

        public string RobotNumber
        {
            get
            {
                return robotNumber;
            }

            set
            {
                robotNumber = value;
            }
        }

        public List<Defence> DefencesChosen
        {
            get
            {
                return defencesChosen;
            }

            set
            {
                defencesChosen = value;
            }
        }

        public bool AutoDefenceReached
        {
            get
            {
                return autoDefenceReached;
            }

            set
            {
                autoDefenceReached = value;
            }
        }

        public bool AutoDefenceCrossed
        {
            get
            {
                return autoDefenceCrossed;
            }

            set
            {
                autoDefenceCrossed = value;
            }
        }

        public bool AutoLowGoal
        {
            get
            {
                return autoLowGoal;
            }

            set
            {
                autoLowGoal = value;
            }
        }

        public bool AutoHighGoal
        {
            get
            {
                return autoHighGoal;
            }

            set
            {
                autoHighGoal = value;
            }
        }

        public bool TeleopPickup
        {
            get
            {
                return teleopPickup;
            }

            set
            {
                teleopPickup = value;
            }
        }

        public bool TeleopClimb
        {
            get
            {
                return teleopClimb;
            }

            set
            {
                teleopClimb = value;
            }
        }

        public bool TeleopChallenge
        {
            get
            {
                return teleopChallenge;
            }

            set
            {
                teleopChallenge = value;
            }
        }

        public int TeleopLowGoalsMissed
        {
            get
            {
                return teleopLowGoalsMissed;
            }

            set
            {
                teleopLowGoalsMissed = value;
            }
        }

        public int TeleopLowGoalsScored
        {
            get
            {
                return teleopLowGoalsScored;
            }

            set
            {
                teleopLowGoalsScored = value;
            }
        }

        public int TeleopHighGoalsMissed
        {
            get
            {
                return teleopHighGoalsMissed;
            }

            set
            {
                teleopHighGoalsMissed = value;
            }
        }

        public int TeleopHighGoalsScored
        {
            get
            {
                return teleopHighGoalsScored;
            }

            set
            {
                teleopHighGoalsScored = value;
            }
        }


        public int RatingAutonomous
        {
            get
            {
                return ratingAutonomous;
            }

            set
            {
                ratingAutonomous = value;
            }
        }

        public int RatingCrossing
        {
            get
            {
                return ratingCrossing;
            }

            set
            {
                ratingCrossing = value;
            }
        }

        public int RatingScoring
        {
            get
            {
                return ratingScoring;
            }

            set
            {
                ratingScoring = value;
            }
        }

        public int RatingAssistance
        {
            get
            {
                return ratingAssistance;
            }

            set
            {
                ratingAssistance = value;
            }
        }

        public void addDefence(Defence defence)
        {
            DefencesChosen.Add(defence);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            String NewLine = "\n";

            sb.Append("Round: " + RoundNumber + NewLine);
            sb.Append("Robot: " + RobotNumber + NewLine + NewLine);
            sb.Append("Autonomous Mode" + NewLine);
            sb.Append("===============" + NewLine);
            sb.Append("Defence Reached: " + AutoDefenceReached + NewLine);
            sb.Append("Defence Crossed: " + AutoDefenceCrossed + NewLine);
            sb.Append("Low Goal: " + AutoLowGoal + NewLine);
            sb.Append("High Goal: " + AutoHighGoal + NewLine);
            sb.Append("Defences Crossed:" + NewLine);
            foreach (Defence defence in DefencesChosen)
            {
                sb.Append("    " + defence.Name + ": " + defence.AutoCrossed + NewLine);
            }
            sb.Append(NewLine);
            sb.Append("Teleop Mode" + NewLine);
            sb.Append("===========" + NewLine);
            sb.Append("Pickup: " + TeleopPickup + NewLine);
            sb.Append("Climb: " + TeleopClimb + NewLine);
            sb.Append("Low Goals Missed: " + TeleopLowGoalsMissed + NewLine);
            sb.Append("Low Goals Scored: " + TeleopLowGoalsScored + NewLine);
            sb.Append("High Goals Missed: " + TeleopHighGoalsMissed + NewLine);
            sb.Append("High Goals Scored: " + TeleopHighGoalsScored + NewLine);
            sb.Append("Successful Challenge: " + TeleopChallenge + NewLine);
            sb.Append("Defences:" + NewLine);

            foreach(Defence defence in DefencesChosen)
            {
                sb.Append("    " + defence.ToString() + NewLine);
            }
            sb.Append(NewLine);

            // Robot Rating
            sb.Append("Robot Rating" + NewLine);
            sb.Append("============" + NewLine);
            sb.Append("Autonomous performance: " + RatingAutonomous + NewLine);
            sb.Append("Crossing ability: " + RatingCrossing + NewLine);
            sb.Append("Scoring ability: " + RatingScoring + NewLine);
            sb.Append("Willingness to assist other robots: " + RatingAssistance + NewLine);




            return sb.ToString();
        }
    }
}
