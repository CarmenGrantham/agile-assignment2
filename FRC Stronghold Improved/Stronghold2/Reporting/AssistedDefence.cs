﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting
{
    class AssistedDefence : Defence
    {
        public enum Assistance { Unassisted, Assisted, NoAttempt };
        public Assistance AssistanceRequired { get; set; }
        public bool ProvidedAssistance { get; set; }

        public AssistedDefence(String name) : base(name)
        {

        }

        public void MarkUnassisted()
        {
            AssistanceRequired = Assistance.Unassisted;
        }

        public void MarkWithAssistance()
        {
            AssistanceRequired = Assistance.Assisted;
        }

        public void MarkNoAttempt()
        {
            AssistanceRequired = Assistance.NoAttempt;
        }

        public override string ToString()
        {
            return base.ToString() + ", Assistance Required = " + AssistanceRequired + ", Provided Assistance = " + ProvidedAssistance;
        }

    }
}
