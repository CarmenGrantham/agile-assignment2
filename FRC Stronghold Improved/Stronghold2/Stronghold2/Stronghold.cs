﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StrongholdLibrary;
using System.IO;

namespace Stronghold2
{
    public partial class Stronghold : Form
    {
        #region Initalise

        private List<RobotRound> robotRounds = new List<RobotRound>();

        // Arrays to hold the defense types and checkboxes

        // List of defences on Round details tab
        private CheckBox[] defenceTypes;
        // List of defences selected on Round details
        private List<String> defencesChosen;
        // List of defences crossed checkboxes on the Autonomous tab
        private CheckBox[] autoDefencesUsed;
        // List of defences crossed labels on the Teleop tab 
        private Label[] teleopDefenceLabels;
        // List of defences crossed numeric up-down on the Teleop tab
        private NumericUpDown[] teleopDefenceCrossed;
        // List of provided assistance checkboxes on the Teleop tab
        private CheckBox[] teleopAssistanceCheckbox;
        // List of assistance comboboxes on Teleop tab 
        private ComboBox[] teleopAssistanceCombobox;

        // Need to lock down defence selection on Details page
        private bool lockDefenceSelection = false;

        public Stronghold()
        {
            InitializeComponent();

            // Set up the arrays to hold the defense checkboxes that we can choose
            defenceTypes = new CheckBox[] {
                this.ChevalDeFriseCheckbox,
                this.DrawbridgeCheckbox,
                this.MoatCheckbox,
                this.PortcullisCheckbox,
                this.RampartsCheckbox,
                this.RockWallCheckbox,
                this.RoughTerrainCheckbox,
                this.SallyPortCheckbox };

            // Set up the arrays to hold the defense checkboxes that have been chosen

            this.autoDefencesUsed = new CheckBox[]
            {
                this.AutoDefenceCheckBox0,
                this.AutoDefenceCheckBox1,
                this.AutoDefenceCheckBox2,
                this.AutoDefenceCheckBox3,
                this.AutoDefenceCheckBox4
            };

            this.teleopDefenceLabels = new Label[]
            {
                this.TeleopDefenceLabel0,
                this.TeleopDefenceLabel1,
                this.TeleopDefenceLabel2,
                this.TeleopDefenceLabel3,
                this.TeleopDefenceLabel4
            };

            this.teleopDefenceCrossed = new NumericUpDown[]
            {
                this.TeleopDefenceNumericUpDown0,
                this.TeleopDefenceNumericUpDown1,
                this.TeleopDefenceNumericUpDown2,
                this.TeleopDefenceNumericUpDown3,
                this.TeleopDefenceNumericUpDown4
            };


            this.teleopAssistanceCheckbox = new CheckBox[]
            {
                this.teleopAssistanceCheckbox0,
                this.teleopAssistanceCheckbox1,
                this.teleopAssistanceCheckbox2,
                this.teleopAssistanceCheckbox3,
                this.teleopAssistanceCheckbox4
            };

            this.teleopAssistanceCombobox = new ComboBox[]
            {
                this.teleopDefenceCombo0,
                this.teleopDefenceCombo1,
                this.teleopDefenceCombo2,
                this.teleopDefenceCombo3,
                this.teleopDefenceCombo4
            };
            
            ResetRound();

            // Hide Saved Rounds Label. Only show when robotRounds is populated
            this.SavedRoundsLabel.Visible = false;
            this.SavedRoundsCountLabel.Visible = false;
        }
        #endregion

        #region Tab Management

        // Checks to see if the tabs can be displayed, hides them if not.
        private void NextTab_Click(object sender, EventArgs e)
        {
            TabController.SelectedIndex = this.TabController.SelectedIndex + 1;
        }

        // Checks to see if the tabs can be displayed, hides them if not.
        private void CheckEnableTabs()
        {
            // Tabs are enabled if we have a robot, round, and four defenses selected.

            // Last step. If the user has selected four defenses (+ the low goal) we can enable the tabs. If not 
            // they should be disabled.
            if (this.defencesChosen.Count() == 5 && !RobotTextBox.Text.Equals("") && !RoundTextBox.Text.Equals(""))
            {
                // When method is called multiple times do not want additional tabs to be re-added.
                bool tabsAlreadyAdded = this.TabController.TabPages.Count > 1;
                if (!tabsAlreadyAdded)
                {
                    this.TabController.TabPages.Insert(1, this.AutonomousTabPage);
                    this.TabController.TabPages.Insert(2, this.TeleopTabPage);
                    this.TabController.TabPages.Insert(3, this.RatingTabPage);
                    this.TabController.TabPages.Insert(4, this.SummaryTabPage);
                }
                this.RoundNextButton.Enabled = true;
            }
            else
            {
                this.TabController.TabPages.Remove(this.AutonomousTabPage);
                this.TabController.TabPages.Remove(this.TeleopTabPage);
                this.TabController.TabPages.Remove(this.RatingTabPage);
                this.TabController.TabPages.Remove(this.SummaryTabPage);
                this.RoundNextButton.Enabled = false;
                this.TabController.SelectedIndex = 0;
            }
        }

        private void TabController_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Time to generate a report? It is if we just entered the last tab.
            if (TabController.SelectedIndex == 4)
            {
                this.GenerateReport();
            }

            // When changing tabs for first time from index 0 lock defences on Details tab
            // so that they cannot be changed and potentially cause problems on other tabs.
            if (TabController.SelectedIndex > 0)
            {
                if (!this.lockDefenceSelection)
                {
                    this.DefensesGroupBox.Enabled = false;
                }
                this.lockDefenceSelection = true;
            }
        }

        #endregion

        #region Round and Robot

        // Not much here.Just a trigger to display tabs when text and defenses are ready.
        private void RoundTextBox_TextChanged(object sender, EventArgs e)
        {
            this.CheckEnableTabs();
        }

        #endregion

        #region Defenses

        //  defenceselection_CheckedChanged handles the initial selection of defenses, making the selected defences avaialble in teleop mode
        private void DefenceSelection_CheckedChanged(object sender, EventArgs e)
        {
            // Cast the sender to a CheckBox so we can grab the Text property and if it is checked
            CheckBox changedCheckbox = (CheckBox)sender;

            // Has it just been selected? If so, we need to store it as a selected defense
            // If not, remove it from the list of defenses
            if (changedCheckbox.Checked)
            {
                // Store the name only if we don't already have it
                if (!this.defencesChosen.Contains(changedCheckbox.Text))
                {
                    this.defencesChosen.Add(changedCheckbox.Text);
                }
            }
            else
            {
                // Remove the name if it was deselected
                defencesChosen.Remove(changedCheckbox.Text);
            }

            // Update the list of active defences

            // Index to keep track of which checkbox to update
            int defenceIndex = 0;

            // Put the defenses in alphabetical order
            this.defencesChosen.Sort();

            // Go through each defense, and add it as an option in teleop modde
            foreach (String defenceName in this.defencesChosen)
            {
                // Name the defences on Autonomous
                this.autoDefencesUsed[defenceIndex].Text = defenceName;
                this.autoDefencesUsed[defenceIndex].Checked = false;

                // If defence is a sallyport or drawbridge, make checkbox and combobox visible.

                bool showAssistance = defenceName == "Sally Port" || defenceName == "Drawbridge";
                this.teleopAssistanceCheckbox[defenceIndex].Visible = showAssistance;
                this.teleopAssistanceCombobox[defenceIndex].Visible = showAssistance;

                // Name the defence labels on Teleop
                this.teleopDefenceLabels[defenceIndex].Text = defenceName;

                // Increase the index for the next defence
                defenceIndex++;
            }

            // If five defenses are chosen (the low goal being one of them) we need to allow access to the tab page
            // and disable defenses which can't be used.If five haven't been chosen, ensure that all defense options
            // are available
            foreach (CheckBox defenceOption in this.defenceTypes)
            {
                if (defencesChosen.Count == 5)
                {
                    if (!defenceOption.Checked)
                    {
                        defenceOption.Enabled = false;
                    }
                }
                else
                {
                    defenceOption.Enabled = true;
                }
            }

            // Finally, as we've made changes, check to see if we need the tabs now
            this.CheckEnableTabs();
        }

        private void TeleopDefenceNumericUpDown0_ValueChanged(object sender, EventArgs e)
        {
            UpdateAssistedComboBox(this.TeleopDefenceNumericUpDown0, this.teleopDefenceCombo0);
        }

        private void TeleopDefenceNumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            UpdateAssistedComboBox(this.TeleopDefenceNumericUpDown1, this.teleopDefenceCombo1);
        }

        private void TeleopDefenceNumericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            UpdateAssistedComboBox(this.TeleopDefenceNumericUpDown2, this.teleopDefenceCombo2);
        }

        private void TeleopDefenceNumericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            UpdateAssistedComboBox(this.TeleopDefenceNumericUpDown3, this.teleopDefenceCombo3);
        }

        private void TeleopDefenceNumericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            UpdateAssistedComboBox(this.TeleopDefenceNumericUpDown4, this.teleopDefenceCombo4);
        }

        private void UpdateAssistedComboBox(NumericUpDown assistedUpDown, ComboBox assistedComboBox)
        {
            if (assistedComboBox.Visible)
            {
                if (assistedUpDown.Value <= 0)
                {
                    ResetAssistedComboBox(assistedComboBox);
                }
                else
                {
                    assistedComboBox.Enabled = true;
                    assistedComboBox.Text = "Unassisted";
                    assistedComboBox.Items.Clear();
                    assistedComboBox.Items.Add("Unassisted");
                    assistedComboBox.Items.Add("With Assistance");
                }
            }
        }
        #endregion

        #region Autonomous

        // Minor problem. If the defenses were crossed, they must have been reached. If not reached, couldn't have been crossed.
        private void DefenseCrossedCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            // If any of the defences crossed are checked then it must have been reached
            if (sender is CheckBox && ((CheckBox)sender).Checked)
            {
                this.DefenseReachedCheckbox.Checked = true;
            }
        }

        private void DefenseReachedCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            // If no defences have been reached then none of them could have been crossed
            if (!this.DefenseReachedCheckbox.Checked)

            {
                foreach (CheckBox defenceCheckbox in autoDefencesUsed)
                {
                    defenceCheckbox.Checked = false;
                }
            }
        }

        #endregion

        #region Reset

        private void ResetButton_Click(object sender, EventArgs e)
        {
            // Check user wants to go ahead and reset data
            String message = "Reset deletes all of your data for this Round and Robot. Do you want to continue?";
            String caption = "Reset Confirmation";
            if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                ResetRound();
            }
        }

        private void ResetRound()
        {
            // Clear elements in Round Details tab
            Reset(this.RoundTextBox);
            Reset(this.RobotTextBox);
            Reset(this.defenceTypes);
            Enable(this.defenceTypes, true);
            ResetDefencesChosen();

            this.DefensesGroupBox.Enabled = true;
            this.lockDefenceSelection = false;

            // Clear elements in Autonomous Tab
            Reset(this.DefenseReachedCheckbox);
            Reset(this.LowGoalCheckbox);
            Reset(this.HighGoalCheckbox);
            Reset(this.autoDefencesUsed);

            // Clear elements in Teleop Tab
            Reset(this.teleopDefenceLabels);
            Reset(this.teleopDefenceCrossed);
            Reset(this.teleopAssistanceCheckbox);
            ResetAssistedComboBoxes(this.teleopAssistanceCombobox);
            Reset(this.PickupCheckbox);
            Reset(this.ClimbCheckbox);
            Reset(this.SuccessfulChallengeCheckbox);
            Reset(this.TeleopLowGoalsMissedNumericUpDown);
            Reset(this.TeleopLowGoalsScoredNumericUpDown);
            Reset(this.TeleopHighGoalsMissedNumericUpDown);
            Reset(this.TeleopHighGoalsScoredNumericUpDown);

            // Clear elements on Summary tab
            Reset(this.AutonomousTrackBar);
            Reset(this.CrossingAbilityTrackBar);
            Reset(this.ScoringAbilityTrackBar);
            Reset(this.AssistanceAbilityTrackBar);
            Reset(this.ReportLabel);

            // Hide tabs
            this.CheckEnableTabs();
        }
        #endregion

        #region Reset Utilities


        private void Reset(TextBox element, string defaultValue = "")
        {
            element.Text = defaultValue;
        }
        private void Reset(NumericUpDown element, string defaultValue = "0")
        {
            element.Text = defaultValue;
        }


        private void Reset(Label element, string defaultValue = "")
        {
            element.Text = defaultValue;
        }

        private void Reset(CheckBox element, bool defaultValue = false)
        {
            element.Checked = defaultValue;
        }

        private void Reset(ComboBox element, string defaultValue = "")
        {
            element.Text = defaultValue;
        }
        private void Reset(TrackBar element, int defaultValue = 1)
        {
            element.Value = defaultValue;
        }

        private void Reset(TextBox[] elements, string defaultValue = "")
        {
            foreach (TextBox element in elements)
            {
                Reset(element, defaultValue);
            }
        }

        private void Reset(NumericUpDown[] elements, string defaultValue = "0")
        {
            foreach (NumericUpDown element in elements)
            {
                Reset(element, defaultValue);
            }
        }

        private void Reset(Label[] elements, string defaultValue = "")
        {
            foreach (Label element in elements)
            {
                Reset(element, defaultValue);
            }
        }

        private void Reset(CheckBox[] elements, bool defaultValue = false)
        {
            foreach (CheckBox element in elements)
            {
                Reset(element, defaultValue);
            }
        }

        private void Reset(ComboBox[] elements, string defaultValue = "")
        {
            foreach (ComboBox element in elements)
            {
                Reset(element, defaultValue);
            }
        }

        private void ResetDefencesChosen()
        {
            // Arraylist of the names of selected defenses
            this.defencesChosen = new List<String>();
            // The low bar is always present
            this.defencesChosen.Add("Low Bar");
        }

        private void ResetAssistedComboBoxes(ComboBox[] elements)
        {
            foreach (ComboBox element in elements)
            {
                ResetAssistedComboBox(element);
            }
        }

        private void ResetAssistedComboBox(ComboBox assistedComboBox)
        {
            assistedComboBox.Enabled = false;
            assistedComboBox.Text = "No Attempt";
            assistedComboBox.Items.Clear();
            assistedComboBox.Items.Add("No Attempt");
        }
        #endregion

        #region Enable Utilities

        private void Enable(CheckBox[] elements, bool value)
        {
            foreach (CheckBox element in elements)
            {
                Enable(element, value);
            }
        }

        private void Enable(CheckBox element, bool value)
        {
            element.Enabled = value;
        }

        #endregion


        #region Generate Report

        private void GenerateReport()
        {
            StringBuilder reportText = new StringBuilder();
            String newLine = "\n";

            // Basic Details
            reportText.Append("Round: " + this.RoundTextBox.Text + newLine);
            reportText.Append("Robot: " + this.RobotTextBox.Text + newLine + newLine);

            // Autonomous Details
            reportText.Append("Autonomous: " + newLine);
            
            // Get the defences that were crossed
            string defencesCrossed = "";
            for (int i = 0; i < this.autoDefencesUsed.Count(); i++)
            {
                CheckBox defenceCheckbox = this.autoDefencesUsed[i];
                if (defenceCheckbox.Checked)
                {
                    if (!defencesCrossed.Equals(""))
                    {
                        defencesCrossed += ", ";
                    }
                    defencesCrossed += this.defencesChosen.ElementAt(i);
                }
            }

            if (!defencesCrossed.Equals(""))
            {
                reportText.Append("Reached and crossed defence(s): " + defencesCrossed + newLine);
            } else if (this.DefenseReachedCheckbox.Checked)
            {
                reportText.Append("Reached a defence but didn't cross. " + newLine);
            }

            // Did it score low goal, high goal, or both?
            if (this.LowGoalCheckbox.Checked && this.HighGoalCheckbox.Checked)
            {
                reportText.Append("Scored a low and high goal. " + newLine);
            } else if  (this.LowGoalCheckbox.Checked)
            {
                reportText.Append("Scored a low goal. " + newLine);
            } else if (this.HighGoalCheckbox.Checked)
            {
                reportText.Append("Scored a high goal. " + newLine);
            }

            if (!this.LowGoalCheckbox.Checked && !this.HighGoalCheckbox.Checked)
            {
                reportText.Append("Did not score." + newLine);
            }

            reportText.Append(newLine);

            // Teleop Details
            reportText.Append("Teleop: " + newLine);

            StringBuilder teleopText = new StringBuilder();
            List<String> defencesPassed = new List<String>();

            // Get the defences that were crossed at least once
            for (int i = 0; i < teleopDefenceCrossed.Count(); i++)
            {
                if (teleopDefenceCrossed[i].Value > 0)
                {
                    defencesPassed.Add(defencesChosen[i]);
                }
            }
            
            // How many defenses were crossed?

            if (defencesPassed.Count == 0)
            {
                teleopText.Append("No defences were crossed. " + newLine);
            } else if (defencesPassed.Count == 1)
            {
                teleopText.Append("The " + defencesPassed[0] + " was crossed. " + newLine);
            } else
            {
                teleopText.Append("The ");
                for(int i = 0; i <= defencesPassed.Count - 2; i++)
                {
                    teleopText.Append(defencesPassed[i]);
                    if (i == defencesPassed.Count - 2)
                    {
                        teleopText.Append(" ");
                    } else
                    {
                        teleopText.Append(", ");
                    }
                }
                teleopText.Append("and " + defencesPassed[defencesPassed.Count - 1] + " were crossed. " + newLine);
            }

            reportText.Append(teleopText);

            // How about the scoring?
            String scoredText = "";

            // Did it climb?
            if (this.ClimbCheckbox.Checked)
            {
                scoredText += "Did climb, ";
            }
            else
            {
                scoredText += "Did not climb, ";
            }

            // Did it climb?
            if (this.SuccessfulChallengeCheckbox.Checked)
            {
                scoredText += " did successfuly challenge, ";
            }
            else
            {
                scoredText += " did not successfully challenge, ";
            }

            // Did it pickup?
            if (PickupCheckbox.Checked)
            {
                scoredText += "did pick up, ";
            } else
            {
                scoredText += "did not pick up, ";
            }

            int lowGoalsScored = (int)this.TeleopLowGoalsScoredNumericUpDown.Value;
            int highGoalsScored = (int)this.TeleopHighGoalsScoredNumericUpDown.Value;

            // Did they score high or low?
            if (lowGoalsScored == 0 && highGoalsScored == 0)
            {
                scoredText += "did not score high or low goals.";
            }
            else
            {
                // so they scored at least a goal.
                scoredText += "scored ";

                // was it a low goal?
                if (lowGoalsScored > 0)
                {
                    scoredText += "low goal(s)";
                }

                //  did they do both?
                if (lowGoalsScored > 0 && highGoalsScored > 0)
                {
                    scoredText += " and ";
                }


                if (highGoalsScored > 0)
                {
                    scoredText += "high goal(s)";
                }
                scoredText += ".";
            }

            reportText.Append(scoredText + newLine + newLine);
            
            // Robot Ratings
            reportText.Append("Robot Ratings (out of 5):" + newLine);
            reportText.Append("Autonomous performance: " + this.AutonomousTrackBar.Value + newLine);
            reportText.Append("Crossing ability: " + this.CrossingAbilityTrackBar.Value + newLine);
            reportText.Append("Scoring ability: " + this.ScoringAbilityTrackBar.Value + newLine);
            reportText.Append("Willingness to assist other robots: " + this.AssistanceAbilityTrackBar.Value + newLine);

            this.ReportLabel.Text = reportText.ToString();
        }

        #endregion

        #region Save & Export
        private void SaveAndRestartButton_Click(object sender, EventArgs e)
        {
            // Record everything in robot class.
            SaveRound();

            // Show the SavedRoundsLabel and update value
            this.SavedRoundsLabel.Visible = true;
            this.SavedRoundsCountLabel.Visible = true;
            this.SavedRoundsCountLabel.Text = Convert.ToString(robotRounds.Count);

            // Reset the Round and Return to Round Details tab
            ResetRound();
        }

        private void SaveAndExportButton_Click(object sender, EventArgs e)
        {
            SaveRound();
            if (ExportRoundData())
            {
                // Reset the Round and Return to Round Details tab
                ResetRound();
            }
        }

        private void SaveRound()
        {
            RobotRound thisRound = new RobotRound();
            // Copy all form values into RobotRound
            thisRound.RoundNumber = RoundTextBox.Text;
            thisRound.RobotNumber = RobotTextBox.Text;

            RecordAutonomousMode(thisRound);
            RecordTeleopMode(thisRound);
            RecordDefences(thisRound);
            RecordRating(thisRound);
            
            // Save into list.
            robotRounds.Add(thisRound);
        }

        private void RecordAutonomousMode(RobotRound thisRound)
        {
            thisRound.AutoDefenceReached = DefenseReachedCheckbox.Checked;
            thisRound.AutoLowGoal = LowGoalCheckbox.Checked;
            thisRound.AutoHighGoal = HighGoalCheckbox.Checked;
        }

        private void RecordDefences(RobotRound thisRound)
        {
            // Index to keep track of which checkbox to update
            int defenceIndex = 0;

            foreach (String defenceName in this.defencesChosen)
            {
                Defence defence = null;
                // Add Sally Port assistance status
                if (defenceName == "Sally Port" || defenceName == "Drawbridge")
                {
                    defence = new AssistedDefence(defenceName);
                    ((AssistedDefence)defence).ProvidedAssistance = teleopAssistanceCheckbox[defenceIndex].Checked;
                    String assistanceRequired = teleopAssistanceCombobox[defenceIndex].Text;
                    if (assistanceRequired == "Unassisted")
                    {
                        ((AssistedDefence)defence).MarkUnassisted();
                    }
                    else if (assistanceRequired == "With Assistance")
                    {
                        ((AssistedDefence)defence).MarkWithAssistance();
                    }
                    else
                    {
                        ((AssistedDefence)defence).MarkNoAttempt();
                    }
                }
                else
                {
                    defence = new Defence(defenceName);
                }

                // Add all the generic fields
                defence.AutoCrossed = autoDefencesUsed[defenceIndex].Checked;
                defence.TeleopCrossedCount = Int32.Parse(teleopDefenceCrossed[defenceIndex].Text);
                thisRound.addDefence(defence);

                defenceIndex++;
            }
        }

        private void RecordTeleopMode(RobotRound thisRound)
        {
            thisRound.TeleopPickup = PickupCheckbox.Checked;
            thisRound.TeleopClimb = ClimbCheckbox.Checked;
            thisRound.TeleopChallenge = SuccessfulChallengeCheckbox.Checked;
            thisRound.TeleopLowGoalsMissed = Int32.Parse(TeleopLowGoalsMissedNumericUpDown.Text);
            thisRound.TeleopLowGoalsScored = Int32.Parse(TeleopLowGoalsScoredNumericUpDown.Text);
            thisRound.TeleopHighGoalsMissed = Int32.Parse(TeleopHighGoalsMissedNumericUpDown.Text);
            thisRound.TeleopHighGoalsScored = Int32.Parse(TeleopHighGoalsScoredNumericUpDown.Text);
        }

        private void RecordRating(RobotRound thisRound)
        {
            thisRound.RatingAssistance = AssistanceAbilityTrackBar.Value;
            thisRound.RatingAutonomous = AutonomousTrackBar.Value;
            thisRound.RatingCrossing = CrossingAbilityTrackBar.Value;
            thisRound.RatingScoring = ScoringAbilityTrackBar.Value;
        }

        // Export Round Data to a CSV File. User first selects location of file
        // this must be a filename with extension of ".csv". If file already
        // exists it will be overwritten.
        private bool ExportRoundData()
        {
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV|*.csv";
            saveFileDialog.Title = "Save Robot Rounds";
            saveFileDialog.DefaultExt = ".csv";
            // When OK clicked need to check that file extension is ".csv"
            saveFileDialog.FileOk += CheckFileExtension;
            // Add extension "csv" if user doesn't enter one
            saveFileDialog.AddExtension = true;

            // OK clicked on Save Dialog so go ahead and export data
            // to this selected file.
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                String path = Path.GetFullPath(saveFileDialog.FileName);

                ExportRoundDataToFile(path);

                // Display message indicating number of robot rounds exported and to where
                string message = "Successfully exported " + this.robotRounds.Count + " robot round(s) to " + path;
                string caption = "Exported Robot Rounds";
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);

                // Hide Saved Rounds label
                this.SavedRoundsCountLabel.Visible = false;
                this.SavedRoundsLabel.Visible = false;

                // Clear the saved robots list
                robotRounds = new List<RobotRound>();

                return true;
            }
            return false;
        }

        // Check that the file extension is ".csv". If it's not error message
        // is shown and event is cancelled.
        private void CheckFileExtension(object sender, CancelEventArgs e)
        {
            SaveFileDialog sv = (sender as SaveFileDialog);
            if (Path.GetExtension(sv.FileName).ToLower() != ".csv")
            {
                e.Cancel = true;
                MessageBox.Show("Please omit the extension or use 'csv'");
                return;
            }
        }

        // Export the Robot Round data to the file specified in path parameter
        private void ExportRoundDataToFile(string path)
        {
            CsvWriter csvWriter = new CsvWriter(path);

            RobotRoundCsvConverter converter = new RobotRoundCsvConverter();

            // Add CSV Data for each RobotRound to CSV file
            foreach (RobotRound robotRound in robotRounds)
            {
                csvWriter.Write(converter.ToCsv(robotRound));
            }
            // Close the CSV Writer
            csvWriter.Dispose();
        }

        #endregion

        #region Form Events

        private void Stronghold_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If there are saved robot round data ask user if they want to export it first.
            if (this.robotRounds.Count() > 0)
            {
                String caption = "Quitting Stronghold";
                String message = "There are " + this.robotRounds.Count() + " robot round(s) that haven't been exported. "
                    + "If you exit now you will lose all the data. Do you want still want to quit?";
                if (MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    // User confirmed that don't want to close form so cancel event.
                    e.Cancel = true;
                }
            }
        }

        #endregion
    }
}
