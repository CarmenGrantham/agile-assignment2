﻿namespace Stronghold2
{
    partial class Stronghold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stronghold));
            this.TabController = new System.Windows.Forms.TabControl();
            this.RoundTabPage = new System.Windows.Forms.TabPage();
            this.RoundNextButton = new System.Windows.Forms.Button();
            this.DefensesGroupBox = new System.Windows.Forms.GroupBox();
            this.SallyPortCheckbox = new System.Windows.Forms.CheckBox();
            this.RoughTerrainCheckbox = new System.Windows.Forms.CheckBox();
            this.RockWallCheckbox = new System.Windows.Forms.CheckBox();
            this.RampartsCheckbox = new System.Windows.Forms.CheckBox();
            this.PortcullisCheckbox = new System.Windows.Forms.CheckBox();
            this.MoatCheckbox = new System.Windows.Forms.CheckBox();
            this.DrawbridgeCheckbox = new System.Windows.Forms.CheckBox();
            this.ChevalDeFriseCheckbox = new System.Windows.Forms.CheckBox();
            this.DetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.RobotTextBox = new System.Windows.Forms.TextBox();
            this.RobotLabel = new System.Windows.Forms.Label();
            this.RoundTextBox = new System.Windows.Forms.TextBox();
            this.RoundLabel = new System.Windows.Forms.Label();
            this.AutonomousTabPage = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AutoDefenceCheckBox0 = new System.Windows.Forms.CheckBox();
            this.AutoDefenceCheckBox4 = new System.Windows.Forms.CheckBox();
            this.AutoDefenceCheckBox1 = new System.Windows.Forms.CheckBox();
            this.AutoDefenceCheckBox2 = new System.Windows.Forms.CheckBox();
            this.AutoDefenceCheckBox3 = new System.Windows.Forms.CheckBox();
            this.AutonomousNextButton = new System.Windows.Forms.Button();
            this.AutonomousGroupBox = new System.Windows.Forms.GroupBox();
            this.HighGoalCheckbox = new System.Windows.Forms.CheckBox();
            this.LowGoalCheckbox = new System.Windows.Forms.CheckBox();
            this.DefenseReachedCheckbox = new System.Windows.Forms.CheckBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TeleopTabPage = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TeleopHighGoalsScoredNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.TeleopHighGoalsCountLabel = new System.Windows.Forms.Label();
            this.TeleopHighGoalsMissedNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TeleopLowGoalsScoredNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.TeleopLowGoalsMissedNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.ClimbCheckbox = new System.Windows.Forms.CheckBox();
            this.PickupCheckbox = new System.Windows.Forms.CheckBox();
            this.SuccessfulChallengeCheckbox = new System.Windows.Forms.CheckBox();
            this.TeleopGroupBox = new System.Windows.Forms.GroupBox();
            this.TeleopDefenceNumericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.TeleopDefenceNumericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.TeleopDefenceNumericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.TeleopDefenceNumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.TeleopDefenceNumericUpDown0 = new System.Windows.Forms.NumericUpDown();
            this.teleopAssistanceCheckbox4 = new System.Windows.Forms.CheckBox();
            this.teleopAssistanceCheckbox3 = new System.Windows.Forms.CheckBox();
            this.teleopAssistanceCheckbox2 = new System.Windows.Forms.CheckBox();
            this.teleopAssistanceCheckbox1 = new System.Windows.Forms.CheckBox();
            this.teleopAssistanceCheckbox0 = new System.Windows.Forms.CheckBox();
            this.teleopDefenceCombo4 = new System.Windows.Forms.ComboBox();
            this.teleopDefenceCombo3 = new System.Windows.Forms.ComboBox();
            this.teleopDefenceCombo2 = new System.Windows.Forms.ComboBox();
            this.teleopDefenceCombo1 = new System.Windows.Forms.ComboBox();
            this.teleopDefenceCombo0 = new System.Windows.Forms.ComboBox();
            this.TeleopDefenceLabel4 = new System.Windows.Forms.Label();
            this.TeleopDefenceLabel3 = new System.Windows.Forms.Label();
            this.TeleopDefenceLabel2 = new System.Windows.Forms.Label();
            this.TeleopDefenceLabel1 = new System.Windows.Forms.Label();
            this.TeleopDefenceLabel0 = new System.Windows.Forms.Label();
            this.TeleopNextButton = new System.Windows.Forms.Button();
            this.RatingTabPage = new System.Windows.Forms.TabPage();
            this.RatingNextButton = new System.Windows.Forms.Button();
            this.PerformanceGroupBox = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.AssistanceAbilityTrackBar = new System.Windows.Forms.TrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.ScoringAbilityTrackBar = new System.Windows.Forms.TrackBar();
            this.CrossingAbilityTrackBar = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.AutonomousTrackBar = new System.Windows.Forms.TrackBar();
            this.SummaryTabPage = new System.Windows.Forms.TabPage();
            this.ReportLabel = new System.Windows.Forms.Label();
            this.SaveAndRestartButton = new System.Windows.Forms.Button();
            this.SaveAndExportButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.SavedRoundsLabel = new System.Windows.Forms.Label();
            this.SavedRoundsCountLabel = new System.Windows.Forms.Label();
            this.TabController.SuspendLayout();
            this.RoundTabPage.SuspendLayout();
            this.DefensesGroupBox.SuspendLayout();
            this.DetailsGroupBox.SuspendLayout();
            this.AutonomousTabPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.AutonomousGroupBox.SuspendLayout();
            this.TeleopTabPage.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopHighGoalsScoredNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopHighGoalsMissedNumericUpDown)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopLowGoalsScoredNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopLowGoalsMissedNumericUpDown)).BeginInit();
            this.TeleopGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown0)).BeginInit();
            this.RatingTabPage.SuspendLayout();
            this.PerformanceGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AssistanceAbilityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScoringAbilityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrossingAbilityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutonomousTrackBar)).BeginInit();
            this.SummaryTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabController
            // 
            this.TabController.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabController.Controls.Add(this.RoundTabPage);
            this.TabController.Controls.Add(this.AutonomousTabPage);
            this.TabController.Controls.Add(this.TeleopTabPage);
            this.TabController.Controls.Add(this.RatingTabPage);
            this.TabController.Controls.Add(this.SummaryTabPage);
            this.TabController.Location = new System.Drawing.Point(12, 29);
            this.TabController.Name = "TabController";
            this.TabController.SelectedIndex = 0;
            this.TabController.Size = new System.Drawing.Size(560, 525);
            this.TabController.TabIndex = 7;
            this.TabController.SelectedIndexChanged += new System.EventHandler(this.TabController_SelectedIndexChanged);
            // 
            // RoundTabPage
            // 
            this.RoundTabPage.Controls.Add(this.RoundNextButton);
            this.RoundTabPage.Controls.Add(this.DefensesGroupBox);
            this.RoundTabPage.Controls.Add(this.DetailsGroupBox);
            this.RoundTabPage.Location = new System.Drawing.Point(4, 22);
            this.RoundTabPage.Name = "RoundTabPage";
            this.RoundTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.RoundTabPage.Size = new System.Drawing.Size(552, 499);
            this.RoundTabPage.TabIndex = 0;
            this.RoundTabPage.Text = "Round Details";
            this.RoundTabPage.UseVisualStyleBackColor = true;
            // 
            // RoundNextButton
            // 
            this.RoundNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RoundNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RoundNextButton.Location = new System.Drawing.Point(465, 464);
            this.RoundNextButton.Name = "RoundNextButton";
            this.RoundNextButton.Size = new System.Drawing.Size(75, 23);
            this.RoundNextButton.TabIndex = 11;
            this.RoundNextButton.Text = "Next";
            this.RoundNextButton.UseVisualStyleBackColor = true;
            this.RoundNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // DefensesGroupBox
            // 
            this.DefensesGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DefensesGroupBox.Controls.Add(this.SallyPortCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RoughTerrainCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RockWallCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RampartsCheckbox);
            this.DefensesGroupBox.Controls.Add(this.PortcullisCheckbox);
            this.DefensesGroupBox.Controls.Add(this.MoatCheckbox);
            this.DefensesGroupBox.Controls.Add(this.DrawbridgeCheckbox);
            this.DefensesGroupBox.Controls.Add(this.ChevalDeFriseCheckbox);
            this.DefensesGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefensesGroupBox.Location = new System.Drawing.Point(6, 129);
            this.DefensesGroupBox.Name = "DefensesGroupBox";
            this.DefensesGroupBox.Size = new System.Drawing.Size(534, 279);
            this.DefensesGroupBox.TabIndex = 6;
            this.DefensesGroupBox.TabStop = false;
            this.DefensesGroupBox.Text = "Defenses";
            // 
            // SallyPortCheckbox
            // 
            this.SallyPortCheckbox.AutoSize = true;
            this.SallyPortCheckbox.Location = new System.Drawing.Point(9, 240);
            this.SallyPortCheckbox.Name = "SallyPortCheckbox";
            this.SallyPortCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SallyPortCheckbox.Size = new System.Drawing.Size(94, 24);
            this.SallyPortCheckbox.TabIndex = 10;
            this.SallyPortCheckbox.Text = "Sally Port";
            this.SallyPortCheckbox.UseVisualStyleBackColor = true;
            this.SallyPortCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // RoughTerrainCheckbox
            // 
            this.RoughTerrainCheckbox.AutoSize = true;
            this.RoughTerrainCheckbox.Location = new System.Drawing.Point(9, 210);
            this.RoughTerrainCheckbox.Name = "RoughTerrainCheckbox";
            this.RoughTerrainCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RoughTerrainCheckbox.Size = new System.Drawing.Size(129, 24);
            this.RoughTerrainCheckbox.TabIndex = 9;
            this.RoughTerrainCheckbox.Text = "Rough Terrain";
            this.RoughTerrainCheckbox.UseVisualStyleBackColor = true;
            this.RoughTerrainCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // RockWallCheckbox
            // 
            this.RockWallCheckbox.AutoSize = true;
            this.RockWallCheckbox.Location = new System.Drawing.Point(9, 180);
            this.RockWallCheckbox.Name = "RockWallCheckbox";
            this.RockWallCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RockWallCheckbox.Size = new System.Drawing.Size(99, 24);
            this.RockWallCheckbox.TabIndex = 8;
            this.RockWallCheckbox.Text = "Rock Wall";
            this.RockWallCheckbox.UseVisualStyleBackColor = true;
            this.RockWallCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // RampartsCheckbox
            // 
            this.RampartsCheckbox.AutoSize = true;
            this.RampartsCheckbox.Location = new System.Drawing.Point(9, 150);
            this.RampartsCheckbox.Name = "RampartsCheckbox";
            this.RampartsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RampartsCheckbox.Size = new System.Drawing.Size(98, 24);
            this.RampartsCheckbox.TabIndex = 7;
            this.RampartsCheckbox.Text = "Ramparts";
            this.RampartsCheckbox.UseVisualStyleBackColor = true;
            this.RampartsCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // PortcullisCheckbox
            // 
            this.PortcullisCheckbox.AutoSize = true;
            this.PortcullisCheckbox.Location = new System.Drawing.Point(9, 120);
            this.PortcullisCheckbox.Name = "PortcullisCheckbox";
            this.PortcullisCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PortcullisCheckbox.Size = new System.Drawing.Size(91, 24);
            this.PortcullisCheckbox.TabIndex = 6;
            this.PortcullisCheckbox.Text = "Portcullis";
            this.PortcullisCheckbox.UseVisualStyleBackColor = true;
            this.PortcullisCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // MoatCheckbox
            // 
            this.MoatCheckbox.AutoSize = true;
            this.MoatCheckbox.Location = new System.Drawing.Point(9, 90);
            this.MoatCheckbox.Name = "MoatCheckbox";
            this.MoatCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MoatCheckbox.Size = new System.Drawing.Size(64, 24);
            this.MoatCheckbox.TabIndex = 5;
            this.MoatCheckbox.Text = "Moat";
            this.MoatCheckbox.UseVisualStyleBackColor = true;
            this.MoatCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // DrawbridgeCheckbox
            // 
            this.DrawbridgeCheckbox.AutoSize = true;
            this.DrawbridgeCheckbox.Location = new System.Drawing.Point(9, 60);
            this.DrawbridgeCheckbox.Name = "DrawbridgeCheckbox";
            this.DrawbridgeCheckbox.Size = new System.Drawing.Size(109, 24);
            this.DrawbridgeCheckbox.TabIndex = 4;
            this.DrawbridgeCheckbox.Text = "Drawbridge";
            this.DrawbridgeCheckbox.UseVisualStyleBackColor = true;
            this.DrawbridgeCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // ChevalDeFriseCheckbox
            // 
            this.ChevalDeFriseCheckbox.AutoSize = true;
            this.ChevalDeFriseCheckbox.Location = new System.Drawing.Point(9, 30);
            this.ChevalDeFriseCheckbox.Name = "ChevalDeFriseCheckbox";
            this.ChevalDeFriseCheckbox.Size = new System.Drawing.Size(137, 24);
            this.ChevalDeFriseCheckbox.TabIndex = 3;
            this.ChevalDeFriseCheckbox.Text = "Cheval de Frise";
            this.ChevalDeFriseCheckbox.UseVisualStyleBackColor = true;
            this.ChevalDeFriseCheckbox.CheckedChanged += new System.EventHandler(this.DefenceSelection_CheckedChanged);
            // 
            // DetailsGroupBox
            // 
            this.DetailsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DetailsGroupBox.Controls.Add(this.RobotTextBox);
            this.DetailsGroupBox.Controls.Add(this.RobotLabel);
            this.DetailsGroupBox.Controls.Add(this.RoundTextBox);
            this.DetailsGroupBox.Controls.Add(this.RoundLabel);
            this.DetailsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DetailsGroupBox.Location = new System.Drawing.Point(8, 19);
            this.DetailsGroupBox.Name = "DetailsGroupBox";
            this.DetailsGroupBox.Size = new System.Drawing.Size(532, 90);
            this.DetailsGroupBox.TabIndex = 1;
            this.DetailsGroupBox.TabStop = false;
            this.DetailsGroupBox.Text = "Details";
            // 
            // RobotTextBox
            // 
            this.RobotTextBox.Location = new System.Drawing.Point(80, 53);
            this.RobotTextBox.Name = "RobotTextBox";
            this.RobotTextBox.Size = new System.Drawing.Size(207, 26);
            this.RobotTextBox.TabIndex = 2;
            this.RobotTextBox.TextChanged += new System.EventHandler(this.RoundTextBox_TextChanged);
            // 
            // RobotLabel
            // 
            this.RobotLabel.Location = new System.Drawing.Point(6, 53);
            this.RobotLabel.Name = "RobotLabel";
            this.RobotLabel.Size = new System.Drawing.Size(68, 20);
            this.RobotLabel.TabIndex = 2;
            this.RobotLabel.Text = "Robot";
            this.RobotLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RoundTextBox
            // 
            this.RoundTextBox.Location = new System.Drawing.Point(80, 25);
            this.RoundTextBox.Name = "RoundTextBox";
            this.RoundTextBox.Size = new System.Drawing.Size(207, 26);
            this.RoundTextBox.TabIndex = 1;
            this.RoundTextBox.TextChanged += new System.EventHandler(this.RoundTextBox_TextChanged);
            // 
            // RoundLabel
            // 
            this.RoundLabel.Location = new System.Drawing.Point(6, 27);
            this.RoundLabel.Name = "RoundLabel";
            this.RoundLabel.Size = new System.Drawing.Size(68, 20);
            this.RoundLabel.TabIndex = 0;
            this.RoundLabel.Text = "Round";
            this.RoundLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AutonomousTabPage
            // 
            this.AutonomousTabPage.Controls.Add(this.groupBox1);
            this.AutonomousTabPage.Controls.Add(this.AutonomousNextButton);
            this.AutonomousTabPage.Controls.Add(this.AutonomousGroupBox);
            this.AutonomousTabPage.Location = new System.Drawing.Point(4, 22);
            this.AutonomousTabPage.Name = "AutonomousTabPage";
            this.AutonomousTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.AutonomousTabPage.Size = new System.Drawing.Size(552, 499);
            this.AutonomousTabPage.TabIndex = 1;
            this.AutonomousTabPage.Text = "Autonomous";
            this.AutonomousTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.AutoDefenceCheckBox0);
            this.groupBox1.Controls.Add(this.AutoDefenceCheckBox4);
            this.groupBox1.Controls.Add(this.AutoDefenceCheckBox1);
            this.groupBox1.Controls.Add(this.AutoDefenceCheckBox2);
            this.groupBox1.Controls.Add(this.AutoDefenceCheckBox3);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 163);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(526, 193);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Defences Crossed Autonomously";
            // 
            // AutoDefenceCheckBox0
            // 
            this.AutoDefenceCheckBox0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoDefenceCheckBox0.Location = new System.Drawing.Point(9, 30);
            this.AutoDefenceCheckBox0.Name = "AutoDefenceCheckBox0";
            this.AutoDefenceCheckBox0.Size = new System.Drawing.Size(150, 24);
            this.AutoDefenceCheckBox0.TabIndex = 6;
            this.AutoDefenceCheckBox0.Text = "Defense 0";
            this.AutoDefenceCheckBox0.UseVisualStyleBackColor = true;
            this.AutoDefenceCheckBox0.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // AutoDefenceCheckBox4
            // 
            this.AutoDefenceCheckBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoDefenceCheckBox4.Location = new System.Drawing.Point(9, 150);
            this.AutoDefenceCheckBox4.Name = "AutoDefenceCheckBox4";
            this.AutoDefenceCheckBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AutoDefenceCheckBox4.Size = new System.Drawing.Size(150, 32);
            this.AutoDefenceCheckBox4.TabIndex = 10;
            this.AutoDefenceCheckBox4.Text = "Defense 4";
            this.AutoDefenceCheckBox4.UseVisualStyleBackColor = true;
            this.AutoDefenceCheckBox4.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // AutoDefenceCheckBox1
            // 
            this.AutoDefenceCheckBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoDefenceCheckBox1.Location = new System.Drawing.Point(9, 60);
            this.AutoDefenceCheckBox1.Name = "AutoDefenceCheckBox1";
            this.AutoDefenceCheckBox1.Size = new System.Drawing.Size(150, 25);
            this.AutoDefenceCheckBox1.TabIndex = 7;
            this.AutoDefenceCheckBox1.Text = "Defense 1";
            this.AutoDefenceCheckBox1.UseVisualStyleBackColor = true;
            this.AutoDefenceCheckBox1.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // AutoDefenceCheckBox2
            // 
            this.AutoDefenceCheckBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoDefenceCheckBox2.Location = new System.Drawing.Point(9, 90);
            this.AutoDefenceCheckBox2.Name = "AutoDefenceCheckBox2";
            this.AutoDefenceCheckBox2.Size = new System.Drawing.Size(150, 24);
            this.AutoDefenceCheckBox2.TabIndex = 8;
            this.AutoDefenceCheckBox2.Text = "Defense 2";
            this.AutoDefenceCheckBox2.UseVisualStyleBackColor = true;
            this.AutoDefenceCheckBox2.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // AutoDefenceCheckBox3
            // 
            this.AutoDefenceCheckBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutoDefenceCheckBox3.Location = new System.Drawing.Point(9, 120);
            this.AutoDefenceCheckBox3.Name = "AutoDefenceCheckBox3";
            this.AutoDefenceCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AutoDefenceCheckBox3.Size = new System.Drawing.Size(150, 26);
            this.AutoDefenceCheckBox3.TabIndex = 9;
            this.AutoDefenceCheckBox3.Text = "Defense 3";
            this.AutoDefenceCheckBox3.UseVisualStyleBackColor = true;
            this.AutoDefenceCheckBox3.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // AutonomousNextButton
            // 
            this.AutonomousNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.AutonomousNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AutonomousNextButton.Location = new System.Drawing.Point(465, 464);
            this.AutonomousNextButton.Name = "AutonomousNextButton";
            this.AutonomousNextButton.Size = new System.Drawing.Size(75, 23);
            this.AutonomousNextButton.TabIndex = 5;
            this.AutonomousNextButton.Text = "Next";
            this.AutonomousNextButton.UseVisualStyleBackColor = true;
            this.AutonomousNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // AutonomousGroupBox
            // 
            this.AutonomousGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.AutonomousGroupBox.Controls.Add(this.HighGoalCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.LowGoalCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.DefenseReachedCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.Label1);
            this.AutonomousGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AutonomousGroupBox.Location = new System.Drawing.Point(8, 19);
            this.AutonomousGroupBox.Name = "AutonomousGroupBox";
            this.AutonomousGroupBox.Size = new System.Drawing.Size(524, 124);
            this.AutonomousGroupBox.TabIndex = 2;
            this.AutonomousGroupBox.TabStop = false;
            this.AutonomousGroupBox.Text = "Autonomous Performance";
            // 
            // HighGoalCheckbox
            // 
            this.HighGoalCheckbox.AutoSize = true;
            this.HighGoalCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HighGoalCheckbox.Location = new System.Drawing.Point(9, 90);
            this.HighGoalCheckbox.Name = "HighGoalCheckbox";
            this.HighGoalCheckbox.Size = new System.Drawing.Size(99, 24);
            this.HighGoalCheckbox.TabIndex = 4;
            this.HighGoalCheckbox.Text = "High Goal";
            this.HighGoalCheckbox.UseVisualStyleBackColor = true;
            // 
            // LowGoalCheckbox
            // 
            this.LowGoalCheckbox.AutoSize = true;
            this.LowGoalCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LowGoalCheckbox.Location = new System.Drawing.Point(9, 60);
            this.LowGoalCheckbox.Name = "LowGoalCheckbox";
            this.LowGoalCheckbox.Size = new System.Drawing.Size(95, 24);
            this.LowGoalCheckbox.TabIndex = 3;
            this.LowGoalCheckbox.Text = "Low Goal";
            this.LowGoalCheckbox.UseVisualStyleBackColor = true;
            // 
            // DefenseReachedCheckbox
            // 
            this.DefenseReachedCheckbox.AutoSize = true;
            this.DefenseReachedCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DefenseReachedCheckbox.Location = new System.Drawing.Point(9, 30);
            this.DefenseReachedCheckbox.Name = "DefenseReachedCheckbox";
            this.DefenseReachedCheckbox.Size = new System.Drawing.Size(158, 24);
            this.DefenseReachedCheckbox.TabIndex = 1;
            this.DefenseReachedCheckbox.Text = "Defense Reached";
            this.DefenseReachedCheckbox.UseVisualStyleBackColor = true;
            this.DefenseReachedCheckbox.CheckedChanged += new System.EventHandler(this.DefenseReachedCheckbox_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(6, 27);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 16);
            this.Label1.TabIndex = 0;
            // 
            // TeleopTabPage
            // 
            this.TeleopTabPage.Controls.Add(this.groupBox2);
            this.TeleopTabPage.Controls.Add(this.TeleopGroupBox);
            this.TeleopTabPage.Controls.Add(this.TeleopNextButton);
            this.TeleopTabPage.Location = new System.Drawing.Point(4, 22);
            this.TeleopTabPage.Name = "TeleopTabPage";
            this.TeleopTabPage.Size = new System.Drawing.Size(552, 499);
            this.TeleopTabPage.TabIndex = 2;
            this.TeleopTabPage.Text = "Teleop";
            this.TeleopTabPage.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.ClimbCheckbox);
            this.groupBox2.Controls.Add(this.PickupCheckbox);
            this.groupBox2.Controls.Add(this.SuccessfulChallengeCheckbox);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 226);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 192);
            this.groupBox2.TabIndex = 55;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Scoring Ability";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.TeleopHighGoalsScoredNumericUpDown);
            this.groupBox4.Controls.Add(this.TeleopHighGoalsCountLabel);
            this.groupBox4.Controls.Add(this.TeleopHighGoalsMissedNumericUpDown);
            this.groupBox4.Location = new System.Drawing.Point(259, 79);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(182, 100);
            this.groupBox4.TabIndex = 56;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "High Goals";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(22, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 38;
            this.label6.Text = "Scored";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopHighGoalsScoredNumericUpDown
            // 
            this.TeleopHighGoalsScoredNumericUpDown.Location = new System.Drawing.Point(97, 24);
            this.TeleopHighGoalsScoredNumericUpDown.Name = "TeleopHighGoalsScoredNumericUpDown";
            this.TeleopHighGoalsScoredNumericUpDown.Size = new System.Drawing.Size(42, 26);
            this.TeleopHighGoalsScoredNumericUpDown.TabIndex = 54;
            // 
            // TeleopHighGoalsCountLabel
            // 
            this.TeleopHighGoalsCountLabel.Location = new System.Drawing.Point(22, 53);
            this.TeleopHighGoalsCountLabel.Name = "TeleopHighGoalsCountLabel";
            this.TeleopHighGoalsCountLabel.Size = new System.Drawing.Size(69, 20);
            this.TeleopHighGoalsCountLabel.TabIndex = 15;
            this.TeleopHighGoalsCountLabel.Text = "Missed";
            this.TeleopHighGoalsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopHighGoalsMissedNumericUpDown
            // 
            this.TeleopHighGoalsMissedNumericUpDown.Location = new System.Drawing.Point(97, 51);
            this.TeleopHighGoalsMissedNumericUpDown.Name = "TeleopHighGoalsMissedNumericUpDown";
            this.TeleopHighGoalsMissedNumericUpDown.Size = new System.Drawing.Size(42, 26);
            this.TeleopHighGoalsMissedNumericUpDown.TabIndex = 53;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.TeleopLowGoalsScoredNumericUpDown);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.TeleopLowGoalsMissedNumericUpDown);
            this.groupBox3.Location = new System.Drawing.Point(30, 79);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(184, 100);
            this.groupBox3.TabIndex = 55;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Low Goals";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 20);
            this.label7.TabIndex = 40;
            this.label7.Text = "Missed";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopLowGoalsScoredNumericUpDown
            // 
            this.TeleopLowGoalsScoredNumericUpDown.Location = new System.Drawing.Point(94, 24);
            this.TeleopLowGoalsScoredNumericUpDown.Name = "TeleopLowGoalsScoredNumericUpDown";
            this.TeleopLowGoalsScoredNumericUpDown.Size = new System.Drawing.Size(42, 26);
            this.TeleopLowGoalsScoredNumericUpDown.TabIndex = 52;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 20);
            this.label8.TabIndex = 42;
            this.label8.Text = "Scored";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopLowGoalsMissedNumericUpDown
            // 
            this.TeleopLowGoalsMissedNumericUpDown.Location = new System.Drawing.Point(94, 51);
            this.TeleopLowGoalsMissedNumericUpDown.Name = "TeleopLowGoalsMissedNumericUpDown";
            this.TeleopLowGoalsMissedNumericUpDown.Size = new System.Drawing.Size(42, 26);
            this.TeleopLowGoalsMissedNumericUpDown.TabIndex = 51;
            // 
            // ClimbCheckbox
            // 
            this.ClimbCheckbox.AutoSize = true;
            this.ClimbCheckbox.Location = new System.Drawing.Point(43, 38);
            this.ClimbCheckbox.Name = "ClimbCheckbox";
            this.ClimbCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ClimbCheckbox.Size = new System.Drawing.Size(67, 24);
            this.ClimbCheckbox.TabIndex = 10;
            this.ClimbCheckbox.Text = "Climb";
            this.ClimbCheckbox.UseVisualStyleBackColor = true;
            // 
            // PickupCheckbox
            // 
            this.PickupCheckbox.AutoSize = true;
            this.PickupCheckbox.Location = new System.Drawing.Point(356, 38);
            this.PickupCheckbox.Name = "PickupCheckbox";
            this.PickupCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PickupCheckbox.Size = new System.Drawing.Size(75, 24);
            this.PickupCheckbox.TabIndex = 6;
            this.PickupCheckbox.Text = "Pickup";
            this.PickupCheckbox.UseVisualStyleBackColor = true;
            // 
            // SuccessfulChallengeCheckbox
            // 
            this.SuccessfulChallengeCheckbox.AutoSize = true;
            this.SuccessfulChallengeCheckbox.Location = new System.Drawing.Point(145, 38);
            this.SuccessfulChallengeCheckbox.Name = "SuccessfulChallengeCheckbox";
            this.SuccessfulChallengeCheckbox.Size = new System.Drawing.Size(181, 24);
            this.SuccessfulChallengeCheckbox.TabIndex = 45;
            this.SuccessfulChallengeCheckbox.Text = "Successful Challenge";
            this.SuccessfulChallengeCheckbox.UseVisualStyleBackColor = true;
            // 
            // TeleopGroupBox
            // 
            this.TeleopGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceNumericUpDown4);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceNumericUpDown3);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceNumericUpDown2);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceNumericUpDown1);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceNumericUpDown0);
            this.TeleopGroupBox.Controls.Add(this.teleopAssistanceCheckbox4);
            this.TeleopGroupBox.Controls.Add(this.teleopAssistanceCheckbox3);
            this.TeleopGroupBox.Controls.Add(this.teleopAssistanceCheckbox2);
            this.TeleopGroupBox.Controls.Add(this.teleopAssistanceCheckbox1);
            this.TeleopGroupBox.Controls.Add(this.teleopAssistanceCheckbox0);
            this.TeleopGroupBox.Controls.Add(this.teleopDefenceCombo4);
            this.TeleopGroupBox.Controls.Add(this.teleopDefenceCombo3);
            this.TeleopGroupBox.Controls.Add(this.teleopDefenceCombo2);
            this.TeleopGroupBox.Controls.Add(this.teleopDefenceCombo1);
            this.TeleopGroupBox.Controls.Add(this.teleopDefenceCombo0);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceLabel4);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceLabel3);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceLabel2);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceLabel1);
            this.TeleopGroupBox.Controls.Add(this.TeleopDefenceLabel0);
            this.TeleopGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TeleopGroupBox.Location = new System.Drawing.Point(8, 19);
            this.TeleopGroupBox.Name = "TeleopGroupBox";
            this.TeleopGroupBox.Size = new System.Drawing.Size(532, 195);
            this.TeleopGroupBox.TabIndex = 3;
            this.TeleopGroupBox.TabStop = false;
            this.TeleopGroupBox.Text = "Teleop Defences Crossed";
            // 
            // TeleopDefenceNumericUpDown4
            // 
            this.TeleopDefenceNumericUpDown4.Location = new System.Drawing.Point(150, 149);
            this.TeleopDefenceNumericUpDown4.Name = "TeleopDefenceNumericUpDown4";
            this.TeleopDefenceNumericUpDown4.Size = new System.Drawing.Size(42, 26);
            this.TeleopDefenceNumericUpDown4.TabIndex = 50;
            this.TeleopDefenceNumericUpDown4.ValueChanged += new System.EventHandler(this.TeleopDefenceNumericUpDown4_ValueChanged);
            // 
            // TeleopDefenceNumericUpDown3
            // 
            this.TeleopDefenceNumericUpDown3.Location = new System.Drawing.Point(150, 120);
            this.TeleopDefenceNumericUpDown3.Name = "TeleopDefenceNumericUpDown3";
            this.TeleopDefenceNumericUpDown3.Size = new System.Drawing.Size(42, 26);
            this.TeleopDefenceNumericUpDown3.TabIndex = 49;
            this.TeleopDefenceNumericUpDown3.ValueChanged += new System.EventHandler(this.TeleopDefenceNumericUpDown3_ValueChanged);
            // 
            // TeleopDefenceNumericUpDown2
            // 
            this.TeleopDefenceNumericUpDown2.Location = new System.Drawing.Point(150, 90);
            this.TeleopDefenceNumericUpDown2.Name = "TeleopDefenceNumericUpDown2";
            this.TeleopDefenceNumericUpDown2.Size = new System.Drawing.Size(42, 26);
            this.TeleopDefenceNumericUpDown2.TabIndex = 48;
            this.TeleopDefenceNumericUpDown2.ValueChanged += new System.EventHandler(this.TeleopDefenceNumericUpDown2_ValueChanged);
            // 
            // TeleopDefenceNumericUpDown1
            // 
            this.TeleopDefenceNumericUpDown1.Location = new System.Drawing.Point(150, 60);
            this.TeleopDefenceNumericUpDown1.Name = "TeleopDefenceNumericUpDown1";
            this.TeleopDefenceNumericUpDown1.Size = new System.Drawing.Size(42, 26);
            this.TeleopDefenceNumericUpDown1.TabIndex = 47;
            this.TeleopDefenceNumericUpDown1.ValueChanged += new System.EventHandler(this.TeleopDefenceNumericUpDown1_ValueChanged);
            // 
            // TeleopDefenceNumericUpDown0
            // 
            this.TeleopDefenceNumericUpDown0.Location = new System.Drawing.Point(150, 30);
            this.TeleopDefenceNumericUpDown0.Name = "TeleopDefenceNumericUpDown0";
            this.TeleopDefenceNumericUpDown0.Size = new System.Drawing.Size(42, 26);
            this.TeleopDefenceNumericUpDown0.TabIndex = 46;
            this.TeleopDefenceNumericUpDown0.ValueChanged += new System.EventHandler(this.TeleopDefenceNumericUpDown0_ValueChanged);
            // 
            // teleopAssistanceCheckbox4
            // 
            this.teleopAssistanceCheckbox4.AutoSize = true;
            this.teleopAssistanceCheckbox4.Location = new System.Drawing.Point(346, 150);
            this.teleopAssistanceCheckbox4.Name = "teleopAssistanceCheckbox4";
            this.teleopAssistanceCheckbox4.Size = new System.Drawing.Size(171, 24);
            this.teleopAssistanceCheckbox4.TabIndex = 36;
            this.teleopAssistanceCheckbox4.Text = "Provided Assistance";
            this.teleopAssistanceCheckbox4.UseVisualStyleBackColor = true;
            this.teleopAssistanceCheckbox4.Visible = false;
            // 
            // teleopAssistanceCheckbox3
            // 
            this.teleopAssistanceCheckbox3.AutoSize = true;
            this.teleopAssistanceCheckbox3.Location = new System.Drawing.Point(346, 120);
            this.teleopAssistanceCheckbox3.Name = "teleopAssistanceCheckbox3";
            this.teleopAssistanceCheckbox3.Size = new System.Drawing.Size(171, 24);
            this.teleopAssistanceCheckbox3.TabIndex = 35;
            this.teleopAssistanceCheckbox3.Text = "Provided Assistance";
            this.teleopAssistanceCheckbox3.UseVisualStyleBackColor = true;
            this.teleopAssistanceCheckbox3.Visible = false;
            // 
            // teleopAssistanceCheckbox2
            // 
            this.teleopAssistanceCheckbox2.AutoSize = true;
            this.teleopAssistanceCheckbox2.Location = new System.Drawing.Point(346, 90);
            this.teleopAssistanceCheckbox2.Name = "teleopAssistanceCheckbox2";
            this.teleopAssistanceCheckbox2.Size = new System.Drawing.Size(171, 24);
            this.teleopAssistanceCheckbox2.TabIndex = 34;
            this.teleopAssistanceCheckbox2.Text = "Provided Assistance";
            this.teleopAssistanceCheckbox2.UseVisualStyleBackColor = true;
            this.teleopAssistanceCheckbox2.Visible = false;
            // 
            // teleopAssistanceCheckbox1
            // 
            this.teleopAssistanceCheckbox1.AutoSize = true;
            this.teleopAssistanceCheckbox1.Location = new System.Drawing.Point(346, 60);
            this.teleopAssistanceCheckbox1.Name = "teleopAssistanceCheckbox1";
            this.teleopAssistanceCheckbox1.Size = new System.Drawing.Size(171, 24);
            this.teleopAssistanceCheckbox1.TabIndex = 33;
            this.teleopAssistanceCheckbox1.Text = "Provided Assistance";
            this.teleopAssistanceCheckbox1.UseVisualStyleBackColor = true;
            this.teleopAssistanceCheckbox1.Visible = false;
            // 
            // teleopAssistanceCheckbox0
            // 
            this.teleopAssistanceCheckbox0.AutoSize = true;
            this.teleopAssistanceCheckbox0.Location = new System.Drawing.Point(346, 30);
            this.teleopAssistanceCheckbox0.Name = "teleopAssistanceCheckbox0";
            this.teleopAssistanceCheckbox0.Size = new System.Drawing.Size(171, 24);
            this.teleopAssistanceCheckbox0.TabIndex = 32;
            this.teleopAssistanceCheckbox0.Text = "Provided Assistance";
            this.teleopAssistanceCheckbox0.UseVisualStyleBackColor = true;
            this.teleopAssistanceCheckbox0.Visible = false;
            // 
            // teleopDefenceCombo4
            // 
            this.teleopDefenceCombo4.FormattingEnabled = true;
            this.teleopDefenceCombo4.Items.AddRange(new object[] {
            "No Attempt"});
            this.teleopDefenceCombo4.Location = new System.Drawing.Point(198, 150);
            this.teleopDefenceCombo4.Name = "teleopDefenceCombo4";
            this.teleopDefenceCombo4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.teleopDefenceCombo4.Size = new System.Drawing.Size(142, 28);
            this.teleopDefenceCombo4.TabIndex = 31;
            this.teleopDefenceCombo4.Text = "No Attempt";
            this.teleopDefenceCombo4.Visible = false;
            // 
            // teleopDefenceCombo3
            // 
            this.teleopDefenceCombo3.FormattingEnabled = true;
            this.teleopDefenceCombo3.Items.AddRange(new object[] {
            "No Attempt"});
            this.teleopDefenceCombo3.Location = new System.Drawing.Point(198, 120);
            this.teleopDefenceCombo3.Name = "teleopDefenceCombo3";
            this.teleopDefenceCombo3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.teleopDefenceCombo3.Size = new System.Drawing.Size(142, 28);
            this.teleopDefenceCombo3.TabIndex = 30;
            this.teleopDefenceCombo3.Text = "No Attempt";
            this.teleopDefenceCombo3.Visible = false;
            // 
            // teleopDefenceCombo2
            // 
            this.teleopDefenceCombo2.FormattingEnabled = true;
            this.teleopDefenceCombo2.Items.AddRange(new object[] {
            "No Attempt"});
            this.teleopDefenceCombo2.Location = new System.Drawing.Point(198, 90);
            this.teleopDefenceCombo2.Name = "teleopDefenceCombo2";
            this.teleopDefenceCombo2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.teleopDefenceCombo2.Size = new System.Drawing.Size(142, 28);
            this.teleopDefenceCombo2.TabIndex = 29;
            this.teleopDefenceCombo2.Text = "No Attempt";
            this.teleopDefenceCombo2.Visible = false;
            // 
            // teleopDefenceCombo1
            // 
            this.teleopDefenceCombo1.FormattingEnabled = true;
            this.teleopDefenceCombo1.Items.AddRange(new object[] {
            "No Attempt"});
            this.teleopDefenceCombo1.Location = new System.Drawing.Point(198, 60);
            this.teleopDefenceCombo1.Name = "teleopDefenceCombo1";
            this.teleopDefenceCombo1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.teleopDefenceCombo1.Size = new System.Drawing.Size(142, 28);
            this.teleopDefenceCombo1.TabIndex = 28;
            this.teleopDefenceCombo1.Text = "No Attempt";
            this.teleopDefenceCombo1.Visible = false;
            // 
            // teleopDefenceCombo0
            // 
            this.teleopDefenceCombo0.FormattingEnabled = true;
            this.teleopDefenceCombo0.Items.AddRange(new object[] {
            "No Attempt"});
            this.teleopDefenceCombo0.Location = new System.Drawing.Point(198, 30);
            this.teleopDefenceCombo0.Name = "teleopDefenceCombo0";
            this.teleopDefenceCombo0.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.teleopDefenceCombo0.Size = new System.Drawing.Size(142, 28);
            this.teleopDefenceCombo0.TabIndex = 27;
            this.teleopDefenceCombo0.Text = "No Attempt";
            this.teleopDefenceCombo0.Visible = false;
            // 
            // TeleopDefenceLabel4
            // 
            this.TeleopDefenceLabel4.Location = new System.Drawing.Point(6, 150);
            this.TeleopDefenceLabel4.Name = "TeleopDefenceLabel4";
            this.TeleopDefenceLabel4.Size = new System.Drawing.Size(138, 27);
            this.TeleopDefenceLabel4.TabIndex = 21;
            this.TeleopDefenceLabel4.Text = "Defence 4";
            this.TeleopDefenceLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopDefenceLabel3
            // 
            this.TeleopDefenceLabel3.Location = new System.Drawing.Point(6, 120);
            this.TeleopDefenceLabel3.Name = "TeleopDefenceLabel3";
            this.TeleopDefenceLabel3.Size = new System.Drawing.Size(138, 21);
            this.TeleopDefenceLabel3.TabIndex = 20;
            this.TeleopDefenceLabel3.Text = "Defence 3";
            this.TeleopDefenceLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopDefenceLabel2
            // 
            this.TeleopDefenceLabel2.Location = new System.Drawing.Point(6, 90);
            this.TeleopDefenceLabel2.Name = "TeleopDefenceLabel2";
            this.TeleopDefenceLabel2.Size = new System.Drawing.Size(138, 22);
            this.TeleopDefenceLabel2.TabIndex = 19;
            this.TeleopDefenceLabel2.Text = "Defence 2";
            this.TeleopDefenceLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopDefenceLabel1
            // 
            this.TeleopDefenceLabel1.Location = new System.Drawing.Point(6, 60);
            this.TeleopDefenceLabel1.Name = "TeleopDefenceLabel1";
            this.TeleopDefenceLabel1.Size = new System.Drawing.Size(138, 22);
            this.TeleopDefenceLabel1.TabIndex = 18;
            this.TeleopDefenceLabel1.Text = "Defence 1";
            this.TeleopDefenceLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopDefenceLabel0
            // 
            this.TeleopDefenceLabel0.Location = new System.Drawing.Point(3, 30);
            this.TeleopDefenceLabel0.Name = "TeleopDefenceLabel0";
            this.TeleopDefenceLabel0.Size = new System.Drawing.Size(141, 24);
            this.TeleopDefenceLabel0.TabIndex = 17;
            this.TeleopDefenceLabel0.Text = "Defence 0";
            this.TeleopDefenceLabel0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopNextButton
            // 
            this.TeleopNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TeleopNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TeleopNextButton.Location = new System.Drawing.Point(465, 464);
            this.TeleopNextButton.Name = "TeleopNextButton";
            this.TeleopNextButton.Size = new System.Drawing.Size(75, 23);
            this.TeleopNextButton.TabIndex = 11;
            this.TeleopNextButton.Text = "Next";
            this.TeleopNextButton.UseVisualStyleBackColor = true;
            this.TeleopNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // RatingTabPage
            // 
            this.RatingTabPage.Controls.Add(this.RatingNextButton);
            this.RatingTabPage.Controls.Add(this.PerformanceGroupBox);
            this.RatingTabPage.Location = new System.Drawing.Point(4, 22);
            this.RatingTabPage.Name = "RatingTabPage";
            this.RatingTabPage.Size = new System.Drawing.Size(552, 499);
            this.RatingTabPage.TabIndex = 3;
            this.RatingTabPage.Text = "Rating";
            this.RatingTabPage.UseVisualStyleBackColor = true;
            // 
            // RatingNextButton
            // 
            this.RatingNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.RatingNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RatingNextButton.Location = new System.Drawing.Point(465, 464);
            this.RatingNextButton.Name = "RatingNextButton";
            this.RatingNextButton.Size = new System.Drawing.Size(75, 23);
            this.RatingNextButton.TabIndex = 12;
            this.RatingNextButton.Text = "Next";
            this.RatingNextButton.UseVisualStyleBackColor = true;
            this.RatingNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // PerformanceGroupBox
            // 
            this.PerformanceGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PerformanceGroupBox.Controls.Add(this.label16);
            this.PerformanceGroupBox.Controls.Add(this.label15);
            this.PerformanceGroupBox.Controls.Add(this.label14);
            this.PerformanceGroupBox.Controls.Add(this.label13);
            this.PerformanceGroupBox.Controls.Add(this.AssistanceAbilityTrackBar);
            this.PerformanceGroupBox.Controls.Add(this.label12);
            this.PerformanceGroupBox.Controls.Add(this.ScoringAbilityTrackBar);
            this.PerformanceGroupBox.Controls.Add(this.CrossingAbilityTrackBar);
            this.PerformanceGroupBox.Controls.Add(this.label11);
            this.PerformanceGroupBox.Controls.Add(this.AutonomousTrackBar);
            this.PerformanceGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PerformanceGroupBox.Location = new System.Drawing.Point(8, 19);
            this.PerformanceGroupBox.Name = "PerformanceGroupBox";
            this.PerformanceGroupBox.Size = new System.Drawing.Size(531, 241);
            this.PerformanceGroupBox.TabIndex = 10;
            this.PerformanceGroupBox.TabStop = false;
            this.PerformanceGroupBox.Text = "Robot Rating";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(432, 27);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 20);
            this.label16.TabIndex = 14;
            this.label16.Text = "Excellent";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(261, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 20);
            this.label15.TabIndex = 13;
            this.label15.Text = "Awful";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 169);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(241, 20);
            this.label14.TabIndex = 12;
            this.label14.Text = "Willingness to assist other robots";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(136, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 20);
            this.label13.TabIndex = 12;
            this.label13.Text = "Scoring Ability";
            // 
            // AssistanceAbilityTrackBar
            // 
            this.AssistanceAbilityTrackBar.AutoSize = false;
            this.AssistanceAbilityTrackBar.LargeChange = 1;
            this.AssistanceAbilityTrackBar.Location = new System.Drawing.Point(259, 169);
            this.AssistanceAbilityTrackBar.Maximum = 5;
            this.AssistanceAbilityTrackBar.Minimum = 1;
            this.AssistanceAbilityTrackBar.Name = "AssistanceAbilityTrackBar";
            this.AssistanceAbilityTrackBar.Size = new System.Drawing.Size(250, 30);
            this.AssistanceAbilityTrackBar.TabIndex = 11;
            this.AssistanceAbilityTrackBar.Value = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(128, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(116, 20);
            this.label12.TabIndex = 3;
            this.label12.Text = "Crossing Ability";
            // 
            // ScoringAbilityTrackBar
            // 
            this.ScoringAbilityTrackBar.AutoSize = false;
            this.ScoringAbilityTrackBar.LargeChange = 1;
            this.ScoringAbilityTrackBar.Location = new System.Drawing.Point(259, 129);
            this.ScoringAbilityTrackBar.Maximum = 5;
            this.ScoringAbilityTrackBar.Minimum = 1;
            this.ScoringAbilityTrackBar.Name = "ScoringAbilityTrackBar";
            this.ScoringAbilityTrackBar.Size = new System.Drawing.Size(250, 30);
            this.ScoringAbilityTrackBar.TabIndex = 11;
            this.ScoringAbilityTrackBar.Value = 1;
            // 
            // CrossingAbilityTrackBar
            // 
            this.CrossingAbilityTrackBar.AutoSize = false;
            this.CrossingAbilityTrackBar.LargeChange = 1;
            this.CrossingAbilityTrackBar.Location = new System.Drawing.Point(259, 89);
            this.CrossingAbilityTrackBar.Maximum = 5;
            this.CrossingAbilityTrackBar.Minimum = 1;
            this.CrossingAbilityTrackBar.Name = "CrossingAbilityTrackBar";
            this.CrossingAbilityTrackBar.Size = new System.Drawing.Size(250, 30);
            this.CrossingAbilityTrackBar.TabIndex = 2;
            this.CrossingAbilityTrackBar.Value = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(49, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(195, 20);
            this.label11.TabIndex = 1;
            this.label11.Text = "Autonomous Performance";
            // 
            // AutonomousTrackBar
            // 
            this.AutonomousTrackBar.AutoSize = false;
            this.AutonomousTrackBar.LargeChange = 1;
            this.AutonomousTrackBar.Location = new System.Drawing.Point(259, 49);
            this.AutonomousTrackBar.Maximum = 5;
            this.AutonomousTrackBar.Minimum = 1;
            this.AutonomousTrackBar.Name = "AutonomousTrackBar";
            this.AutonomousTrackBar.Size = new System.Drawing.Size(250, 30);
            this.AutonomousTrackBar.TabIndex = 0;
            this.AutonomousTrackBar.Value = 1;
            // 
            // SummaryTabPage
            // 
            this.SummaryTabPage.Controls.Add(this.ReportLabel);
            this.SummaryTabPage.Controls.Add(this.SaveAndRestartButton);
            this.SummaryTabPage.Controls.Add(this.SaveAndExportButton);
            this.SummaryTabPage.Controls.Add(this.ResetButton);
            this.SummaryTabPage.Location = new System.Drawing.Point(4, 22);
            this.SummaryTabPage.Name = "SummaryTabPage";
            this.SummaryTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SummaryTabPage.Size = new System.Drawing.Size(552, 499);
            this.SummaryTabPage.TabIndex = 4;
            this.SummaryTabPage.Text = "Summary";
            this.SummaryTabPage.UseVisualStyleBackColor = true;
            // 
            // ReportLabel
            // 
            this.ReportLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReportLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportLabel.Location = new System.Drawing.Point(10, 19);
            this.ReportLabel.Name = "ReportLabel";
            this.ReportLabel.Size = new System.Drawing.Size(536, 428);
            this.ReportLabel.TabIndex = 16;
            this.ReportLabel.Text = "Label3";
            // 
            // SaveAndRestartButton
            // 
            this.SaveAndRestartButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveAndRestartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAndRestartButton.Location = new System.Drawing.Point(233, 464);
            this.SaveAndRestartButton.Name = "SaveAndRestartButton";
            this.SaveAndRestartButton.Size = new System.Drawing.Size(102, 23);
            this.SaveAndRestartButton.TabIndex = 15;
            this.SaveAndRestartButton.Text = "Save && Restart";
            this.SaveAndRestartButton.UseVisualStyleBackColor = true;
            this.SaveAndRestartButton.Click += new System.EventHandler(this.SaveAndRestartButton_Click);
            // 
            // SaveAndExportButton
            // 
            this.SaveAndExportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveAndExportButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAndExportButton.Location = new System.Drawing.Point(455, 464);
            this.SaveAndExportButton.Name = "SaveAndExportButton";
            this.SaveAndExportButton.Size = new System.Drawing.Size(88, 23);
            this.SaveAndExportButton.TabIndex = 14;
            this.SaveAndExportButton.Text = "Save && Export";
            this.SaveAndExportButton.UseVisualStyleBackColor = true;
            this.SaveAndExportButton.Click += new System.EventHandler(this.SaveAndExportButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ResetButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetButton.Location = new System.Drawing.Point(10, 464);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 13;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // SavedRoundsLabel
            // 
            this.SavedRoundsLabel.AutoSize = true;
            this.SavedRoundsLabel.BackColor = System.Drawing.Color.Transparent;
            this.SavedRoundsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavedRoundsLabel.ForeColor = System.Drawing.Color.White;
            this.SavedRoundsLabel.Location = new System.Drawing.Point(365, 9);
            this.SavedRoundsLabel.Name = "SavedRoundsLabel";
            this.SavedRoundsLabel.Size = new System.Drawing.Size(131, 20);
            this.SavedRoundsLabel.TabIndex = 8;
            this.SavedRoundsLabel.Text = "Saved Rounds:";
            // 
            // SavedRoundsCountLabel
            // 
            this.SavedRoundsCountLabel.AutoSize = true;
            this.SavedRoundsCountLabel.BackColor = System.Drawing.Color.Transparent;
            this.SavedRoundsCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SavedRoundsCountLabel.ForeColor = System.Drawing.Color.White;
            this.SavedRoundsCountLabel.Location = new System.Drawing.Point(502, 9);
            this.SavedRoundsCountLabel.Name = "SavedRoundsCountLabel";
            this.SavedRoundsCountLabel.Size = new System.Drawing.Size(67, 20);
            this.SavedRoundsCountLabel.TabIndex = 9;
            this.SavedRoundsCountLabel.Text = "label17";
            // 
            // Stronghold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(584, 566);
            this.Controls.Add(this.SavedRoundsCountLabel);
            this.Controls.Add(this.TabController);
            this.Controls.Add(this.SavedRoundsLabel);
            this.MaximumSize = new System.Drawing.Size(800, 800);
            this.MinimumSize = new System.Drawing.Size(600, 600);
            this.Name = "Stronghold";
            this.Text = "Data Collection";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Stronghold_FormClosing);
            this.TabController.ResumeLayout(false);
            this.RoundTabPage.ResumeLayout(false);
            this.DefensesGroupBox.ResumeLayout(false);
            this.DefensesGroupBox.PerformLayout();
            this.DetailsGroupBox.ResumeLayout(false);
            this.DetailsGroupBox.PerformLayout();
            this.AutonomousTabPage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.AutonomousGroupBox.ResumeLayout(false);
            this.AutonomousGroupBox.PerformLayout();
            this.TeleopTabPage.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TeleopHighGoalsScoredNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopHighGoalsMissedNumericUpDown)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TeleopLowGoalsScoredNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopLowGoalsMissedNumericUpDown)).EndInit();
            this.TeleopGroupBox.ResumeLayout(false);
            this.TeleopGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeleopDefenceNumericUpDown0)).EndInit();
            this.RatingTabPage.ResumeLayout(false);
            this.PerformanceGroupBox.ResumeLayout(false);
            this.PerformanceGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AssistanceAbilityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ScoringAbilityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CrossingAbilityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AutonomousTrackBar)).EndInit();
            this.SummaryTabPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TabControl TabController;
        internal System.Windows.Forms.TabPage RoundTabPage;
        internal System.Windows.Forms.Button RoundNextButton;
        internal System.Windows.Forms.GroupBox DefensesGroupBox;
        internal System.Windows.Forms.CheckBox SallyPortCheckbox;
        internal System.Windows.Forms.CheckBox RoughTerrainCheckbox;
        internal System.Windows.Forms.CheckBox RockWallCheckbox;
        internal System.Windows.Forms.CheckBox RampartsCheckbox;
        internal System.Windows.Forms.CheckBox PortcullisCheckbox;
        internal System.Windows.Forms.CheckBox MoatCheckbox;
        internal System.Windows.Forms.CheckBox DrawbridgeCheckbox;
        internal System.Windows.Forms.CheckBox ChevalDeFriseCheckbox;
        internal System.Windows.Forms.GroupBox DetailsGroupBox;
        internal System.Windows.Forms.TextBox RobotTextBox;
        internal System.Windows.Forms.Label RobotLabel;
        internal System.Windows.Forms.TextBox RoundTextBox;
        internal System.Windows.Forms.Label RoundLabel;
        internal System.Windows.Forms.TabPage AutonomousTabPage;
        internal System.Windows.Forms.Button AutonomousNextButton;
        internal System.Windows.Forms.GroupBox AutonomousGroupBox;
        internal System.Windows.Forms.CheckBox HighGoalCheckbox;
        internal System.Windows.Forms.CheckBox LowGoalCheckbox;
        internal System.Windows.Forms.CheckBox DefenseReachedCheckbox;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TabPage TeleopTabPage;
        internal System.Windows.Forms.GroupBox TeleopGroupBox;
        private System.Windows.Forms.CheckBox SuccessfulChallengeCheckbox;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox teleopAssistanceCheckbox4;
        private System.Windows.Forms.CheckBox teleopAssistanceCheckbox3;
        private System.Windows.Forms.CheckBox teleopAssistanceCheckbox2;
        private System.Windows.Forms.CheckBox teleopAssistanceCheckbox1;
        private System.Windows.Forms.CheckBox teleopAssistanceCheckbox0;
        private System.Windows.Forms.ComboBox teleopDefenceCombo4;
        private System.Windows.Forms.ComboBox teleopDefenceCombo3;
        private System.Windows.Forms.ComboBox teleopDefenceCombo2;
        private System.Windows.Forms.ComboBox teleopDefenceCombo1;
        private System.Windows.Forms.ComboBox teleopDefenceCombo0;
        private System.Windows.Forms.Label TeleopDefenceLabel4;
        private System.Windows.Forms.Label TeleopDefenceLabel3;
        private System.Windows.Forms.Label TeleopDefenceLabel2;
        private System.Windows.Forms.Label TeleopDefenceLabel1;
        private System.Windows.Forms.Label TeleopDefenceLabel0;
        internal System.Windows.Forms.Label TeleopHighGoalsCountLabel;
        internal System.Windows.Forms.CheckBox ClimbCheckbox;
        internal System.Windows.Forms.CheckBox PickupCheckbox;
        internal System.Windows.Forms.Button TeleopNextButton;
        internal System.Windows.Forms.TabPage RatingTabPage;
        private System.Windows.Forms.GroupBox PerformanceGroupBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TrackBar AssistanceAbilityTrackBar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TrackBar ScoringAbilityTrackBar;
        private System.Windows.Forms.TrackBar CrossingAbilityTrackBar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TrackBar AutonomousTrackBar;
        private System.Windows.Forms.Label SavedRoundsLabel;
        private System.Windows.Forms.Label SavedRoundsCountLabel;
        private System.Windows.Forms.NumericUpDown TeleopDefenceNumericUpDown0;
        private System.Windows.Forms.NumericUpDown TeleopDefenceNumericUpDown3;
        private System.Windows.Forms.NumericUpDown TeleopDefenceNumericUpDown2;
        private System.Windows.Forms.NumericUpDown TeleopDefenceNumericUpDown1;
        private System.Windows.Forms.NumericUpDown TeleopDefenceNumericUpDown4;
        private System.Windows.Forms.NumericUpDown TeleopHighGoalsScoredNumericUpDown;
        private System.Windows.Forms.NumericUpDown TeleopHighGoalsMissedNumericUpDown;
        private System.Windows.Forms.NumericUpDown TeleopLowGoalsScoredNumericUpDown;
        private System.Windows.Forms.NumericUpDown TeleopLowGoalsMissedNumericUpDown;
        internal System.Windows.Forms.CheckBox AutoDefenceCheckBox4;
        internal System.Windows.Forms.CheckBox AutoDefenceCheckBox3;
        internal System.Windows.Forms.CheckBox AutoDefenceCheckBox0;
        internal System.Windows.Forms.CheckBox AutoDefenceCheckBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.CheckBox AutoDefenceCheckBox2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        internal System.Windows.Forms.Button RatingNextButton;
        private System.Windows.Forms.TabPage SummaryTabPage;
        internal System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Button SaveAndExportButton;
        private System.Windows.Forms.Button SaveAndRestartButton;
        internal System.Windows.Forms.Label ReportLabel;
    }
}

