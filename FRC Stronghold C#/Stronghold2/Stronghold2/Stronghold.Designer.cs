﻿namespace Stronghold2
{
    partial class Stronghold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roundTabPage = new System.Windows.Forms.TabPage();
            this.RoundNextButton = new System.Windows.Forms.Button();
            this.DefensesGroupBox = new System.Windows.Forms.GroupBox();
            this.SallyPortCheckbox = new System.Windows.Forms.CheckBox();
            this.RoughTerrainCheckbox = new System.Windows.Forms.CheckBox();
            this.RockWallCheckbox = new System.Windows.Forms.CheckBox();
            this.RampartsCheckbox = new System.Windows.Forms.CheckBox();
            this.PortcullisCheckbox = new System.Windows.Forms.CheckBox();
            this.MoatCheckbox = new System.Windows.Forms.CheckBox();
            this.DrawbridgeCheckbox = new System.Windows.Forms.CheckBox();
            this.ChevalDeFriseCheckbox = new System.Windows.Forms.CheckBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.DetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.RobotTextBox = new System.Windows.Forms.TextBox();
            this.RobotLabel = new System.Windows.Forms.Label();
            this.RoundTextBox = new System.Windows.Forms.TextBox();
            this.RoundLabel = new System.Windows.Forms.Label();
            this.TeleopHighGoalsCountTextbox = new System.Windows.Forms.TextBox();
            this.TeleopHighGoalsCountLabel = new System.Windows.Forms.Label();
            this.ClimbCheckbox = new System.Windows.Forms.CheckBox();
            this.TeleopHighGoalsCheckbox = new System.Windows.Forms.CheckBox();
            this.TeleopLowGoalsCheckbox = new System.Windows.Forms.CheckBox();
            this.PickupCheckbox = new System.Windows.Forms.CheckBox();
            this.DefenseCheckBox4 = new System.Windows.Forms.CheckBox();
            this.DefenseCheckBox3 = new System.Windows.Forms.CheckBox();
            this.autonomousTabPage = new System.Windows.Forms.TabPage();
            this.AutonomousNextButton = new System.Windows.Forms.Button();
            this.AutonomousGroupBox = new System.Windows.Forms.GroupBox();
            this.HighGoalCheckbox = new System.Windows.Forms.CheckBox();
            this.LowGoalCheckbox = new System.Windows.Forms.CheckBox();
            this.DefenseCrossedCheckbox = new System.Windows.Forms.CheckBox();
            this.DefenseReachedCheckbox = new System.Windows.Forms.CheckBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.teleopTabPage = new System.Windows.Forms.TabPage();
            this.TeleopNextButton = new System.Windows.Forms.Button();
            this.TeleopGroupBox = new System.Windows.Forms.GroupBox();
            this.DefenseCheckBox2 = new System.Windows.Forms.CheckBox();
            this.DefenseCheckBox1 = new System.Windows.Forms.CheckBox();
            this.DefenseCheckBox0 = new System.Windows.Forms.CheckBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.summaryTabPage = new System.Windows.Forms.TabPage();
            this.ReportLabel = new System.Windows.Forms.Label();
            this.RestartButton = new System.Windows.Forms.Button();
            this.TabController = new System.Windows.Forms.TabControl();
            this.roundTabPage.SuspendLayout();
            this.DefensesGroupBox.SuspendLayout();
            this.DetailsGroupBox.SuspendLayout();
            this.autonomousTabPage.SuspendLayout();
            this.AutonomousGroupBox.SuspendLayout();
            this.teleopTabPage.SuspendLayout();
            this.TeleopGroupBox.SuspendLayout();
            this.summaryTabPage.SuspendLayout();
            this.TabController.SuspendLayout();
            this.SuspendLayout();
            // 
            // roundTabPage
            // 
            this.roundTabPage.Controls.Add(this.RoundNextButton);
            this.roundTabPage.Controls.Add(this.DefensesGroupBox);
            this.roundTabPage.Controls.Add(this.DetailsGroupBox);
            this.roundTabPage.Location = new System.Drawing.Point(4, 22);
            this.roundTabPage.Name = "roundTabPage";
            this.roundTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.roundTabPage.Size = new System.Drawing.Size(292, 387);
            this.roundTabPage.TabIndex = 0;
            this.roundTabPage.Text = "Round Details";
            this.roundTabPage.UseVisualStyleBackColor = true;
            // 
            // RoundNextButton
            // 
            this.RoundNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RoundNextButton.Location = new System.Drawing.Point(206, 353);
            this.RoundNextButton.Name = "RoundNextButton";
            this.RoundNextButton.Size = new System.Drawing.Size(75, 23);
            this.RoundNextButton.TabIndex = 11;
            this.RoundNextButton.Text = "Next";
            this.RoundNextButton.UseVisualStyleBackColor = true;
            this.RoundNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // DefensesGroupBox
            // 
            this.DefensesGroupBox.Controls.Add(this.SallyPortCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RoughTerrainCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RockWallCheckbox);
            this.DefensesGroupBox.Controls.Add(this.RampartsCheckbox);
            this.DefensesGroupBox.Controls.Add(this.PortcullisCheckbox);
            this.DefensesGroupBox.Controls.Add(this.MoatCheckbox);
            this.DefensesGroupBox.Controls.Add(this.DrawbridgeCheckbox);
            this.DefensesGroupBox.Controls.Add(this.ChevalDeFriseCheckbox);
            this.DefensesGroupBox.Controls.Add(this.Label4);
            this.DefensesGroupBox.Location = new System.Drawing.Point(8, 102);
            this.DefensesGroupBox.Name = "DefensesGroupBox";
            this.DefensesGroupBox.Size = new System.Drawing.Size(275, 219);
            this.DefensesGroupBox.TabIndex = 6;
            this.DefensesGroupBox.TabStop = false;
            this.DefensesGroupBox.Text = "Defenses";
            // 
            // SallyPortCheckbox
            // 
            this.SallyPortCheckbox.AutoSize = true;
            this.SallyPortCheckbox.Location = new System.Drawing.Point(10, 189);
            this.SallyPortCheckbox.Name = "SallyPortCheckbox";
            this.SallyPortCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.SallyPortCheckbox.Size = new System.Drawing.Size(70, 17);
            this.SallyPortCheckbox.TabIndex = 10;
            this.SallyPortCheckbox.Text = "Sally Port";
            this.SallyPortCheckbox.UseVisualStyleBackColor = true;
            this.SallyPortCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // RoughTerrainCheckbox
            // 
            this.RoughTerrainCheckbox.AutoSize = true;
            this.RoughTerrainCheckbox.Location = new System.Drawing.Point(10, 166);
            this.RoughTerrainCheckbox.Name = "RoughTerrainCheckbox";
            this.RoughTerrainCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RoughTerrainCheckbox.Size = new System.Drawing.Size(94, 17);
            this.RoughTerrainCheckbox.TabIndex = 9;
            this.RoughTerrainCheckbox.Text = "Rough Terrain";
            this.RoughTerrainCheckbox.UseVisualStyleBackColor = true;
            this.RoughTerrainCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // RockWallCheckbox
            // 
            this.RockWallCheckbox.AutoSize = true;
            this.RockWallCheckbox.Location = new System.Drawing.Point(10, 143);
            this.RockWallCheckbox.Name = "RockWallCheckbox";
            this.RockWallCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RockWallCheckbox.Size = new System.Drawing.Size(76, 17);
            this.RockWallCheckbox.TabIndex = 8;
            this.RockWallCheckbox.Text = "Rock Wall";
            this.RockWallCheckbox.UseVisualStyleBackColor = true;
            this.RockWallCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // RampartsCheckbox
            // 
            this.RampartsCheckbox.AutoSize = true;
            this.RampartsCheckbox.Location = new System.Drawing.Point(10, 120);
            this.RampartsCheckbox.Name = "RampartsCheckbox";
            this.RampartsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RampartsCheckbox.Size = new System.Drawing.Size(71, 17);
            this.RampartsCheckbox.TabIndex = 7;
            this.RampartsCheckbox.Text = "Ramparts";
            this.RampartsCheckbox.UseVisualStyleBackColor = true;
            this.RampartsCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // PortcullisCheckbox
            // 
            this.PortcullisCheckbox.AutoSize = true;
            this.PortcullisCheckbox.Location = new System.Drawing.Point(10, 97);
            this.PortcullisCheckbox.Name = "PortcullisCheckbox";
            this.PortcullisCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PortcullisCheckbox.Size = new System.Drawing.Size(68, 17);
            this.PortcullisCheckbox.TabIndex = 6;
            this.PortcullisCheckbox.Text = "Portcullis";
            this.PortcullisCheckbox.UseVisualStyleBackColor = true;
            this.PortcullisCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // MoatCheckbox
            // 
            this.MoatCheckbox.AutoSize = true;
            this.MoatCheckbox.Location = new System.Drawing.Point(10, 74);
            this.MoatCheckbox.Name = "MoatCheckbox";
            this.MoatCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.MoatCheckbox.Size = new System.Drawing.Size(50, 17);
            this.MoatCheckbox.TabIndex = 5;
            this.MoatCheckbox.Text = "Moat";
            this.MoatCheckbox.UseVisualStyleBackColor = true;
            this.MoatCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // DrawbridgeCheckbox
            // 
            this.DrawbridgeCheckbox.AutoSize = true;
            this.DrawbridgeCheckbox.Location = new System.Drawing.Point(10, 50);
            this.DrawbridgeCheckbox.Name = "DrawbridgeCheckbox";
            this.DrawbridgeCheckbox.Size = new System.Drawing.Size(80, 17);
            this.DrawbridgeCheckbox.TabIndex = 4;
            this.DrawbridgeCheckbox.Text = "Drawbridge";
            this.DrawbridgeCheckbox.UseVisualStyleBackColor = true;
            this.DrawbridgeCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // ChevalDeFriseCheckbox
            // 
            this.ChevalDeFriseCheckbox.AutoSize = true;
            this.ChevalDeFriseCheckbox.Location = new System.Drawing.Point(10, 27);
            this.ChevalDeFriseCheckbox.Name = "ChevalDeFriseCheckbox";
            this.ChevalDeFriseCheckbox.Size = new System.Drawing.Size(99, 17);
            this.ChevalDeFriseCheckbox.TabIndex = 3;
            this.ChevalDeFriseCheckbox.Text = "Cheval de Frise";
            this.ChevalDeFriseCheckbox.UseVisualStyleBackColor = true;
            this.ChevalDeFriseCheckbox.CheckedChanged += new System.EventHandler(this.defenceSelection_CheckedChanged);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(121, 27);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(100, 16);
            this.Label4.TabIndex = 0;
            // 
            // DetailsGroupBox
            // 
            this.DetailsGroupBox.Controls.Add(this.RobotTextBox);
            this.DetailsGroupBox.Controls.Add(this.RobotLabel);
            this.DetailsGroupBox.Controls.Add(this.RoundTextBox);
            this.DetailsGroupBox.Controls.Add(this.RoundLabel);
            this.DetailsGroupBox.Location = new System.Drawing.Point(8, 6);
            this.DetailsGroupBox.Name = "DetailsGroupBox";
            this.DetailsGroupBox.Size = new System.Drawing.Size(275, 90);
            this.DetailsGroupBox.TabIndex = 1;
            this.DetailsGroupBox.TabStop = false;
            this.DetailsGroupBox.Text = "Details";
            // 
            // RobotTextBox
            // 
            this.RobotTextBox.Location = new System.Drawing.Point(62, 53);
            this.RobotTextBox.Name = "RobotTextBox";
            this.RobotTextBox.Size = new System.Drawing.Size(207, 20);
            this.RobotTextBox.TabIndex = 2;
            this.RobotTextBox.TextChanged += new System.EventHandler(this.RoundTextBox_TextChanged);
            // 
            // RobotLabel
            // 
            this.RobotLabel.Location = new System.Drawing.Point(6, 53);
            this.RobotLabel.Name = "RobotLabel";
            this.RobotLabel.Size = new System.Drawing.Size(50, 20);
            this.RobotLabel.TabIndex = 2;
            this.RobotLabel.Text = "Robot";
            this.RobotLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RoundTextBox
            // 
            this.RoundTextBox.Location = new System.Drawing.Point(62, 27);
            this.RoundTextBox.Name = "RoundTextBox";
            this.RoundTextBox.Size = new System.Drawing.Size(207, 20);
            this.RoundTextBox.TabIndex = 1;
            this.RoundTextBox.TextChanged += new System.EventHandler(this.RoundTextBox_TextChanged);
            // 
            // RoundLabel
            // 
            this.RoundLabel.Location = new System.Drawing.Point(6, 27);
            this.RoundLabel.Name = "RoundLabel";
            this.RoundLabel.Size = new System.Drawing.Size(50, 20);
            this.RoundLabel.TabIndex = 0;
            this.RoundLabel.Text = "Round";
            this.RoundLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TeleopHighGoalsCountTextbox
            // 
            this.TeleopHighGoalsCountTextbox.Location = new System.Drawing.Point(60, 233);
            this.TeleopHighGoalsCountTextbox.Name = "TeleopHighGoalsCountTextbox";
            this.TeleopHighGoalsCountTextbox.Size = new System.Drawing.Size(49, 20);
            this.TeleopHighGoalsCountTextbox.TabIndex = 9;
            this.TeleopHighGoalsCountTextbox.TextChanged += new System.EventHandler(this.TeleopHighGoalsCountTextbox_TextChanged);
            this.TeleopHighGoalsCountTextbox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TeleopHighGoalsCountTextbox_KeyPress);
            // 
            // TeleopHighGoalsCountLabel
            // 
            this.TeleopHighGoalsCountLabel.Location = new System.Drawing.Point(4, 233);
            this.TeleopHighGoalsCountLabel.Name = "TeleopHighGoalsCountLabel";
            this.TeleopHighGoalsCountLabel.Size = new System.Drawing.Size(50, 20);
            this.TeleopHighGoalsCountLabel.TabIndex = 15;
            this.TeleopHighGoalsCountLabel.Text = "Count";
            this.TeleopHighGoalsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ClimbCheckbox
            // 
            this.ClimbCheckbox.AutoSize = true;
            this.ClimbCheckbox.Location = new System.Drawing.Point(9, 272);
            this.ClimbCheckbox.Name = "ClimbCheckbox";
            this.ClimbCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ClimbCheckbox.Size = new System.Drawing.Size(51, 17);
            this.ClimbCheckbox.TabIndex = 10;
            this.ClimbCheckbox.Text = "Climb";
            this.ClimbCheckbox.UseVisualStyleBackColor = true;
            // 
            // TeleopHighGoalsCheckbox
            // 
            this.TeleopHighGoalsCheckbox.AutoSize = true;
            this.TeleopHighGoalsCheckbox.Location = new System.Drawing.Point(10, 210);
            this.TeleopHighGoalsCheckbox.Name = "TeleopHighGoalsCheckbox";
            this.TeleopHighGoalsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TeleopHighGoalsCheckbox.Size = new System.Drawing.Size(78, 17);
            this.TeleopHighGoalsCheckbox.TabIndex = 8;
            this.TeleopHighGoalsCheckbox.Text = "High Goals";
            this.TeleopHighGoalsCheckbox.UseVisualStyleBackColor = true;
            // 
            // TeleopLowGoalsCheckbox
            // 
            this.TeleopLowGoalsCheckbox.AutoSize = true;
            this.TeleopLowGoalsCheckbox.Location = new System.Drawing.Point(10, 187);
            this.TeleopLowGoalsCheckbox.Name = "TeleopLowGoalsCheckbox";
            this.TeleopLowGoalsCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.TeleopLowGoalsCheckbox.Size = new System.Drawing.Size(76, 17);
            this.TeleopLowGoalsCheckbox.TabIndex = 7;
            this.TeleopLowGoalsCheckbox.Text = "Low Goals";
            this.TeleopLowGoalsCheckbox.UseVisualStyleBackColor = true;
            // 
            // PickupCheckbox
            // 
            this.PickupCheckbox.AutoSize = true;
            this.PickupCheckbox.Location = new System.Drawing.Point(10, 152);
            this.PickupCheckbox.Name = "PickupCheckbox";
            this.PickupCheckbox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.PickupCheckbox.Size = new System.Drawing.Size(59, 17);
            this.PickupCheckbox.TabIndex = 6;
            this.PickupCheckbox.Text = "Pickup";
            this.PickupCheckbox.UseVisualStyleBackColor = true;
            // 
            // DefenseCheckBox4
            // 
            this.DefenseCheckBox4.Location = new System.Drawing.Point(10, 119);
            this.DefenseCheckBox4.Name = "DefenseCheckBox4";
            this.DefenseCheckBox4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DefenseCheckBox4.Size = new System.Drawing.Size(150, 17);
            this.DefenseCheckBox4.TabIndex = 5;
            this.DefenseCheckBox4.Text = "Defense 4";
            this.DefenseCheckBox4.UseVisualStyleBackColor = true;
            // 
            // DefenseCheckBox3
            // 
            this.DefenseCheckBox3.Location = new System.Drawing.Point(10, 96);
            this.DefenseCheckBox3.Name = "DefenseCheckBox3";
            this.DefenseCheckBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DefenseCheckBox3.Size = new System.Drawing.Size(150, 17);
            this.DefenseCheckBox3.TabIndex = 4;
            this.DefenseCheckBox3.Text = "Defense 3";
            this.DefenseCheckBox3.UseVisualStyleBackColor = true;
            // 
            // autonomousTabPage
            // 
            this.autonomousTabPage.Controls.Add(this.AutonomousNextButton);
            this.autonomousTabPage.Controls.Add(this.AutonomousGroupBox);
            this.autonomousTabPage.Location = new System.Drawing.Point(4, 22);
            this.autonomousTabPage.Name = "autonomousTabPage";
            this.autonomousTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.autonomousTabPage.Size = new System.Drawing.Size(292, 387);
            this.autonomousTabPage.TabIndex = 1;
            this.autonomousTabPage.Text = "Autonomous";
            this.autonomousTabPage.UseVisualStyleBackColor = true;
            // 
            // AutonomousNextButton
            // 
            this.AutonomousNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AutonomousNextButton.Location = new System.Drawing.Point(206, 353);
            this.AutonomousNextButton.Name = "AutonomousNextButton";
            this.AutonomousNextButton.Size = new System.Drawing.Size(75, 23);
            this.AutonomousNextButton.TabIndex = 5;
            this.AutonomousNextButton.Text = "Next";
            this.AutonomousNextButton.UseVisualStyleBackColor = true;
            this.AutonomousNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // AutonomousGroupBox
            // 
            this.AutonomousGroupBox.Controls.Add(this.HighGoalCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.LowGoalCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.DefenseCrossedCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.DefenseReachedCheckbox);
            this.AutonomousGroupBox.Controls.Add(this.Label1);
            this.AutonomousGroupBox.Location = new System.Drawing.Point(8, 6);
            this.AutonomousGroupBox.Name = "AutonomousGroupBox";
            this.AutonomousGroupBox.Size = new System.Drawing.Size(275, 122);
            this.AutonomousGroupBox.TabIndex = 2;
            this.AutonomousGroupBox.TabStop = false;
            this.AutonomousGroupBox.Text = "Autonomous";
            // 
            // HighGoalCheckbox
            // 
            this.HighGoalCheckbox.AutoSize = true;
            this.HighGoalCheckbox.Location = new System.Drawing.Point(9, 92);
            this.HighGoalCheckbox.Name = "HighGoalCheckbox";
            this.HighGoalCheckbox.Size = new System.Drawing.Size(73, 17);
            this.HighGoalCheckbox.TabIndex = 4;
            this.HighGoalCheckbox.Text = "High Goal";
            this.HighGoalCheckbox.UseVisualStyleBackColor = true;
            // 
            // LowGoalCheckbox
            // 
            this.LowGoalCheckbox.AutoSize = true;
            this.LowGoalCheckbox.Location = new System.Drawing.Point(9, 69);
            this.LowGoalCheckbox.Name = "LowGoalCheckbox";
            this.LowGoalCheckbox.Size = new System.Drawing.Size(71, 17);
            this.LowGoalCheckbox.TabIndex = 3;
            this.LowGoalCheckbox.Text = "Low Goal";
            this.LowGoalCheckbox.UseVisualStyleBackColor = true;
            // 
            // DefenseCrossedCheckbox
            // 
            this.DefenseCrossedCheckbox.AutoSize = true;
            this.DefenseCrossedCheckbox.Location = new System.Drawing.Point(9, 46);
            this.DefenseCrossedCheckbox.Name = "DefenseCrossedCheckbox";
            this.DefenseCrossedCheckbox.Size = new System.Drawing.Size(107, 17);
            this.DefenseCrossedCheckbox.TabIndex = 2;
            this.DefenseCrossedCheckbox.Text = "Defense Crossed";
            this.DefenseCrossedCheckbox.UseVisualStyleBackColor = true;
            this.DefenseCrossedCheckbox.CheckedChanged += new System.EventHandler(this.DefenseCrossedCheckbox_CheckedChanged);
            // 
            // DefenseReachedCheckbox
            // 
            this.DefenseReachedCheckbox.AutoSize = true;
            this.DefenseReachedCheckbox.Location = new System.Drawing.Point(9, 23);
            this.DefenseReachedCheckbox.Name = "DefenseReachedCheckbox";
            this.DefenseReachedCheckbox.Size = new System.Drawing.Size(113, 17);
            this.DefenseReachedCheckbox.TabIndex = 1;
            this.DefenseReachedCheckbox.Text = "Defense Reached";
            this.DefenseReachedCheckbox.UseVisualStyleBackColor = true;
            this.DefenseReachedCheckbox.CheckedChanged += new System.EventHandler(this.DefenseReachedCheckbox_CheckedChanged);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(6, 27);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(100, 16);
            this.Label1.TabIndex = 0;
            // 
            // teleopTabPage
            // 
            this.teleopTabPage.Controls.Add(this.TeleopNextButton);
            this.teleopTabPage.Controls.Add(this.TeleopGroupBox);
            this.teleopTabPage.Location = new System.Drawing.Point(4, 22);
            this.teleopTabPage.Name = "teleopTabPage";
            this.teleopTabPage.Size = new System.Drawing.Size(292, 387);
            this.teleopTabPage.TabIndex = 2;
            this.teleopTabPage.Text = "Teleop";
            this.teleopTabPage.UseVisualStyleBackColor = true;
            // 
            // TeleopNextButton
            // 
            this.TeleopNextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TeleopNextButton.Location = new System.Drawing.Point(206, 353);
            this.TeleopNextButton.Name = "TeleopNextButton";
            this.TeleopNextButton.Size = new System.Drawing.Size(75, 23);
            this.TeleopNextButton.TabIndex = 11;
            this.TeleopNextButton.Text = "Next";
            this.TeleopNextButton.UseVisualStyleBackColor = true;
            this.TeleopNextButton.Click += new System.EventHandler(this.NextTab_Click);
            // 
            // TeleopGroupBox
            // 
            this.TeleopGroupBox.Controls.Add(this.TeleopHighGoalsCountTextbox);
            this.TeleopGroupBox.Controls.Add(this.TeleopHighGoalsCountLabel);
            this.TeleopGroupBox.Controls.Add(this.ClimbCheckbox);
            this.TeleopGroupBox.Controls.Add(this.TeleopHighGoalsCheckbox);
            this.TeleopGroupBox.Controls.Add(this.TeleopLowGoalsCheckbox);
            this.TeleopGroupBox.Controls.Add(this.PickupCheckbox);
            this.TeleopGroupBox.Controls.Add(this.DefenseCheckBox4);
            this.TeleopGroupBox.Controls.Add(this.DefenseCheckBox3);
            this.TeleopGroupBox.Controls.Add(this.DefenseCheckBox2);
            this.TeleopGroupBox.Controls.Add(this.DefenseCheckBox1);
            this.TeleopGroupBox.Controls.Add(this.DefenseCheckBox0);
            this.TeleopGroupBox.Controls.Add(this.Label2);
            this.TeleopGroupBox.Location = new System.Drawing.Point(8, 6);
            this.TeleopGroupBox.Name = "TeleopGroupBox";
            this.TeleopGroupBox.Size = new System.Drawing.Size(275, 303);
            this.TeleopGroupBox.TabIndex = 3;
            this.TeleopGroupBox.TabStop = false;
            this.TeleopGroupBox.Text = "Operator Control";
            // 
            // DefenseCheckBox2
            // 
            this.DefenseCheckBox2.Location = new System.Drawing.Point(10, 73);
            this.DefenseCheckBox2.Name = "DefenseCheckBox2";
            this.DefenseCheckBox2.Size = new System.Drawing.Size(150, 17);
            this.DefenseCheckBox2.TabIndex = 3;
            this.DefenseCheckBox2.Text = "Defense 2";
            this.DefenseCheckBox2.UseVisualStyleBackColor = true;
            // 
            // DefenseCheckBox1
            // 
            this.DefenseCheckBox1.Location = new System.Drawing.Point(10, 50);
            this.DefenseCheckBox1.Name = "DefenseCheckBox1";
            this.DefenseCheckBox1.Size = new System.Drawing.Size(150, 17);
            this.DefenseCheckBox1.TabIndex = 2;
            this.DefenseCheckBox1.Text = "Defense 1";
            this.DefenseCheckBox1.UseVisualStyleBackColor = true;
            // 
            // DefenseCheckBox0
            // 
            this.DefenseCheckBox0.Location = new System.Drawing.Point(10, 27);
            this.DefenseCheckBox0.Name = "DefenseCheckBox0";
            this.DefenseCheckBox0.Size = new System.Drawing.Size(150, 17);
            this.DefenseCheckBox0.TabIndex = 1;
            this.DefenseCheckBox0.Text = "Defense 0";
            this.DefenseCheckBox0.UseVisualStyleBackColor = true;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(121, 27);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(100, 16);
            this.Label2.TabIndex = 0;
            // 
            // summaryTabPage
            // 
            this.summaryTabPage.Controls.Add(this.ReportLabel);
            this.summaryTabPage.Controls.Add(this.RestartButton);
            this.summaryTabPage.Location = new System.Drawing.Point(4, 22);
            this.summaryTabPage.Name = "summaryTabPage";
            this.summaryTabPage.Size = new System.Drawing.Size(292, 387);
            this.summaryTabPage.TabIndex = 3;
            this.summaryTabPage.Text = "Summary";
            this.summaryTabPage.UseVisualStyleBackColor = true;
            // 
            // ReportLabel
            // 
            this.ReportLabel.Location = new System.Drawing.Point(12, 15);
            this.ReportLabel.Name = "ReportLabel";
            this.ReportLabel.Size = new System.Drawing.Size(266, 330);
            this.ReportLabel.TabIndex = 9;
            this.ReportLabel.Text = "Label3";
            // 
            // RestartButton
            // 
            this.RestartButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RestartButton.Location = new System.Drawing.Point(8, 354);
            this.RestartButton.Name = "RestartButton";
            this.RestartButton.Size = new System.Drawing.Size(75, 23);
            this.RestartButton.TabIndex = 8;
            this.RestartButton.Text = "Restart";
            this.RestartButton.UseVisualStyleBackColor = true;
            this.RestartButton.Click += new System.EventHandler(this.RestartButton_Click);
            // 
            // TabController
            // 
            this.TabController.Controls.Add(this.roundTabPage);
            this.TabController.Controls.Add(this.autonomousTabPage);
            this.TabController.Controls.Add(this.teleopTabPage);
            this.TabController.Controls.Add(this.summaryTabPage);
            this.TabController.Location = new System.Drawing.Point(0, 1);
            this.TabController.Name = "TabController";
            this.TabController.SelectedIndex = 0;
            this.TabController.Size = new System.Drawing.Size(300, 413);
            this.TabController.TabIndex = 6;
            this.TabController.SelectedIndexChanged += new System.EventHandler(this.TabController_SelectedIndexChanged);
            // 
            // Stronghold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 415);
            this.Controls.Add(this.TabController);
            this.Name = "Stronghold";
            this.Text = "Stronghold";
            this.roundTabPage.ResumeLayout(false);
            this.DefensesGroupBox.ResumeLayout(false);
            this.DefensesGroupBox.PerformLayout();
            this.DetailsGroupBox.ResumeLayout(false);
            this.DetailsGroupBox.PerformLayout();
            this.autonomousTabPage.ResumeLayout(false);
            this.AutonomousGroupBox.ResumeLayout(false);
            this.AutonomousGroupBox.PerformLayout();
            this.teleopTabPage.ResumeLayout(false);
            this.TeleopGroupBox.ResumeLayout(false);
            this.TeleopGroupBox.PerformLayout();
            this.summaryTabPage.ResumeLayout(false);
            this.TabController.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.TabPage roundTabPage;
        internal System.Windows.Forms.Button RoundNextButton;
        internal System.Windows.Forms.GroupBox DefensesGroupBox;
        internal System.Windows.Forms.CheckBox SallyPortCheckbox;
        internal System.Windows.Forms.CheckBox RoughTerrainCheckbox;
        internal System.Windows.Forms.CheckBox RockWallCheckbox;
        internal System.Windows.Forms.CheckBox RampartsCheckbox;
        internal System.Windows.Forms.CheckBox PortcullisCheckbox;
        internal System.Windows.Forms.CheckBox MoatCheckbox;
        internal System.Windows.Forms.CheckBox DrawbridgeCheckbox;
        internal System.Windows.Forms.CheckBox ChevalDeFriseCheckbox;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.GroupBox DetailsGroupBox;
        internal System.Windows.Forms.TextBox RobotTextBox;
        internal System.Windows.Forms.Label RobotLabel;
        internal System.Windows.Forms.TextBox RoundTextBox;
        internal System.Windows.Forms.Label RoundLabel;
        internal System.Windows.Forms.TextBox TeleopHighGoalsCountTextbox;
        internal System.Windows.Forms.Label TeleopHighGoalsCountLabel;
        internal System.Windows.Forms.CheckBox ClimbCheckbox;
        internal System.Windows.Forms.CheckBox TeleopHighGoalsCheckbox;
        internal System.Windows.Forms.CheckBox TeleopLowGoalsCheckbox;
        internal System.Windows.Forms.CheckBox PickupCheckbox;
        internal System.Windows.Forms.CheckBox DefenseCheckBox4;
        internal System.Windows.Forms.CheckBox DefenseCheckBox3;
        internal System.Windows.Forms.TabPage autonomousTabPage;
        internal System.Windows.Forms.Button AutonomousNextButton;
        internal System.Windows.Forms.GroupBox AutonomousGroupBox;
        internal System.Windows.Forms.CheckBox HighGoalCheckbox;
        internal System.Windows.Forms.CheckBox LowGoalCheckbox;
        internal System.Windows.Forms.CheckBox DefenseCrossedCheckbox;
        internal System.Windows.Forms.CheckBox DefenseReachedCheckbox;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TabPage teleopTabPage;
        internal System.Windows.Forms.Button TeleopNextButton;
        internal System.Windows.Forms.GroupBox TeleopGroupBox;
        internal System.Windows.Forms.CheckBox DefenseCheckBox2;
        internal System.Windows.Forms.CheckBox DefenseCheckBox1;
        internal System.Windows.Forms.CheckBox DefenseCheckBox0;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TabPage summaryTabPage;
        internal System.Windows.Forms.Label ReportLabel;
        internal System.Windows.Forms.Button RestartButton;
        internal System.Windows.Forms.TabControl TabController;
    }
}

