﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stronghold2
{
    public partial class Stronghold : Form
    {
        #region Initalise

        // Arrays to hold the defense types and checkboxes
        private CheckBox[] defenceTypes;
        private List<String> defencesChosen;
        private CheckBox[] defencesUsed = new CheckBox[5];

        public Stronghold()
        {
            InitializeComponent();

            // Set up the arrays to hold the defense checkboxes that we can choose
            defenceTypes = new CheckBox[] {
                this.ChevalDeFriseCheckbox,
                this.DrawbridgeCheckbox,
                this.MoatCheckbox,
                this.PortcullisCheckbox,
                this.RampartsCheckbox,
                this.RockWallCheckbox,
                this.RoughTerrainCheckbox,
                this.SallyPortCheckbox };

            // Set up the arrays to hold the defense checkboxes that have been chosen
            this.defencesUsed = new CheckBox[] {
                this.DefenseCheckBox0,
                this.DefenseCheckBox1,
                this.DefenseCheckBox2,
                this.DefenseCheckBox3,
                this.DefenseCheckBox4};

            // Arraylist of the names of selected defenses
            this.defencesChosen = new List<String>();
            // The low bar is always present
            this.defencesChosen.Add("Low Bar");

            // Initialise the tabs
            this.checkEnableTabs();
        }
        #endregion

        #region Tab Management

        // Checks to see if the tabs can be displayed, hides them if not.
        private void NextTab_Click(object sender, EventArgs e)
        {
            TabController.SelectedIndex = this.TabController.SelectedIndex + 1;
        }

        // Checks to see if the tabs can be displayed, hides them if not.
        private void checkEnableTabs()
        {
            // Tabs are enabled if we have a robot, round, and four defenses selected.

            // Last step. If the user has selected four defenses (+ the low goal) we can enable the tabs. If not 
            // they should be disabled.
            if (this.defencesChosen.Count() == 5 && !RobotTextBox.Text.Equals("") && !RoundTextBox.Text.Equals(""))
            {
                this.TabController.TabPages.Insert(1, this.autonomousTabPage);
                this.TabController.TabPages.Insert(2, this.teleopTabPage);
                this.TabController.TabPages.Insert(3, this.summaryTabPage);
                this.RoundNextButton.Enabled = true;
            }
            else
            {
                this.TabController.TabPages.Remove(this.autonomousTabPage);
                this.TabController.TabPages.Remove(this.teleopTabPage);
                this.TabController.TabPages.Remove(this.summaryTabPage);
                this.RoundNextButton.Enabled = false;
                this.TabController.SelectedIndex = 0;
            }
        }

        private void TabController_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Time to generate a report? It is if we just entered the last tab.
            if (TabController.SelectedIndex == 3)
            {
                this.generateReport();
            }
        }

        #endregion

        #region Round and Robot

        // Not much here.Just a trigger to display tabs when text and defenses are ready.
        private void RoundTextBox_TextChanged(object sender, EventArgs e)
        {
            this.checkEnableTabs();
        }

        #endregion

        #region Defenses

        //  defenceselection_CheckedChanged handles the initial selection of defenses, making the selected defences avaialble in teleop mode
        private void defenceSelection_CheckedChanged(object sender, EventArgs e)
        {
            // Cast the sender to a CheckBox so we can grab the Text property and if it is checked
            CheckBox changedCheckbox = (CheckBox)sender;

            // Has it just been selected? If so, we need to store it as a selected defense
            // If not, remove it from the list of defenses
            if (changedCheckbox.Checked)
            {
                // Store the name only if we don't already have it
                if (!this.defencesChosen.Contains(changedCheckbox.Text))
                {
                    this.defencesChosen.Add(changedCheckbox.Text);
                }
            }
            else
            {
                // Remove the name if it was deselected
                defencesChosen.Remove(changedCheckbox.Text);
            }

            // Update the list of active defences

            // Index to keep track of which checkbox to update
            int defenceIndex = 0;

            // Put the defenses in alphabetical order
            this.defencesChosen.Sort();

            // Go through each defense, and add it as an option in teleop modde
            foreach (String defenceName in this.defencesChosen)
            {
                // Name the defences
                this.defencesUsed[defenceIndex].Text = defenceName;
                // Make sure it isn't selected
                this.defencesUsed[defenceIndex].Checked = false;
                // Increase the index for the next defence
                defenceIndex++;
            }

            // If five defenses are chosen (the low goal being one of them) we need to allow access to the tab page
            // and disable defenses which can't be used.If five haven't been chosen, ensure that all defense options
            // are available
            foreach (CheckBox defenceOption in this.defenceTypes)
            {
                if (defencesChosen.Count == 5)
                {
                    if (!defenceOption.Checked)
                    {
                        defenceOption.Enabled = false;
                    }
                }
                else
                {
                    defenceOption.Enabled = true;
                }
            }

            // Finally, as we've made changes, check to see if we need the tabs now
            this.checkEnableTabs();
        }
        #endregion

        #region Autonomous

        // Minor problem. If the defenses were crossed, they must have been reached. If not reached, couldn't have been crossed.
        private void DefenseCrossedCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.DefenseCrossedCheckbox.Checked)
            {
                this.DefenseReachedCheckbox.Checked = true;
            }
        }

        private void DefenseReachedCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.DefenseReachedCheckbox.Checked)
            {
                this.DefenseCrossedCheckbox.Checked = false;
            }
        }

        #endregion

        #region Number of Goals Scored

         private void TeleopHighGoalsCountTextbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.KeyChar = '\0';
            }
        }


         private void TeleopHighGoalsCountTextbox_TextChanged(object sender, EventArgs e)
         {
             if (!(TeleopHighGoalsCountTextbox.Text == ""))
                 if (TeleopHighGoalsCountTextbox.Text != "0" )
                 {
                     this.TeleopHighGoalsCheckbox.Checked = true;
                 }

             if (TeleopHighGoalsCountTextbox.Text == "0")
             {
                 this.TeleopHighGoalsCheckbox.Checked = false;
             }
         }




        #endregion

        #region Restart

         private void RestartButton_Click(object sender, EventArgs e)
         {
             // Clear the general information
             this.RoundTextBox.Text = "";
             this.RobotTextBox.Text = "";

             foreach (CheckBox defenceOption in this.defenceTypes)
             {
                 defenceOption.Checked = false;
                 defenceOption.Enabled = true;
             }

             // Clear autonomous
             this.DefenseReachedCheckbox.Checked = false;
             this.DefenseCrossedCheckbox.Checked = false;
             this.LowGoalCheckbox.Checked = false;
             this.HighGoalCheckbox.Checked = false;

             // Clear Teleop
             foreach (CheckBox defenceOption in this.defencesUsed)
             {
                 defenceOption.Checked = false;
             }

             this.TeleopLowGoalsCheckbox.Checked = false;
             this.TeleopHighGoalsCheckbox.Checked = false;
             this.TeleopHighGoalsCountTextbox.Text = "";

             this.PickupCheckbox.Checked = false;
             this.ClimbCheckbox.Checked = false;

             // Hide tabs
             this.checkEnableTabs();
         }
        

        #endregion

        #region Generate Report

        private void generateReport()
        {
            String reportText =  "";

            // Basic Details
            reportText = "Round: " + this.RoundTextBox.Text + "\n";
            reportText += "Robot: " + this.RobotTextBox.Text + "\n\n";

            // Autonomous Details
            reportText += "Autonomous: ";

            String autoText = "";
            if (this.DefenseCrossedCheckbox.Checked)
            {
                autoText += "Reached and crossed a defence. ";
            } else if (this.DefenseReachedCheckbox.Checked)
            {
                autoText += "Reached a defence but didn't cross. ";
            }

            if (this.LowGoalCheckbox.Checked)
            {
                autoText += "Scored a Low Goal. ";
            }

            if (this.HighGoalCheckbox.Checked)
            {
                autoText += "Scored a High Goal. ";
            }

            if (autoText.Equals(""))
            {
                autoText = "Did not score";
            }

            reportText += autoText + "\n\n";

            // Teleop Details
            reportText += "Teleop: ";

            String teleopText = "";
            List<String> defencesPassed = new List<String>();

            foreach (CheckBox defenceOption in this.defencesUsed)
            {
                // If the checkbox is selected, store the name of the defense that was passed
                if (defenceOption.Checked)
                {
                    defencesPassed.Add(defenceOption.Text);
                }
            }

            // How many defenses were crossed?

            if (defencesPassed.Count == 0)
            {
                teleopText += "No defences were crossed. ";
            } else if (defencesPassed.Count == 1)
            {
                teleopText += "The " + defencesPassed[0] + " was crossed. ";
            } else
            {
                teleopText += "The ";
                for(int i = 0; i <= defencesPassed.Count - 2; i++)
                {
                    teleopText += defencesPassed[i];
                    if (i == defencesPassed.Count - 2)
                    {
                        teleopText += " ";
                    } else
                    {
                        teleopText += ", ";
                    }
                }
                teleopText += "and " + defencesPassed[defencesPassed.Count - 1] + " were crossed. ";
            }

            reportText += teleopText + "\n\n";

            // How about the scoring?
            if (PickupCheckbox.Checked)
            {
                teleopText = "Can pick up, ";
            } else
            {
                teleopText = "Did not pick up, ";
            }

            // Did they score high or low?
            if (!this.TeleopLowGoalsCheckbox.Checked && !this.TeleopHighGoalsCheckbox.Checked)
            {
                teleopText += "did not shoot high or low goals.";
            } else
            {
                // So they shot at least a goal.
                teleopText += "can shoot ";

                // Was it a low goal?
                if (this.TeleopLowGoalsCheckbox.Checked)
                {
                    teleopText += "low goals";
                }

                //  Did they do both?
                if (this.TeleopLowGoalsCheckbox.Checked && this.TeleopHighGoalsCheckbox.Checked)
                {
                    teleopText += " and ";
                }


                if (this.TeleopHighGoalsCheckbox.Checked)
                {
                    // Score is optional. If it was entered, display it. Note that they can't have a 0 and have the checkbox checked
                    if (!this.TeleopHighGoalsCountTextbox.Text.Equals(""))
                    {
                        teleopText += "(" + this.TeleopHighGoalsCountTextbox.Text + ") ";
                    }
                    teleopText += "high goals";
                }
                teleopText += ".";
            }

            reportText += teleopText + "\n\n";

            // Finally, did it climb?
            if (this.ClimbCheckbox.Checked)
            {
                teleopText = "The robot can climb.";
            } else
            {
                teleopText = "The robot did not climb.";
            }

            reportText += teleopText + "\n\n";

            this.ReportLabel.Text = reportText;
        }

        #endregion

        private void RoundNextButton_Click(object sender, EventArgs e)
        {

        }










    }
}
