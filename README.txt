# Agile Assignment 2 #

## Contributors ##

* Carmen Grantham (gracy016)
* Deanna Veale (veadf001)

## Introduction ##

The Agile Assignment 2 required rewriting a provided VB.NET project in C#.NET. Adding some new features to data entry forms and adding Reporting functionality. The latter was implemented with a new project so that it can be run independantly of everything else.


## Project Structure ##


### FRC Stronghold ### 

This was the original VB.NET project used as a basis for converting to C#


### FRC Stronghold C# ###

The rewrite of the VB.NET project into C#.NET. (Including all existing logic errors)


### FRC Stronghold Improved ###

This includes all the new feature requests during data entry phase. Namely:


In Autonomous Mode
* Count number of times each defence was crossed

In Teleop mode
* Count number of times each defence was crossed
* Record if Sally Port or Drawbridge was crossed with assistance or not, and if assistance was provided.
* Record if challenge was successful

After round finished
* Rate robots performance
* Export robot round data into CSV


### Reporting ###

A new project to handle reporting. Data is imported from CSV and can be analysed to determine which robots to use as allies in future rounds.


### StrongholdLibrary ###

A library of classes that are used by *Reporting* and *FRC Stronghold Improved* projects.
This library is used by FRC Stronghold Improved and Reporting projects, so if either project fails because it can't find the files in the library this project needs to be opened and rebuilt. It should automatically add the StrongholdLibrary.dll file to the correct location.


### Errata ###

* Robot Round Handout.doc - The paper based form to enter data about the robot rounds. It has 2 pages. One in colour and the other in black and white. It is up to the person who needs these to determine which one to use.
* Reflection_CarmenGrantham.doc - The assignment reflection document by Carmen Grantham
* Deanna Veale will upload her reflection document separately.
