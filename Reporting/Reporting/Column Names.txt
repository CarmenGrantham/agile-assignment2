﻿            this.summaryListView.Columns.Add("Robot Number", 110);
            this.summaryListView.Columns.Add("Round", 110);
            this.summaryListView.Columns.Add("Scoring Rating", 110);
            this.summaryListView.Columns.Add("Crossing Rating", 110);
            this.summaryListView.Columns.Add("Autonomous Rating", 135);
            this.summaryListView.Columns.Add("Assistance Rating", 135);
			this.summaryListView.Columns.Add("Total Rating", 110);


			this.scoringListView.Columns.Add("Robot Number", 110);
            this.scoringListView.Columns.Add("Round", 110);
            this.scoringListView.Columns.Add("High Goals (Goals/Attempts)", 185);
            this.scoringListView.Columns.Add("Low Goals (Goals/Attempts)", 185);
            this.scoringListView.Columns.Add("Successful Challenge", 145);
            this.scoringListView.Columns.Add("Climb", 95);
            this.scoringListView.Columns.Add("Scoring Rating", 110);

            this.crossingListView.Columns.Add("Robot Number", 100);
            this.crossingListView.Columns.Add("Round", 70);
            this.crossingListView.Columns.Add("Defence 1", 165);
            this.crossingListView.Columns.Add("Defence 2", 165);
            this.crossingListView.Columns.Add("Defence 3", 165);
            this.crossingListView.Columns.Add("Defence 4", 165);
			this.crossingListView.Columns.Add("Defence 5", 165);
            this.crossingListView.Columns.Add("Total Crossings", 110);
            this.crossingListView.Columns.Add("Crossing Rating", 110);

			this.autonomousListView.Columns.Add("Robot Number", 110);
            this.autonomousListView.Columns.Add("Round", 110);
            this.autonomousListView.Columns.Add("Defence Reached", 135);
            this.autonomousListView.Columns.Add("Defence Crossed", 285);
            this.autonomousListView.Columns.Add("Low Goal", 110);
            this.autonomousListView.Columns.Add("High Goal", 110);
            this.autonomousListView.Columns.Add("Autonomous Rating", 135);

            this.assistanceListView.Columns.Add("Robot Number", 110);
            this.assistanceListView.Columns.Add("Round", 110);
            this.assistanceListView.Columns.Add("Assisted with Sally Port", 160);
            this.assistanceListView.Columns.Add("Assisted with Drawbridge", 170);
            this.assistanceListView.Columns.Add("Assistance Rating", 135);

