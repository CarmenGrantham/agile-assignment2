﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StrongholdLibrary;
using System.IO;
using System.Diagnostics;

namespace Reporting
{
    public partial class Form1 : Form
    {
        ReportingSession ReportingSession = new ReportingSession();
        // Store list of robots that have not been excluded
        List<RobotRound> RobotsChosen = new List<RobotRound>();
        List<RobotRound> Robots = new List<RobotRound>();


        public Form1()
        {
            InitializeComponent();
        }

        private void MakeDataButton_Click(object sender, EventArgs e)
        {
            // Test method to create some fake robots to report on.
            createRobotRounds();
            updateExcludedList();
            refreshListViews();

        }

        // Resorts listviews and updates robot visibility.
        private void refreshListViews()
        {
            
            refreshSummaryListView();
            refreshScoringListView();
            refreshCrossingListView();
            refreshAutonomousListView();
            refreshAssistanceListView();
        }

        private void refreshSummaryListView()
        {
            // Determine whether excluded robots are shown or not.
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
            }
            else
            {
                Robots = RobotsChosen;
            }
            
            this.summaryListView.Items.Clear();
            sortByTotal();
            for (int i = 0; i < Robots.Count; i++)
            {
                this.summaryListView.Items.Add(Robots[i].RobotNumber, i);
                this.summaryListView.Items[i].SubItems.Add(Robots[i].RoundAndVersionNumber());
                this.summaryListView.Items[i].SubItems.Add(Robots[i].RatingScoring.ToString());
                this.summaryListView.Items[i].SubItems.Add(Robots[i].RatingCrossing.ToString());
                this.summaryListView.Items[i].SubItems.Add(Robots[i].RatingAutonomous.ToString());
                this.summaryListView.Items[i].SubItems.Add(Robots[i].RatingAssistance.ToString());
                this.summaryListView.Items[i].SubItems.Add(Robots[i].getTotalRating().ToString());
            }
        }

        private void refreshScoringListView()
        {
            // Determine whether excluded robots are shown or not.
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
            }
            else
            {
                Robots = RobotsChosen;
            }

            this.scoringListView.Items.Clear();
            sortByScoring();

            for (int i = 0; i < Robots.Count; i++)
            {
                this.scoringListView.Items.Add(Robots[i].RobotNumber, i);
                this.scoringListView.Items[i].SubItems.Add(Robots[i].RoundNumber);
                this.scoringListView.Items[i].SubItems.Add(Robots[i].TeleopHighGoalsScored.ToString() + 
                    "/" + (Robots[i].TeleopHighGoalsScored + Robots[i].TeleopHighGoalsMissed).ToString());
                this.scoringListView.Items[i].SubItems.Add(Robots[i].TeleopLowGoalsScored.ToString() +
                    "/" + (Robots[i].TeleopLowGoalsScored + Robots[i].TeleopLowGoalsMissed).ToString());
                if (Robots[i].TeleopChallenge)
                {
                    this.scoringListView.Items[i].SubItems.Add("Yes");
                }
                else
                {
                    this.scoringListView.Items[i].SubItems.Add("No");
                }
                
                if (Robots[i].TeleopClimb)
                {
                    this.scoringListView.Items[i].SubItems.Add("Yes");
                }
                else
                {
                    this.scoringListView.Items[i].SubItems.Add("No");
                }

                this.scoringListView.Items[i].SubItems.Add(Robots[i].RatingScoring.ToString());
            }
        }

        private void refreshCrossingListView()
        {
            // Determine whether excluded robots are shown or not.
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
            }
            else
            {
                Robots = RobotsChosen;
            }

            this.crossingListView.Items.Clear();
            sortByCrossing();

            for (int i = 0; i < Robots.Count; i++)
            {
                RobotRound currentRobotRound = Robots[i];

                this.crossingListView.Items.Add(currentRobotRound.RobotNumber, i);
                this.crossingListView.Items[i].SubItems.Add(currentRobotRound.RoundNumber);

                // Defence 1
                string defence1 = currentRobotRound.getDefence(0).Summary();
                this.crossingListView.Items[i].SubItems.Add(defence1);

                // Defence 2
                string defence2 = currentRobotRound.getDefence(1).Summary();
                this.crossingListView.Items[i].SubItems.Add(defence2);

                // Defence 3

                string defence3 = currentRobotRound.getDefence(2).Summary();
                this.crossingListView.Items[i].SubItems.Add(defence3);


                // Defence 4

                string defence4 = currentRobotRound.getDefence(3).Summary();
                this.crossingListView.Items[i].SubItems.Add(defence4);

                // Defence 5
                string defence5 = currentRobotRound.getDefence(4).Summary();
                this.crossingListView.Items[i].SubItems.Add(defence5);

                this.crossingListView.Items[i].SubItems.Add(currentRobotRound.getTotalCrossings().ToString());
                this.crossingListView.Items[i].SubItems.Add(currentRobotRound.RatingCrossing.ToString());
            }
        }

        private void refreshAutonomousListView()
        {
            // Determine whether excluded robots are shown or not.
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
            }
            else
            {
                Robots = RobotsChosen;
            }

            this.autonomousListView.Items.Clear();
            sortByAutonomous();

            for (int i = 0; i < Robots.Count; i++)
            {
                this.autonomousListView.Items.Add(Robots[i].RobotNumber, i);
                this.autonomousListView.Items[i].SubItems.Add(Robots[i].RoundNumber);
                if (Robots[i].AutoDefenceReached)
                {
                    this.autonomousListView.Items[i].SubItems.Add("Yes");
                }
                else
                {
                    this.autonomousListView.Items[i].SubItems.Add("No");
                }

                if (Robots[i].AutoDefenceCrossed())
                {
                    String defenceList = "";
                    foreach (Defence defenceChoice in Robots[i].DefencesChosen){
                        if (defenceChoice.AutoCrossed)
                        {
                            defenceList += defenceChoice.Name + ", ";
                        }
                    }
                    if (defenceList.Equals(""))
                    {
                        defenceList = "Yes";
                    }
                    else
                    {
                        defenceList = defenceList.Substring(0, defenceList.Length - 2);
                    }
                    this.autonomousListView.Items[i].SubItems.Add(defenceList);
                }
                else
                {
                    this.autonomousListView.Items[i].SubItems.Add("No");
                }

                if (Robots[i].AutoLowGoal)
                {
                    this.autonomousListView.Items[i].SubItems.Add("Yes");
                }
                else
                {
                    this.autonomousListView.Items[i].SubItems.Add("No");
                }

                if (Robots[i].AutoHighGoal)
                {
                    this.autonomousListView.Items[i].SubItems.Add("Yes");
                }
                else
                {
                    this.autonomousListView.Items[i].SubItems.Add("No");
                }
                this.autonomousListView.Items[i].SubItems.Add(Robots[i].RatingAutonomous.ToString());
            }
        }

        private void refreshAssistanceListView()
        {
            // Determine whether excluded robots are shown or not.
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
            }
            else
            {
                Robots = RobotsChosen;
            }

            this.assistanceListView.Items.Clear();
            sortByAssistance();


            for (int i = 0; i < Robots.Count; i++)
            {
                RobotRound currentRobotRound = Robots[i];
                this.assistanceListView.Items.Add(currentRobotRound.RobotNumber, i);
                this.assistanceListView.Items[i].SubItems.Add(currentRobotRound.RoundNumber);

                // Assisted with Sally Port
                this.assistanceListView.Items[i].SubItems.Add(SallyPortAssistanceText(currentRobotRound));

                // Assisted with Drawbridge
                this.assistanceListView.Items[i].SubItems.Add(DrawbridgeAssistanceText(currentRobotRound));

                this.assistanceListView.Items[i].SubItems.Add(currentRobotRound.RatingAssistance.ToString());
            }
        }


        // Sorts the robots list - necessary because the listviews display robots in the order of the list.
        private void sortByTotal()
        {
            RobotRound temp = null;
            for (int i = 0; i < Robots.Count - 1; i++)
            {
                for (int j = 0; j < Robots.Count - 1; j++)
                {
                    if (Robots[j].getTotalRating() < Robots[j + 1].getTotalRating() ||
                        Robots[j].getTotalRating() == Robots[j + 1].getTotalRating() &&
                        Robots[j].RobotID < Robots[j + 1].RobotID)
                    {
                        temp = Robots[j];
                        Robots[j] = Robots[j + 1];
                        Robots[j + 1] = temp;
                    }
                }
            }
            String debugString = "";
            foreach(RobotRound robo in Robots)
            {
                debugString += robo.RobotID + " ";
            }

            Debug.Print(debugString);

        }

        // Sorts the robots list - necessary because the listviews display robots in the order of the list.
        private void sortByScoring()
        {
            RobotRound temp = null;
            for (int i = 0; i < Robots.Count - 1; i++)
            {
                for (int j = 0; j < Robots.Count - 1; j++)
                {
                    if (Robots[j].RatingScoring < Robots[j + 1].RatingScoring ||
                    Robots[j].RatingScoring == Robots[j + 1].RatingScoring &&
                    Robots[j].RobotID < Robots[j + 1].RobotID)
                    {
                        temp = Robots[j];
                        Robots[j] = Robots[j + 1];
                        Robots[j + 1] = temp;
                    }
                }
            }
        }

        // Sorts the robots list - necessary because the listviews display robots in the order of the list.
        private void sortByCrossing()
        {
            RobotRound temp = null;
            for (int i = 0; i < Robots.Count - 1; i++)
            {
                for (int j = 0; j < Robots.Count - 1; j++)
                {
                    if (Robots[j].RatingCrossing < Robots[j + 1].RatingCrossing)
                    {
                        temp = Robots[j];
                        Robots[j] = Robots[j + 1];
                        Robots[j + 1] = temp;
                    }
                    else if (Robots[j].RatingCrossing == Robots[j + 1].RatingCrossing)
                    {
                        if (Robots[j].getTotalCrossings() < Robots[j + 1].getTotalCrossings() ||
                            Robots[j].getTotalCrossings() == Robots[j + 1].getTotalCrossings() &&
                            Robots[j].RobotID < Robots[j + 1].RobotID)
                        {
                            temp = Robots[j];
                            Robots[j] = Robots[j + 1];
                            Robots[j + 1] = temp;
                        }
                    }
                }
            }
        }

        // Sorts the robots list - necessary because the listviews display robots in the order of the list.
        private void sortByAutonomous()
        {
            RobotRound temp = null;
            for (int i = 0; i < Robots.Count - 1; i++)
            {
                for (int j = 0; j < Robots.Count - 1; j++)
                {
                    if (Robots[j].RatingAutonomous < Robots[j + 1].RatingAutonomous ||
                        Robots[j].RatingAutonomous == Robots[j + 1].RatingAutonomous &&
                        Robots[j].RobotID < Robots[j + 1].RobotID)
                    {
                        temp = Robots[j];
                        Robots[j] = Robots[j + 1];
                        Robots[j + 1] = temp;
                    }
                }
            }
        }

        // Sorts the robots list - necessary because the listviews display robots in the order of the list.
        private void sortByAssistance()
        {
            RobotRound temp = null;
            for (int i = 0; i < GetAllRobots().Count - 1; i++)
            {
                for (int j = 0; j < Robots.Count - 1; j++)
                {
                    if (Robots[j].RatingAssistance < Robots[j + 1].RatingAssistance ||
                        Robots[j].RatingAssistance == Robots[j + 1].RatingAssistance &&
                        Robots[j].RobotID < Robots[j + 1].RobotID)
                    {
                        temp = Robots[j];
                        Robots[j] = Robots[j + 1];
                        Robots[j + 1] = temp;
                    }
                }
            }
        }
        
        // If a robot is unchecked, it is no longer excluded.
        private void scoringListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int itemIndex = e.Index;
            if (!scoringListView.Items[itemIndex].Checked)
            {
                sortByScoring();
                Robots[itemIndex].Excluded = false;

                // Uncheck the robot in the all the other tabs
                int roboID = Robots[itemIndex].RobotID;

                // Uncheck in scoring tab
                sortByTotal();
                int indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                summaryListView.Items[indexNum].Checked = false;

                // Uncheck in crossing tab
                sortByCrossing();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                crossingListView.Items[indexNum].Checked = false;

                // Uncheck in autonomous tab
                sortByAutonomous();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                autonomousListView.Items[indexNum].Checked = false;

                // Uncheck in assistance tab
                sortByAssistance();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                assistanceListView.Items[indexNum].Checked = false;
            }
        }

        private void scoringListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateRobotDetailPanel(scoringListView.FocusedItem.Index);
            robotDetail.Visible = true;
        }

        private void crossingListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int itemIndex = e.Index;
            if (!crossingListView.Items[itemIndex].Checked)
            {
                sortByCrossing();
                Robots[itemIndex].Excluded = false;

                // Uncheck the robot in the all the other tabs
                int roboID = Robots[itemIndex].RobotID;

                // Uncheck in scoring tab
                sortByScoring();
                int indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                scoringListView.Items[indexNum].Checked = false;

                // Uncheck in summary tab
                sortByTotal();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                summaryListView.Items[indexNum].Checked = false;

                // Uncheck in autonomous tab
                sortByAutonomous();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                autonomousListView.Items[indexNum].Checked = false;

                // Uncheck in assistance tab
                sortByAssistance();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                assistanceListView.Items[indexNum].Checked = false;
            }
        }

        private void crossingListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateRobotDetailPanel(crossingListView.FocusedItem.Index);
            robotDetail.Visible = true;
        }

        private void autonomousListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int itemIndex = e.Index;
            if (!autonomousListView.Items[itemIndex].Checked)
            {
                sortByAutonomous();
                Robots[itemIndex].Excluded = false;

                // Uncheck the robot in the all the other tabs
                int roboID = Robots[itemIndex].RobotID;

                // Uncheck in scoring tab
                sortByScoring();
                int indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                scoringListView.Items[indexNum].Checked = false;

                // Uncheck in crossing tab
                sortByCrossing();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                crossingListView.Items[indexNum].Checked = false;

                // Uncheck in summary tab
                sortByTotal();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                summaryListView.Items[indexNum].Checked = false;

                // Uncheck in assistance tab
                sortByAssistance();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                assistanceListView.Items[indexNum].Checked = false;
            }
        }

        private void autonomousListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateRobotDetailPanel(autonomousListView.FocusedItem.Index);
            robotDetail.Visible = true;
        }

        private void assistanceListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int itemIndex = e.Index;
            if (!assistanceListView.Items[itemIndex].Checked)
            {
                sortByAssistance();
                Robots[itemIndex].Excluded = false;

                // Uncheck the robot in the all the other tabs
                int roboID = Robots[itemIndex].RobotID;

                // Uncheck in scoring tab
                sortByScoring();
                int indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                scoringListView.Items[indexNum].Checked = false;

                // Uncheck in crossing tab
                sortByCrossing();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                crossingListView.Items[indexNum].Checked = false;

                // Uncheck in autonomous tab
                sortByAutonomous();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                autonomousListView.Items[indexNum].Checked = false;

                // Uncheck in summary tab
                sortByTotal();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                summaryListView.Items[indexNum].Checked = false;
            }
        }

        private void assistanceListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateRobotDetailPanel(assistanceListView.FocusedItem.Index);
            robotDetail.Visible = true;
        }

        // If a robot is unchecked, it is no longer excluded.
        private void summaryListView_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int itemIndex = e.Index;
            if (summaryListView.Items[itemIndex].Checked)
            {
                sortByTotal();
                Robots[itemIndex].Excluded = false;

                // Uncheck the robot in the all the other tabs
                int roboID = Robots[itemIndex].RobotID;

                // Uncheck in scoring tab
                sortByScoring();
                int indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                scoringListView.Items[indexNum].Checked = false;

                // Uncheck in crossing tab
                sortByCrossing();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                crossingListView.Items[indexNum].Checked = false;

                // Uncheck in autonomous tab
                sortByAutonomous();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                autonomousListView.Items[indexNum].Checked = false;

                // Uncheck in assistance tab
                sortByAssistance();
                indexNum = 0;
                for (int i = 0; i < Robots.Count; i++)
                {
                    if (Robots[i].RobotID == roboID)
                    {
                        indexNum = i;
                    }
                }
                assistanceListView.Items[indexNum].Checked = false;

            }

        }

        // When a robot is clicked on, it updates round information tab and comparison tab
        // and makes the panel visible.
        private void summaryListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Sorts robots into appropriate order so selected robot's information can be displayed.
            updateRobotDetailPanel(summaryListView.FocusedItem.Index);
            robotDetail.Visible = true;
        }


        
        // Updates information in the robot detail panel.
        private void updateRobotDetailPanel(int selectedRobotIndex)
        {
            int currentTab = tabControl1.SelectedIndex;

            switch (currentTab)
            {
                case 0:
                    sortByTotal();
                    break;
                case 1:
                    sortByScoring();
                    break;
                case 2:
                    sortByCrossing();
                    break;
                case 3:
                    sortByAutonomous();
                    break;
                case 4:
                    sortByAssistance();
                    break;
            }

            // Updates labels to show selected robot round information.
            robotNumberLabel.Text = Robots[selectedRobotIndex].RobotNumber;

            #region RoundInfoTab
            // ROUND INFORMATION TAB
            // ---------------------------------------------------------------

            robotRoundLabel.Text = Robots[selectedRobotIndex].RoundNumber;

            // Sets labels for Autonomous--------------
            if (Robots[selectedRobotIndex].AutoDefenceReached)
            {
                reachedDefenceLabel.Text = "Yes";
            }
            else
            {
                reachedDefenceLabel.Text = "No";
            }

            if (Robots[selectedRobotIndex].AutoDefenceCrossed())
            {
                String defencesAutoCrossed = "";
                foreach(Defence chosenDef in Robots[selectedRobotIndex].DefencesChosen)
                {
                    if (chosenDef.AutoCrossed)
                    {
                        defencesAutoCrossed += chosenDef.Name + ", ";
                    }
                }
                if (defencesAutoCrossed.Equals(""))
                {
                    defencesAutoCrossed = "Yes";
                }
                else
                {
                    defencesAutoCrossed = defencesAutoCrossed.Substring(0, defencesAutoCrossed.Length - 2);
                }
                crossedDefenceLabel.Text = defencesAutoCrossed;
            }
            else
            {
                crossedDefenceLabel.Text = "No";
            }

            if (Robots[selectedRobotIndex].AutoHighGoal)
            {
                scoredHighLabel.Text = "Yes";
            }
            else
            {
                scoredHighLabel.Text = "No";
            }

            if (Robots[selectedRobotIndex].AutoLowGoal)
            {
                scoredLowLabel.Text = "Yes";
            }
            else
            {
                scoredLowLabel.Text = "No";
            }

            autoRatingLabel.Text = Robots[selectedRobotIndex].RatingAutonomous.ToString();


            // Sets labels for Assistance------------

            // Assisted with Sally Port

            assistedSallyPortLabel.Text = SallyPortAssistanceText(Robots[selectedRobotIndex]);

            // Assisted with Drawbridge
            assistedDrawbridgeLabel.Text = DrawbridgeAssistanceText(Robots[selectedRobotIndex]);

            assistanceRatingLabel.Text = Robots[selectedRobotIndex].RatingAssistance.ToString();

            // Sets labels for Scoring---------------

            teleopLowGoalLabel.Text = Robots[selectedRobotIndex].TeleopLowGoalsScored.ToString() +
                "/" + (Robots[selectedRobotIndex].TeleopLowGoalsScored +
                Robots[selectedRobotIndex].TeleopLowGoalsMissed).ToString();

            teleopHighGoalLabel.Text = Robots[selectedRobotIndex].TeleopHighGoalsScored.ToString() +
                "/" + (Robots[selectedRobotIndex].TeleopHighGoalsScored + Robots[selectedRobotIndex].TeleopLowGoalsMissed).ToString();

            if (Robots[selectedRobotIndex].TeleopClimb)
            {
                teleopClimbLabel.Text = "Yes";
            }
            else
            {
                teleopClimbLabel.Text = "No";
            }

            if (Robots[selectedRobotIndex].TeleopPickup)
            {
                teleopPickupLabel.Text = "Yes";
            }
            else
            {
                teleopPickupLabel.Text = "No";
            }

            if (Robots[selectedRobotIndex].TeleopChallenge)
            {
                teleopChallengeLabel.Text = "Yes";
            }
            else
            {
                teleopChallengeLabel.Text = "No";
            }

            scoringRatingLabel.Text = Robots[selectedRobotIndex].RatingScoring.ToString();

            // Sets labels for crossing---------------

            // Putting defence names on the labels
            defence1Label.Text = Robots[selectedRobotIndex].DefencesChosen[0].Name + ":";
            defence2Label.Text = Robots[selectedRobotIndex].DefencesChosen[1].Name + ":";
            defence3Label.Text = Robots[selectedRobotIndex].DefencesChosen[2].Name + ":";
            defence4Label.Text = Robots[selectedRobotIndex].DefencesChosen[3].Name + ":";
            defence5Label.Text = Robots[selectedRobotIndex].DefencesChosen[4].Name + ":";

            // Displaying robots crossing results for various defences
            defenceCrossing1Label.Text = Robots[selectedRobotIndex].DefencesChosen[0].TeleopCrossedCount.ToString();
            defenceCrossing2Label.Text = Robots[selectedRobotIndex].DefencesChosen[1].TeleopCrossedCount.ToString();
            defenceCrossing3Label.Text = Robots[selectedRobotIndex].DefencesChosen[2].TeleopCrossedCount.ToString();
            defenceCrossing4Label.Text = Robots[selectedRobotIndex].DefencesChosen[3].TeleopCrossedCount.ToString();
            defenceCrossing5Label.Text = Robots[selectedRobotIndex].DefencesChosen[4].TeleopCrossedCount.ToString();

            crossingRatingLabel.Text = Robots[selectedRobotIndex].RatingCrossing.ToString();


            #endregion
            #region RoundComparisonScores
            // ROBOT COMPARISON TAB
            // ----------------------------------------------------------

            // Find all rounds the chosen robot has competed in and store them in a list.
            List<RobotRound> AllRounds = new List<RobotRound>();
            String chosenRobotNumber = Robots[selectedRobotIndex].RobotNumber;

            foreach (RobotRound theRound in GetAllRobots())
            {
                if (theRound.RobotNumber.Equals(chosenRobotNumber))
                {
                    AllRounds.Add(theRound);
                }
            }

            comparisonAssistanceRatingLabel.Text = "";
            comparisonAssistDrawbridgeLabel.Text = "";
            comparisonAssistSallyPortLabel.Text = "";
            comparisonAutoHighGoalLabel.Text = "";
            comparisonAutoLowGoalLabel.Text = "";
            comparisonAutoRatingLabel.Text = "";
            comparisonChallengeLabel.Text = "";
            comparisonChevaldeFriseLabel.Text = "";
            comparisonClimbLabel.Text = "";
            comparisonCrossingRatingLabel.Text = "";
            comparisonDefCrossedLabel.Text = "";
            comparisonDefReachedLabel.Text = "";
            comparisonDrawbridgeLabel.Text = "";
            comparisonLowBarLabel.Text = "";
            comparisonMoatLabel.Text = "";
            comparisonPickupLabel.Text = "";
            comparisonPortcullisLabel.Text = "";
            comparisonRampartsLabel.Text = "";
            comparisonRockWallLabel.Text = "";
            comparisonRoughTerrainLabel.Text = "";
            comparisonRoundNumsLabel.Text = "";
            comparisonSallyPortLabel.Text = "";
            comparisonScoringRatingLabel.Text = "";
            comparisonTeleopHighGoalLabel.Text = "";
            comparisonTeleopLowGoalLabel.Text = "";



            foreach (RobotRound thisRound in AllRounds)
            {
                // Round numbers
                comparisonRoundNumsLabel.Text += thisRound.RoundAndVersionNumber() + ", ";

                // Autonomous labels
                if (thisRound.AutoDefenceReached)
                {
                    comparisonDefReachedLabel.Text += "Y ";
                }
                else
                {
                    comparisonDefReachedLabel.Text += "N ";
                }

                if (thisRound.AutoDefenceCrossed())
                {
                    comparisonDefCrossedLabel.Text += "Y ";
                }
                else
                {
                    comparisonDefCrossedLabel.Text += "N ";
                }

                if (thisRound.AutoHighGoal)
                {
                    comparisonAutoHighGoalLabel.Text += "Y ";
                }
                else
                {
                    comparisonAutoHighGoalLabel.Text += "N ";
                }

                if (thisRound.AutoLowGoal)
                {
                    comparisonAutoLowGoalLabel.Text += "Y ";
                }
                else
                {
                    comparisonAutoLowGoalLabel.Text += "N ";
                }

                comparisonAutoRatingLabel.Text += thisRound.RatingAutonomous.ToString() + " ";

                // Teleop Scoring Labels
                comparisonTeleopHighGoalLabel.Text += thisRound.TeleopHighGoalsScored.ToString() + "/" +
                    (thisRound.TeleopHighGoalsScored + thisRound.TeleopHighGoalsMissed).ToString() + " ";

                comparisonTeleopLowGoalLabel.Text += thisRound.TeleopLowGoalsScored.ToString() + "/" +
                    (thisRound.TeleopLowGoalsScored + thisRound.TeleopLowGoalsMissed).ToString() + " ";

                if (thisRound.TeleopClimb)
                {
                    comparisonClimbLabel.Text += "Y ";
                }
                else
                {
                    comparisonClimbLabel.Text += "N ";
                }

                if (thisRound.TeleopChallenge)
                {
                    comparisonChallengeLabel.Text += "Y ";
                }
                else
                {
                    comparisonChallengeLabel.Text += "N ";
                }

                if (thisRound.TeleopPickup)
                {
                    comparisonPickupLabel.Text += "Y ";
                }
                else
                {
                    comparisonPickupLabel.Text += "N ";
                }

                comparisonScoringRatingLabel.Text += thisRound.RatingScoring.ToString() + " ";

                // Defences
                int defenceNum = isDefenceChosen(thisRound, "Low Bar");
                if (defenceNum >= 0)
                {
                    comparisonLowBarLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonLowBarLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Portcullis");
                if (defenceNum >= 0)
                {
                    comparisonPortcullisLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonPortcullisLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Cheval de Frise");
                if (defenceNum >= 0)
                {
                    comparisonChevaldeFriseLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonChevaldeFriseLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Moat");
                if (defenceNum >= 0)
                {
                    comparisonMoatLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonMoatLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Ramparts");
                if (defenceNum >= 0)
                {
                    comparisonRampartsLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonRampartsLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Drawbridge");
                if (defenceNum >= 0)
                {
                    comparisonDrawbridgeLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonDrawbridgeLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Sally Port");
                if (defenceNum >= 0)
                {
                    comparisonSallyPortLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonSallyPortLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Rock Wall");
                if (defenceNum >= 0)
                {
                    comparisonRockWallLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonRockWallLabel.Text += "- ";
                }

                defenceNum = isDefenceChosen(thisRound, "Rough Terrain");
                if (defenceNum >= 0)
                {
                    comparisonRoughTerrainLabel.Text += thisRound.DefencesChosen[defenceNum].TeleopCrossedCount.ToString() + " ";
                }
                else
                {
                    comparisonRoughTerrainLabel.Text += "- ";
                }

                comparisonCrossingRatingLabel.Text += thisRound.RatingCrossing.ToString() + " ";

                // Assistance
                int sallyPortIndex = -1;
                int drawbridgeIndex = -1;

                for (int i = 0; i < 4; i++)
                {
                    if (thisRound.DefencesChosen[i].Name.Equals("Sally Port"))
                    {
                        sallyPortIndex = i;
                    }
                    if (thisRound.DefencesChosen[i].Name.Equals("Drawbridge"))
                    {
                        drawbridgeIndex = i;
                    }
                }

                if (sallyPortIndex >= 0)
                {
                    AssistedDefence sallyPort = (AssistedDefence)thisRound.DefencesChosen[sallyPortIndex];
                    if (sallyPort.ProvidedAssistance)
                    {
                        comparisonAssistSallyPortLabel.Text += "Y ";
                    }
                    else
                    {
                        comparisonAssistSallyPortLabel.Text += "N ";
                    }
                }
                else
                {
                    comparisonAssistSallyPortLabel.Text += "- ";
                }

                if (drawbridgeIndex >= 0)
                {
                    AssistedDefence sallyPort = (AssistedDefence)thisRound.DefencesChosen[drawbridgeIndex];
                    if (sallyPort.ProvidedAssistance)
                    {
                        comparisonAssistDrawbridgeLabel.Text += "Y ";
                    }
                    else
                    {
                        comparisonAssistDrawbridgeLabel.Text += "N ";
                    }
                }
                else
                {
                    comparisonAssistDrawbridgeLabel.Text += "- ";
                }

                comparisonAssistanceRatingLabel.Text += thisRound.RatingAssistance.ToString() + " ";
            }
            #endregion
            #region allDefenceAutoCrossed
            HashSet<String> AllDefencesAutoCrossed = new HashSet<string>();
            foreach (RobotRound theRounds in AllRounds)
            {
                foreach(Defence chosenDef in theRounds.DefencesChosen)
                {
                    if (chosenDef.AutoCrossed)
                    {
                        AllDefencesAutoCrossed.Add(chosenDef.Name);
                    }
                }
            }
            String temp = "";
            foreach (String defAutoCrossed in AllDefencesAutoCrossed)
            {
                temp += defAutoCrossed + ", ";
            }
            if (temp.Equals(""))
            {
                temp = "None";
            }
            else
            {
                temp = temp.Substring(0, temp.Length - 2);
            }

            allDefencesCrossedLabel.Text = temp;
            #endregion

            #region roundComparisonAverage&Percentages
            // --------       Add percentages and labels onto the end of the comparison labels.
            //------------------------------------------------------------------------------
            // DefReachedLabel
            Char[] comparisonChar = comparisonDefReachedLabel.Text.ToCharArray();
            double success = 0;
            double fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            int percent = (int)(100 * (success / (success + fail)));
            comparisonDefReachedLabel.Text += "  " + percent + "%";

            // DefCrossedLabel
            comparisonChar = comparisonDefCrossedLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonDefCrossedLabel.Text += "  " + percent + "%";

            // AutoHighGoalLabel
            comparisonChar = comparisonAutoHighGoalLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonAutoHighGoalLabel.Text += "  " + percent + "%";

            // AutoLowGoalLabel
            comparisonChar = comparisonAutoLowGoalLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonAutoLowGoalLabel.Text += "  " + percent + "%";

            char[] separator = new char[] { ' ' };

            // AutoRatingLabel
            String[] comparisonStr = comparisonAutoRatingLabel.Text.Split(separator);
            decimal rating = 0;
            decimal counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                rating += Decimal.Parse(comparisonStr[i]);
                counter += 1;
            }

            Decimal averageRating = rating / counter;
            comparisonAutoRatingLabel.Text += "  Avg. " + Decimal.Round(averageRating, 2);

            // LowGoalLabel
            comparisonStr = comparisonTeleopLowGoalLabel.Text.Split(separator);
            decimal goals = 0;
            counter = 0;

            // Goals are written as x/y, therefore, goals needs to be separated into
            // two parts with split.
            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                char[] anotherSeparator = new char[] { '/' };
                String[] goalAndAttempt = comparisonStr[i].Split(anotherSeparator);
                goals += Decimal.Parse(goalAndAttempt[0]);
                counter += 1;
            }
            Decimal averageGoals = goals / counter;
            comparisonTeleopLowGoalLabel.Text += "  Avg. " + Decimal.Round(averageGoals, 2);


            // HighGoalLabel
            comparisonStr = comparisonTeleopHighGoalLabel.Text.Split(separator);
            goals = 0;
            counter = 0;

            // Goals are written as x/y, therefore, goals needs to be separated into
            // two parts with split.
            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                char[] anotherSeparator = new char[] { '/' };
                String[] goalAndAttempt = comparisonStr[i].Split(anotherSeparator);
                goals += Decimal.Parse(goalAndAttempt[0]);
                counter += 1;
            }
            averageGoals = goals / counter;
            comparisonTeleopHighGoalLabel.Text += "  Avg. " + Decimal.Round(averageGoals, 2);

            // ClimbLabel
            comparisonChar = comparisonClimbLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonClimbLabel.Text += "  " + percent + "%";

            // PickupLabel
            comparisonChar = comparisonPickupLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonPickupLabel.Text += "  " + percent + "%";

            // ChallengeLabel
            comparisonChar = comparisonChallengeLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            percent = (int)(100 * (success / (success + fail)));
            comparisonChallengeLabel.Text += "  " + percent + "%";

            // ScoringRatingLabel
            comparisonStr = comparisonScoringRatingLabel.Text.Split(separator);
            rating = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                rating += Decimal.Parse(comparisonStr[i]);
                counter += 1;
            }

            averageRating = rating / counter;
            comparisonScoringRatingLabel.Text += "  Avg. " + Decimal.Round(averageRating, 2);

            // LowBarLabel
            comparisonStr = comparisonLowBarLabel.Text.Split(separator);
            decimal crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonLowBarLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // PortcullisLabel
            comparisonStr = comparisonPortcullisLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonPortcullisLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // ChevalDeFriseLabel
            comparisonStr = comparisonChevaldeFriseLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonChevaldeFriseLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // MoatLabel
            comparisonStr = comparisonMoatLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonMoatLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // RampartsLabel
            comparisonStr = comparisonRampartsLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonRampartsLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // DrawbridgeLabel
            comparisonStr = comparisonDrawbridgeLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonDrawbridgeLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // SallyPortLabel
            comparisonStr = comparisonSallyPortLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonSallyPortLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // RockWallLabel
            comparisonStr = comparisonRockWallLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonRockWallLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // RoughTerrainLabel
            comparisonStr = comparisonRoughTerrainLabel.Text.Split(separator);
            crossings = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                if (!comparisonStr[i].Equals("-"))
                {
                    crossings += Decimal.Parse(comparisonStr[i]);
                    counter += 1;
                }

            }
            if (counter != 0)
            {
                decimal averageCrossing = crossings / counter;
                comparisonRoughTerrainLabel.Text += "  Avg. " + Decimal.Round(averageCrossing, 2);
            }

            // CrossingRatingLabel
            comparisonStr = comparisonCrossingRatingLabel.Text.Split(separator);
            rating = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                rating += Decimal.Parse(comparisonStr[i]);
                counter += 1;
            }

            averageRating = rating / counter;
            comparisonCrossingRatingLabel.Text += "  Avg. " + Decimal.Round(averageRating, 2);

            // AssistanceSallyPortLabel
            comparisonChar = comparisonAssistSallyPortLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            if ((success + fail) > 0)
            {
                percent = (int)(100 * (success / (success + fail)));
                comparisonAssistSallyPortLabel.Text += "  " + percent + "%";
            }

            // AssistanceDrawbridgeLabel
            comparisonChar = comparisonAssistDrawbridgeLabel.Text.ToCharArray();
            success = 0;
            fail = 0;
            foreach (Char theChar in comparisonChar)
            {
                if (theChar == 'Y')
                {
                    success += 1;
                }
                else if (theChar == 'N')
                {
                    fail += 1;
                }
            }
            if ((success + fail) > 0)
            {
                percent = (int)(100 * (success / (success + fail)));
                comparisonAssistDrawbridgeLabel.Text += "  " + percent + "%";
            }

            // AssistanceRatingLabel
            comparisonStr = comparisonAssistanceRatingLabel.Text.Split(separator);
            rating = 0;
            counter = 0;

            for (int i = 0; i < comparisonStr.Length - 1; i++)
            {
                rating += Decimal.Parse(comparisonStr[i]);
                counter += 1;
            }

            averageRating = rating / counter;
            comparisonAssistanceRatingLabel.Text += "  Avg. " + Decimal.Round(averageRating, 2);
            #endregion

            // Remove trailing comma from robot round numbers text
            comparisonRoundNumsLabel.Text = comparisonRoundNumsLabel.Text.Substring(0, comparisonRoundNumsLabel.Text.Length - 2);
        }

        private int isDefenceChosen(RobotRound theRound, String defenceName)
        {


            for (int i = 0; i < 4; i++) {
                if (theRound.DefencesChosen[i].Name.Equals(defenceName))
                {
                    return i;
                }
            }
            return -1;
        }

        
        // Makes the robot detail panel invisible. (To user it appears like it has been closed)
        private void ExitPanelButton_Click(object sender, EventArgs e)
        {
            robotDetail.Visible = false;
        }

        // Refreshes the listview data. It is used to update list so exclude-checked robots are hidden.
        // Also used to unhide excluded robots if user has "View All" checkbox checked.
        private void RefreshButton_Click(object sender, EventArgs e)
        {
            updateExcludedList();
            refreshListViews();
            if (viewAllCheckBox.Checked)
            {
                Robots = GetAllRobots();
                checkExcludedItems();
            }
        }

        // Upladed the robotsChosen list to exclude all robots that have been checked.
        private void updateExcludedList()
        {   
            // Set the excluded value for all robots.
            sortByTotal();
            ListView.CheckedIndexCollection myExcludedItems = summaryListView.CheckedIndices;

            for (int i = 0; i < myExcludedItems.Count; i++)
            {
                Robots[myExcludedItems[i]].Excluded = true;
            }

            sortByScoring();
            myExcludedItems = scoringListView.CheckedIndices;

            for (int i = 0; i < myExcludedItems.Count; i++)
            {
                Robots[myExcludedItems[i]].Excluded = true;
            }

            sortByCrossing();
            myExcludedItems = crossingListView.CheckedIndices;

            for (int i = 0; i < myExcludedItems.Count; i++)
            {
                Robots[myExcludedItems[i]].Excluded = true;
            }

            sortByAutonomous();
            myExcludedItems = autonomousListView.CheckedIndices;

            for (int i = 0; i < myExcludedItems.Count; i++)
            {
                Robots[myExcludedItems[i]].Excluded = true;
            }

            sortByAssistance();
            myExcludedItems = assistanceListView.CheckedIndices;

            for (int i = 0; i < myExcludedItems.Count; i++)
            {
                Robots[myExcludedItems[i]].Excluded = true;
            }

            // Change RobotsChosen so it does not include excluded robots.
            RobotsChosen.Clear();
            List<RobotRound> excludedRobots = new List<RobotRound>();
            for (int i = 0; i < GetAllRobots().Count; i++)
            {
                if (GetAllRobots()[i].Excluded == false)
                {
                    RobotsChosen.Add(GetAllRobots()[i]);
                }
                else
                {
                    // Adds all robots who have excluded set to true.
                    excludedRobots.Add(GetAllRobots()[i]);
                }
            }

            // Now check whether all those "excluded" robots are checked the current tab. If not, they should be unexcluded.
            foreach (RobotRound excludedRounds in excludedRobots)
            {
                int currentTab = tabControl1.SelectedIndex;

                switch (currentTab)
                {
                    case 0:
                        sortByTotal();
                        for (int i = 0; i < Robots.Count; i++)
                        {
                            if (excludedRounds.RobotID == Robots[i].RobotID)
                            {
                                if (!summaryListView.Items[i].Checked)
                                {
                                    Robots[i].Excluded = false;
                                }
                            }
                        }
                        break;
                    case 1:
                        sortByScoring();
                        for (int i = 0; i < Robots.Count; i++)
                        {
                            if (excludedRounds.RobotID == Robots[i].RobotID)
                            {
                                if (!scoringListView.Items[i].Checked)
                                {
                                    Robots[i].Excluded = false;
                                }
                            }
                        }
                        break;
                    case 2:
                        sortByCrossing();
                        for (int i = 0; i < Robots.Count; i++)
                        {
                            if (excludedRounds.RobotID == Robots[i].RobotID)
                            {
                                if (!crossingListView.Items[i].Checked)
                                {
                                    Robots[i].Excluded = false;
                                }
                            }
                        }
                        break;
                    case 3:
                        sortByAutonomous();
                        for (int i = 0; i < Robots.Count; i++)
                        {
                            if (excludedRounds.RobotID == Robots[i].RobotID)
                            {
                                if (!autonomousListView.Items[i].Checked)
                                {
                                    Robots[i].Excluded = false;
                                }
                            }
                        }
                        break;
                    case 4:
                        sortByAssistance();
                        for (int i = 0; i < Robots.Count; i++)
                        {
                            if (excludedRounds.RobotID == Robots[i].RobotID)
                            {
                                if (!assistanceListView.Items[i].Checked)
                                {
                                    Robots[i].Excluded = false;
                                }
                            }
                        }
                        break;
                }
            }

            
        }



        // Method to check the checkboxes for all excluded robots. It is necessary because when view all is selected,
        // the ticks go away on excluded robots.
        private void checkExcludedItems()
        {
            sortByTotal();
            ListView.ListViewItemCollection allItems = summaryListView.Items;
            for (int i = 0; i < allItems.Count; i++)
            {
                if (Robots[i].Excluded)
                {
                    allItems[i].Checked = true;
                    Robots[i].Excluded = true;
                }
            }

            sortByScoring();
            allItems = scoringListView.Items;
            for (int i = 0; i < allItems.Count; i++)
            {
                if (Robots[i].Excluded)
                {
                    allItems[i].Checked = true;
                    Robots[i].Excluded = true;
                }
            }

            sortByCrossing();
            allItems = crossingListView.Items;
            for (int i = 0; i < allItems.Count; i++)
            {
                if (Robots[i].Excluded)
                {
                    allItems[i].Checked = true;
                    Robots[i].Excluded = true;
                }
            }

            sortByAutonomous();
            allItems = autonomousListView.Items;
            for (int i = 0; i < allItems.Count; i++)
            {
                if (Robots[i].Excluded)
                {
                    allItems[i].Checked = true;
                    Robots[i].Excluded = true;
                }
            }

            sortByAssistance();
            allItems = assistanceListView.Items;
            for (int i = 0; i < allItems.Count; i++)
            {
                if (Robots[i].Excluded)
                {
                    allItems[i].Checked = true;
                    Robots[i].Excluded = true;
                }
            }


        }

        private void createRobotRounds()
        {
            List<RobotRound> Robots = RobotRoundFactory.CreateTempRobotRounds();
            
            ReportingSession.Add(Robots);

        }

        private void ImportButton_Click(object sender, EventArgs e)
        {
            ImportRoundData();
        }

        private void ImportRoundData()
        {
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog = new OpenFileDialog();

            // Set filter options and filter index.
            openFileDialog.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;

            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String path = Path.GetFullPath(openFileDialog.FileName);
                CsvReader csvReader = new CsvReader(path);

                List<string> columns = new List<string>();
                RobotRoundCsvConverter converter = new RobotRoundCsvConverter();
                // Keep track of the number of RobotRound's added
                int addedCount = 0;
                // Keep track of the number of rows read from file
                int readCount = 0;
                while (csvReader.ReadRow(columns))
                {
                    RobotRound robotRound = converter.FromCsv(columns);

                    if (ReportingSession.Add(robotRound))
                    {
                        addedCount++;
                    }
                    readCount++;
                }

                // Display message to user summarising result of import
                String message = "";
                if (readCount <= 0)
                {
                    message = "No Robot Rounds were added. File was empty.";
                }
                if (addedCount == readCount)
                {
                    message = "Imported " + addedCount + " Robot Rounds.";
                } else
                {
                    message = "Imported " + addedCount + " of " + readCount + " possible Robot Rounds. "
                        + (readCount - addedCount) + " not added as they were duplicates.";
                }
                String caption = "Import Result";
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            updateExcludedList();
            refreshListViews();

        }

        public string SallyPortAssistanceText(RobotRound robotRound)
        {
            return AssistanceText(robotRound.hasSallyPort(), robotRound.SallyportProvidedAssistance());
        }

        public string DrawbridgeAssistanceText(RobotRound robotRound)
        {
            return AssistanceText(robotRound.hasDrawbridge(), robotRound.DrawbridgeProvidedAssistance());
        }

        private string AssistanceText(bool hasDefence, bool providedAssistance)
        {
            if (hasDefence)
            {
                return PrintedText(providedAssistance);
            } else
            {
                return "n/a";
            }
        }
        // Print the boolean using standard Text
        // Return "Yes" for true, "No" for false.
        private string PrintedText(bool value)
        {
            return value ? "Yes" : "No";
        }

        private List<RobotRound> GetAllRobots()
        {
            return ReportingSession.AllRobots;
        }

        private void InstructionsButton_Click(object sender, EventArgs e)
        {
            if (instructionLabel1.Visible == true)
            {
                instructionLabel1.Visible = false;
                instructionLabel2.Visible = false;
                instructionLabel3.Visible = false;
            }
            else
            {
                instructionLabel1.Visible = true;
                instructionLabel2.Visible = true;
                instructionLabel3.Visible = true;
            }
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            summaryListView.Items.Clear();
            scoringListView.Items.Clear();
            crossingListView.Items.Clear();
            autonomousListView.Items.Clear();
            assistanceListView.Items.Clear();

            Robots = new List<RobotRound>();
            ReportingSession.AllRobots = new List<RobotRound>();
        }

        // The tabs started flickering on hover when I added a background image. I found
        // the code in the below CreateParams method on StackOverflow and it has fixed the problem.
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;
                return cp;
            }
        }
    }
}
