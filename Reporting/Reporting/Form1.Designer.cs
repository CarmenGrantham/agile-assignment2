﻿using StrongholdLibrary;

namespace Reporting
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.Summary = new System.Windows.Forms.TabPage();
            this.summaryListView = new System.Windows.Forms.ListView();
            this.robotDetail = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.robotNumberLabel = new System.Windows.Forms.Label();
            this.ExitPanelButton = new System.Windows.Forms.Button();
            this.robotDetailTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.defenceCrossing5Label = new System.Windows.Forms.Label();
            this.defence5Label = new System.Windows.Forms.Label();
            this.crossingRatingLabel = new System.Windows.Forms.Label();
            this.defenceCrossing1Label = new System.Windows.Forms.Label();
            this.defenceCrossing2Label = new System.Windows.Forms.Label();
            this.defenceCrossing3Label = new System.Windows.Forms.Label();
            this.defenceCrossing4Label = new System.Windows.Forms.Label();
            this.scoringRatingLabel = new System.Windows.Forms.Label();
            this.teleopChallengeLabel = new System.Windows.Forms.Label();
            this.teleopPickupLabel = new System.Windows.Forms.Label();
            this.teleopClimbLabel = new System.Windows.Forms.Label();
            this.teleopHighGoalLabel = new System.Windows.Forms.Label();
            this.teleopLowGoalLabel = new System.Windows.Forms.Label();
            this.assistanceRatingLabel = new System.Windows.Forms.Label();
            this.assistedDrawbridgeLabel = new System.Windows.Forms.Label();
            this.assistedSallyPortLabel = new System.Windows.Forms.Label();
            this.robotRoundLabel = new System.Windows.Forms.Label();
            this.autoRatingLabel = new System.Windows.Forms.Label();
            this.scoredLowLabel = new System.Windows.Forms.Label();
            this.scoredHighLabel = new System.Windows.Forms.Label();
            this.crossedDefenceLabel = new System.Windows.Forms.Label();
            this.reachedDefenceLabel = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.defence1Label = new System.Windows.Forms.Label();
            this.defence2Label = new System.Windows.Forms.Label();
            this.defence3Label = new System.Windows.Forms.Label();
            this.defence4Label = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.allDefencesCrossedLabel = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comparisonRoughTerrainLabel = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.comparisonRampartsLabel = new System.Windows.Forms.Label();
            this.comparisonDrawbridgeLabel = new System.Windows.Forms.Label();
            this.comparisonSallyPortLabel = new System.Windows.Forms.Label();
            this.comparisonRockWallLabel = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.comparisonCrossingRatingLabel = new System.Windows.Forms.Label();
            this.comparisonLowBarLabel = new System.Windows.Forms.Label();
            this.comparisonPortcullisLabel = new System.Windows.Forms.Label();
            this.comparisonChevaldeFriseLabel = new System.Windows.Forms.Label();
            this.comparisonMoatLabel = new System.Windows.Forms.Label();
            this.comparisonScoringRatingLabel = new System.Windows.Forms.Label();
            this.comparisonChallengeLabel = new System.Windows.Forms.Label();
            this.comparisonPickupLabel = new System.Windows.Forms.Label();
            this.comparisonClimbLabel = new System.Windows.Forms.Label();
            this.comparisonTeleopHighGoalLabel = new System.Windows.Forms.Label();
            this.comparisonTeleopLowGoalLabel = new System.Windows.Forms.Label();
            this.comparisonAssistanceRatingLabel = new System.Windows.Forms.Label();
            this.comparisonAssistDrawbridgeLabel = new System.Windows.Forms.Label();
            this.comparisonAssistSallyPortLabel = new System.Windows.Forms.Label();
            this.comparisonRoundNumsLabel = new System.Windows.Forms.Label();
            this.comparisonAutoRatingLabel = new System.Windows.Forms.Label();
            this.comparisonAutoLowGoalLabel = new System.Windows.Forms.Label();
            this.comparisonAutoHighGoalLabel = new System.Windows.Forms.Label();
            this.comparisonDefCrossedLabel = new System.Windows.Forms.Label();
            this.comparisonDefReachedLabel = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Scoring = new System.Windows.Forms.TabPage();
            this.scoringListView = new System.Windows.Forms.ListView();
            this.Crossing = new System.Windows.Forms.TabPage();
            this.crossingListView = new System.Windows.Forms.ListView();
            this.Autonomous = new System.Windows.Forms.TabPage();
            this.autonomousListView = new System.Windows.Forms.ListView();
            this.Assistance = new System.Windows.Forms.TabPage();
            this.assistanceListView = new System.Windows.Forms.ListView();
            this.viewAllCheckBox = new System.Windows.Forms.CheckBox();
            this.RefreshButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.InstructionsButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.ImportButton = new System.Windows.Forms.Button();
            this.instructionLabel3 = new System.Windows.Forms.Label();
            this.instructionLabel2 = new System.Windows.Forms.Label();
            this.instructionLabel1 = new System.Windows.Forms.Label();
            this.Summary.SuspendLayout();
            this.robotDetail.SuspendLayout();
            this.robotDetailTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Scoring.SuspendLayout();
            this.Crossing.SuspendLayout();
            this.Autonomous.SuspendLayout();
            this.Assistance.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(18, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 37);
            this.label1.TabIndex = 4;
            this.label1.Text = "Reporting";
            // 
            // Summary
            // 
            this.Summary.Controls.Add(this.summaryListView);
            this.Summary.Location = new System.Drawing.Point(4, 25);
            this.Summary.Name = "Summary";
            this.Summary.Padding = new System.Windows.Forms.Padding(3);
            this.Summary.Size = new System.Drawing.Size(1252, 578);
            this.Summary.TabIndex = 0;
            this.Summary.Text = "Summary";
            this.Summary.ToolTipText = "Sorts by Overall Performance";
            this.Summary.UseVisualStyleBackColor = true;

            this.summaryListView.Columns.Add("Robot Number", 110);
            this.summaryListView.Columns.Add("Round", 110);
            this.summaryListView.Columns.Add("Scoring Rating", 110);
            this.summaryListView.Columns.Add("Crossing Rating", 110);
            this.summaryListView.Columns.Add("Autonomous Rating", 135);
            this.summaryListView.Columns.Add("Assistance Rating", 135);
            this.summaryListView.Columns.Add("Total Rating", 110);


            this.scoringListView.Columns.Add("Robot Number", 110);
            this.scoringListView.Columns.Add("Round", 110);
            this.scoringListView.Columns.Add("High Goals (Goals/Attempts)", 185);
            this.scoringListView.Columns.Add("Low Goals (Goals/Attempts)", 185);
            this.scoringListView.Columns.Add("Successful Challenge", 145);
            this.scoringListView.Columns.Add("Climb", 95);
            this.scoringListView.Columns.Add("Scoring Rating", 110);

            this.crossingListView.Columns.Add("Robot Number", 100);
            this.crossingListView.Columns.Add("Round", 70);
            this.crossingListView.Columns.Add("Defence 1", 165);
            this.crossingListView.Columns.Add("Defence 2", 165);
            this.crossingListView.Columns.Add("Defence 3", 165);
            this.crossingListView.Columns.Add("Defence 4", 165);
            this.crossingListView.Columns.Add("Defence 5", 165);
            this.crossingListView.Columns.Add("Total Crossings", 110);
            this.crossingListView.Columns.Add("Crossing Rating", 110);

            this.autonomousListView.Columns.Add("Robot Number", 110);
            this.autonomousListView.Columns.Add("Round", 110);
            this.autonomousListView.Columns.Add("Defence Reached", 135);
            this.autonomousListView.Columns.Add("Defence Crossed", 285);
            this.autonomousListView.Columns.Add("Low Goal", 110);
            this.autonomousListView.Columns.Add("High Goal", 110);
            this.autonomousListView.Columns.Add("Autonomous Rating", 135);

            this.assistanceListView.Columns.Add("Robot Number", 110);
            this.assistanceListView.Columns.Add("Round", 110);
            this.assistanceListView.Columns.Add("Assisted with Sally Port", 160);
            this.assistanceListView.Columns.Add("Assisted with Drawbridge", 170);
            this.assistanceListView.Columns.Add("Assistance Rating", 135);
            // 
            // summaryListView
            // 
            this.summaryListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.summaryListView.CheckBoxes = true;
            this.summaryListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.summaryListView.Location = new System.Drawing.Point(6, 3);
            this.summaryListView.Name = "summaryListView";
            this.summaryListView.Size = new System.Drawing.Size(1240, 572);
            this.summaryListView.TabIndex = 0;
            this.summaryListView.UseCompatibleStateImageBehavior = false;
            this.summaryListView.View = System.Windows.Forms.View.Details;
            this.summaryListView.SelectedIndexChanged += new System.EventHandler(this.summaryListView_SelectedIndexChanged);
            this.summaryListView.TabIndexChanged += new System.EventHandler(this.RefreshButton_Click);
            // 
            // robotDetail
            // 
            this.robotDetail.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.robotDetail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("robotDetail.BackgroundImage")));
            this.robotDetail.Controls.Add(this.label2);
            this.robotDetail.Controls.Add(this.robotNumberLabel);
            this.robotDetail.Controls.Add(this.ExitPanelButton);
            this.robotDetail.Controls.Add(this.robotDetailTabControl);
            this.robotDetail.Location = new System.Drawing.Point(99, 25);
            this.robotDetail.Name = "robotDetail";
            this.robotDetail.Size = new System.Drawing.Size(1092, 525);
            this.robotDetail.TabIndex = 6;
            this.robotDetail.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(343, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 25);
            this.label2.TabIndex = 50;
            this.label2.Text = "Robot Number:";
            // 
            // robotNumberLabel
            // 
            this.robotNumberLabel.AutoSize = true;
            this.robotNumberLabel.BackColor = System.Drawing.Color.Transparent;
            this.robotNumberLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.robotNumberLabel.ForeColor = System.Drawing.Color.White;
            this.robotNumberLabel.Location = new System.Drawing.Point(505, 13);
            this.robotNumberLabel.Name = "robotNumberLabel";
            this.robotNumberLabel.Size = new System.Drawing.Size(89, 25);
            this.robotNumberLabel.TabIndex = 80;
            this.robotNumberLabel.Text = "label27";
            // 
            // ExitPanelButton
            // 
            this.ExitPanelButton.BackColor = System.Drawing.Color.Transparent;
            this.ExitPanelButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ExitPanelButton.FlatAppearance.BorderSize = 0;
            this.ExitPanelButton.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ExitPanelButton.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ButtonFace;
            this.ExitPanelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitPanelButton.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitPanelButton.ForeColor = System.Drawing.Color.Red;
            this.ExitPanelButton.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ExitPanelButton.Location = new System.Drawing.Point(1044, 3);
            this.ExitPanelButton.Name = "ExitPanelButton";
            this.ExitPanelButton.Size = new System.Drawing.Size(45, 40);
            this.ExitPanelButton.TabIndex = 1;
            this.ExitPanelButton.Text = "X";
            this.ExitPanelButton.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ExitPanelButton.UseVisualStyleBackColor = false;
            this.ExitPanelButton.Click += new System.EventHandler(this.ExitPanelButton_Click);
            // 
            // robotDetailTabControl
            // 
            this.robotDetailTabControl.Controls.Add(this.tabPage1);
            this.robotDetailTabControl.Controls.Add(this.tabPage2);
            this.robotDetailTabControl.Location = new System.Drawing.Point(14, 29);
            this.robotDetailTabControl.Name = "robotDetailTabControl";
            this.robotDetailTabControl.SelectedIndex = 0;
            this.robotDetailTabControl.Size = new System.Drawing.Size(1060, 476);
            this.robotDetailTabControl.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.defenceCrossing5Label);
            this.tabPage1.Controls.Add(this.defence5Label);
            this.tabPage1.Controls.Add(this.crossingRatingLabel);
            this.tabPage1.Controls.Add(this.defenceCrossing1Label);
            this.tabPage1.Controls.Add(this.defenceCrossing2Label);
            this.tabPage1.Controls.Add(this.defenceCrossing3Label);
            this.tabPage1.Controls.Add(this.defenceCrossing4Label);
            this.tabPage1.Controls.Add(this.scoringRatingLabel);
            this.tabPage1.Controls.Add(this.teleopChallengeLabel);
            this.tabPage1.Controls.Add(this.teleopPickupLabel);
            this.tabPage1.Controls.Add(this.teleopClimbLabel);
            this.tabPage1.Controls.Add(this.teleopHighGoalLabel);
            this.tabPage1.Controls.Add(this.teleopLowGoalLabel);
            this.tabPage1.Controls.Add(this.assistanceRatingLabel);
            this.tabPage1.Controls.Add(this.assistedDrawbridgeLabel);
            this.tabPage1.Controls.Add(this.assistedSallyPortLabel);
            this.tabPage1.Controls.Add(this.robotRoundLabel);
            this.tabPage1.Controls.Add(this.autoRatingLabel);
            this.tabPage1.Controls.Add(this.scoredLowLabel);
            this.tabPage1.Controls.Add(this.scoredHighLabel);
            this.tabPage1.Controls.Add(this.crossedDefenceLabel);
            this.tabPage1.Controls.Add(this.reachedDefenceLabel);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.defence1Label);
            this.tabPage1.Controls.Add(this.defence2Label);
            this.tabPage1.Controls.Add(this.defence3Label);
            this.tabPage1.Controls.Add(this.defence4Label);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1052, 450);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Round Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // defenceCrossing5Label
            // 
            this.defenceCrossing5Label.AutoSize = true;
            this.defenceCrossing5Label.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenceCrossing5Label.Location = new System.Drawing.Point(156, 346);
            this.defenceCrossing5Label.Name = "defenceCrossing5Label";
            this.defenceCrossing5Label.Size = new System.Drawing.Size(42, 15);
            this.defenceCrossing5Label.TabIndex = 97;
            this.defenceCrossing5Label.Text = "label";
            // 
            // defence5Label
            // 
            this.defence5Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defence5Label.Location = new System.Drawing.Point(11, 345);
            this.defence5Label.Name = "defence5Label";
            this.defence5Label.Size = new System.Drawing.Size(144, 16);
            this.defence5Label.TabIndex = 96;
            this.defence5Label.Text = "Defence 5:";
            this.defence5Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // crossingRatingLabel
            // 
            this.crossingRatingLabel.AutoSize = true;
            this.crossingRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crossingRatingLabel.Location = new System.Drawing.Point(156, 371);
            this.crossingRatingLabel.Name = "crossingRatingLabel";
            this.crossingRatingLabel.Size = new System.Drawing.Size(56, 15);
            this.crossingRatingLabel.TabIndex = 95;
            this.crossingRatingLabel.Text = "label27";
            // 
            // defenceCrossing1Label
            // 
            this.defenceCrossing1Label.AutoSize = true;
            this.defenceCrossing1Label.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenceCrossing1Label.Location = new System.Drawing.Point(156, 246);
            this.defenceCrossing1Label.Name = "defenceCrossing1Label";
            this.defenceCrossing1Label.Size = new System.Drawing.Size(56, 15);
            this.defenceCrossing1Label.TabIndex = 94;
            this.defenceCrossing1Label.Text = "label22";
            // 
            // defenceCrossing2Label
            // 
            this.defenceCrossing2Label.AutoSize = true;
            this.defenceCrossing2Label.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenceCrossing2Label.Location = new System.Drawing.Point(156, 271);
            this.defenceCrossing2Label.Name = "defenceCrossing2Label";
            this.defenceCrossing2Label.Size = new System.Drawing.Size(56, 15);
            this.defenceCrossing2Label.TabIndex = 93;
            this.defenceCrossing2Label.Text = "label21";
            // 
            // defenceCrossing3Label
            // 
            this.defenceCrossing3Label.AutoSize = true;
            this.defenceCrossing3Label.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenceCrossing3Label.Location = new System.Drawing.Point(156, 296);
            this.defenceCrossing3Label.Name = "defenceCrossing3Label";
            this.defenceCrossing3Label.Size = new System.Drawing.Size(56, 15);
            this.defenceCrossing3Label.TabIndex = 92;
            this.defenceCrossing3Label.Text = "label20";
            // 
            // defenceCrossing4Label
            // 
            this.defenceCrossing4Label.AutoSize = true;
            this.defenceCrossing4Label.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defenceCrossing4Label.Location = new System.Drawing.Point(156, 321);
            this.defenceCrossing4Label.Name = "defenceCrossing4Label";
            this.defenceCrossing4Label.Size = new System.Drawing.Size(56, 15);
            this.defenceCrossing4Label.TabIndex = 91;
            this.defenceCrossing4Label.Text = "label17";
            // 
            // scoringRatingLabel
            // 
            this.scoringRatingLabel.AutoSize = true;
            this.scoringRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoringRatingLabel.Location = new System.Drawing.Point(707, 371);
            this.scoringRatingLabel.Name = "scoringRatingLabel";
            this.scoringRatingLabel.Size = new System.Drawing.Size(56, 15);
            this.scoringRatingLabel.TabIndex = 90;
            this.scoringRatingLabel.Text = "label32";
            // 
            // teleopChallengeLabel
            // 
            this.teleopChallengeLabel.AutoSize = true;
            this.teleopChallengeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleopChallengeLabel.Location = new System.Drawing.Point(707, 346);
            this.teleopChallengeLabel.Name = "teleopChallengeLabel";
            this.teleopChallengeLabel.Size = new System.Drawing.Size(56, 15);
            this.teleopChallengeLabel.TabIndex = 89;
            this.teleopChallengeLabel.Text = "label31";
            // 
            // teleopPickupLabel
            // 
            this.teleopPickupLabel.AutoSize = true;
            this.teleopPickupLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleopPickupLabel.Location = new System.Drawing.Point(707, 321);
            this.teleopPickupLabel.Name = "teleopPickupLabel";
            this.teleopPickupLabel.Size = new System.Drawing.Size(56, 15);
            this.teleopPickupLabel.TabIndex = 88;
            this.teleopPickupLabel.Text = "label30";
            // 
            // teleopClimbLabel
            // 
            this.teleopClimbLabel.AutoSize = true;
            this.teleopClimbLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleopClimbLabel.Location = new System.Drawing.Point(707, 296);
            this.teleopClimbLabel.Name = "teleopClimbLabel";
            this.teleopClimbLabel.Size = new System.Drawing.Size(56, 15);
            this.teleopClimbLabel.TabIndex = 87;
            this.teleopClimbLabel.Text = "label29";
            // 
            // teleopHighGoalLabel
            // 
            this.teleopHighGoalLabel.AutoSize = true;
            this.teleopHighGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleopHighGoalLabel.Location = new System.Drawing.Point(707, 271);
            this.teleopHighGoalLabel.Name = "teleopHighGoalLabel";
            this.teleopHighGoalLabel.Size = new System.Drawing.Size(56, 15);
            this.teleopHighGoalLabel.TabIndex = 86;
            this.teleopHighGoalLabel.Text = "label28";
            // 
            // teleopLowGoalLabel
            // 
            this.teleopLowGoalLabel.AutoSize = true;
            this.teleopLowGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleopLowGoalLabel.Location = new System.Drawing.Point(707, 246);
            this.teleopLowGoalLabel.Name = "teleopLowGoalLabel";
            this.teleopLowGoalLabel.Size = new System.Drawing.Size(56, 15);
            this.teleopLowGoalLabel.TabIndex = 85;
            this.teleopLowGoalLabel.Text = "label27";
            // 
            // assistanceRatingLabel
            // 
            this.assistanceRatingLabel.AutoSize = true;
            this.assistanceRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assistanceRatingLabel.Location = new System.Drawing.Point(707, 121);
            this.assistanceRatingLabel.Name = "assistanceRatingLabel";
            this.assistanceRatingLabel.Size = new System.Drawing.Size(56, 15);
            this.assistanceRatingLabel.TabIndex = 84;
            this.assistanceRatingLabel.Text = "label29";
            // 
            // assistedDrawbridgeLabel
            // 
            this.assistedDrawbridgeLabel.AutoSize = true;
            this.assistedDrawbridgeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assistedDrawbridgeLabel.Location = new System.Drawing.Point(707, 96);
            this.assistedDrawbridgeLabel.Name = "assistedDrawbridgeLabel";
            this.assistedDrawbridgeLabel.Size = new System.Drawing.Size(56, 15);
            this.assistedDrawbridgeLabel.TabIndex = 83;
            this.assistedDrawbridgeLabel.Text = "label28";
            // 
            // assistedSallyPortLabel
            // 
            this.assistedSallyPortLabel.AutoSize = true;
            this.assistedSallyPortLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assistedSallyPortLabel.Location = new System.Drawing.Point(707, 71);
            this.assistedSallyPortLabel.Name = "assistedSallyPortLabel";
            this.assistedSallyPortLabel.Size = new System.Drawing.Size(56, 15);
            this.assistedSallyPortLabel.TabIndex = 82;
            this.assistedSallyPortLabel.Text = "label27";
            // 
            // robotRoundLabel
            // 
            this.robotRoundLabel.AutoSize = true;
            this.robotRoundLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.robotRoundLabel.Location = new System.Drawing.Point(150, 20);
            this.robotRoundLabel.Name = "robotRoundLabel";
            this.robotRoundLabel.Size = new System.Drawing.Size(52, 16);
            this.robotRoundLabel.TabIndex = 81;
            this.robotRoundLabel.Text = "label27";
            // 
            // autoRatingLabel
            // 
            this.autoRatingLabel.AutoSize = true;
            this.autoRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autoRatingLabel.Location = new System.Drawing.Point(156, 171);
            this.autoRatingLabel.Name = "autoRatingLabel";
            this.autoRatingLabel.Size = new System.Drawing.Size(56, 15);
            this.autoRatingLabel.TabIndex = 79;
            this.autoRatingLabel.Text = "label30";
            // 
            // scoredLowLabel
            // 
            this.scoredLowLabel.AutoSize = true;
            this.scoredLowLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoredLowLabel.Location = new System.Drawing.Point(156, 146);
            this.scoredLowLabel.Name = "scoredLowLabel";
            this.scoredLowLabel.Size = new System.Drawing.Size(56, 15);
            this.scoredLowLabel.TabIndex = 78;
            this.scoredLowLabel.Text = "label29";
            // 
            // scoredHighLabel
            // 
            this.scoredHighLabel.AutoSize = true;
            this.scoredHighLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoredHighLabel.Location = new System.Drawing.Point(156, 121);
            this.scoredHighLabel.Name = "scoredHighLabel";
            this.scoredHighLabel.Size = new System.Drawing.Size(56, 15);
            this.scoredHighLabel.TabIndex = 77;
            this.scoredHighLabel.Text = "label28";
            // 
            // crossedDefenceLabel
            // 
            this.crossedDefenceLabel.AutoSize = true;
            this.crossedDefenceLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crossedDefenceLabel.Location = new System.Drawing.Point(156, 96);
            this.crossedDefenceLabel.Name = "crossedDefenceLabel";
            this.crossedDefenceLabel.Size = new System.Drawing.Size(56, 15);
            this.crossedDefenceLabel.TabIndex = 76;
            this.crossedDefenceLabel.Text = "label27";
            // 
            // reachedDefenceLabel
            // 
            this.reachedDefenceLabel.AutoSize = true;
            this.reachedDefenceLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reachedDefenceLabel.Location = new System.Drawing.Point(156, 71);
            this.reachedDefenceLabel.Name = "reachedDefenceLabel";
            this.reachedDefenceLabel.Size = new System.Drawing.Size(140, 15);
            this.reachedDefenceLabel.TabIndex = 75;
            this.reachedDefenceLabel.Text = "reachedDefenceLabel";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(602, 370);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(99, 16);
            this.label26.TabIndex = 74;
            this.label26.Text = "Scoring Rating:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(49, 370);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 16);
            this.label25.TabIndex = 73;
            this.label25.Text = "Crossing Rating:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(25, 170);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(128, 16);
            this.label24.TabIndex = 72;
            this.label24.Text = "Autonomous Rating:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(582, 120);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(119, 16);
            this.label23.TabIndex = 71;
            this.label23.Text = "Assistance Rating:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // defence1Label
            // 
            this.defence1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defence1Label.Location = new System.Drawing.Point(11, 245);
            this.defence1Label.Name = "defence1Label";
            this.defence1Label.Size = new System.Drawing.Size(144, 16);
            this.defence1Label.TabIndex = 70;
            this.defence1Label.Text = "Defence 1:";
            this.defence1Label.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // defence2Label
            // 
            this.defence2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defence2Label.Location = new System.Drawing.Point(11, 270);
            this.defence2Label.Name = "defence2Label";
            this.defence2Label.Size = new System.Drawing.Size(144, 16);
            this.defence2Label.TabIndex = 69;
            this.defence2Label.Text = "Defence 2:";
            this.defence2Label.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // defence3Label
            // 
            this.defence3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defence3Label.Location = new System.Drawing.Point(11, 295);
            this.defence3Label.Name = "defence3Label";
            this.defence3Label.Size = new System.Drawing.Size(144, 16);
            this.defence3Label.TabIndex = 68;
            this.defence3Label.Text = "Defence 3:";
            this.defence3Label.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // defence4Label
            // 
            this.defence4Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defence4Label.Location = new System.Drawing.Point(11, 320);
            this.defence4Label.Name = "defence4Label";
            this.defence4Label.Size = new System.Drawing.Size(144, 16);
            this.defence4Label.TabIndex = 67;
            this.defence4Label.Text = "Defence 4:";
            this.defence4Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(631, 45);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 16);
            this.label19.TabIndex = 66;
            this.label19.Text = "Assistance";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(45, 220);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(185, 16);
            this.label18.TabIndex = 65;
            this.label18.Text = "Teleop Defence Crossing";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(613, 220);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 16);
            this.label16.TabIndex = 64;
            this.label16.Text = "Teleop Scoring";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(92, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 16);
            this.label15.TabIndex = 63;
            this.label15.Text = "Autonomous";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(629, 345);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 16);
            this.label14.TabIndex = 62;
            this.label14.Text = "Challenge:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(656, 295);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 16);
            this.label13.TabIndex = 61;
            this.label13.Text = "Climb:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(649, 320);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 16);
            this.label12.TabIndex = 60;
            this.label12.Text = "Pickup:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(36, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 16);
            this.label11.TabIndex = 59;
            this.label11.Text = "Scored High Goal:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(39, 145);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 16);
            this.label10.TabIndex = 58;
            this.label10.Text = "Scored Low Goal:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(39, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(116, 16);
            this.label9.TabIndex = 57;
            this.label9.Text = "Crossed Defence:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label9, "All defences crossed autonomously.");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(33, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 16);
            this.label8.TabIndex = 56;
            this.label8.Text = "Reached Defence:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(540, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 16);
            this.label7.TabIndex = 55;
            this.label7.Text = "Assisted with Drawbridge:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(553, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(148, 16);
            this.label6.TabIndex = 54;
            this.label6.Text = "Assisted with Sally Port:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(469, 270);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(232, 16);
            this.label5.TabIndex = 53;
            this.label5.Text = "Teleop High Goals [scores/attempts]:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(473, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 16);
            this.label4.TabIndex = 52;
            this.label4.Text = "Teleop Low Goals [scores/attempts]:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(29, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.TabIndex = 51;
            this.label3.Text = "Round Number:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.allDefencesCrossedLabel);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.comparisonRoughTerrainLabel);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.comparisonRampartsLabel);
            this.tabPage2.Controls.Add(this.comparisonDrawbridgeLabel);
            this.tabPage2.Controls.Add(this.comparisonSallyPortLabel);
            this.tabPage2.Controls.Add(this.comparisonRockWallLabel);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.comparisonCrossingRatingLabel);
            this.tabPage2.Controls.Add(this.comparisonLowBarLabel);
            this.tabPage2.Controls.Add(this.comparisonPortcullisLabel);
            this.tabPage2.Controls.Add(this.comparisonChevaldeFriseLabel);
            this.tabPage2.Controls.Add(this.comparisonMoatLabel);
            this.tabPage2.Controls.Add(this.comparisonScoringRatingLabel);
            this.tabPage2.Controls.Add(this.comparisonChallengeLabel);
            this.tabPage2.Controls.Add(this.comparisonPickupLabel);
            this.tabPage2.Controls.Add(this.comparisonClimbLabel);
            this.tabPage2.Controls.Add(this.comparisonTeleopHighGoalLabel);
            this.tabPage2.Controls.Add(this.comparisonTeleopLowGoalLabel);
            this.tabPage2.Controls.Add(this.comparisonAssistanceRatingLabel);
            this.tabPage2.Controls.Add(this.comparisonAssistDrawbridgeLabel);
            this.tabPage2.Controls.Add(this.comparisonAssistSallyPortLabel);
            this.tabPage2.Controls.Add(this.comparisonRoundNumsLabel);
            this.tabPage2.Controls.Add(this.comparisonAutoRatingLabel);
            this.tabPage2.Controls.Add(this.comparisonAutoLowGoalLabel);
            this.tabPage2.Controls.Add(this.comparisonAutoHighGoalLabel);
            this.tabPage2.Controls.Add(this.comparisonDefCrossedLabel);
            this.tabPage2.Controls.Add(this.comparisonDefReachedLabel);
            this.tabPage2.Controls.Add(this.label43);
            this.tabPage2.Controls.Add(this.label44);
            this.tabPage2.Controls.Add(this.label45);
            this.tabPage2.Controls.Add(this.label46);
            this.tabPage2.Controls.Add(this.label47);
            this.tabPage2.Controls.Add(this.label48);
            this.tabPage2.Controls.Add(this.label49);
            this.tabPage2.Controls.Add(this.label50);
            this.tabPage2.Controls.Add(this.label51);
            this.tabPage2.Controls.Add(this.label52);
            this.tabPage2.Controls.Add(this.label53);
            this.tabPage2.Controls.Add(this.label54);
            this.tabPage2.Controls.Add(this.label55);
            this.tabPage2.Controls.Add(this.label56);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.label61);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.label65);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1052, 450);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Round Comparison";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // allDefencesCrossedLabel
            // 
            this.allDefencesCrossedLabel.AutoSize = true;
            this.allDefencesCrossedLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.allDefencesCrossedLabel.Location = new System.Drawing.Point(173, 121);
            this.allDefencesCrossedLabel.Name = "allDefencesCrossedLabel";
            this.allDefencesCrossedLabel.Size = new System.Drawing.Size(35, 15);
            this.allDefencesCrossedLabel.TabIndex = 151;
            this.allDefencesCrossedLabel.Text = "test";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 120);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(141, 16);
            this.label21.TabIndex = 150;
            this.label21.Text = "All Defences Crossed:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label21, "Defences crossed autonomously");
            // 
            // comparisonRoughTerrainLabel
            // 
            this.comparisonRoughTerrainLabel.AutoSize = true;
            this.comparisonRoughTerrainLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonRoughTerrainLabel.Location = new System.Drawing.Point(703, 246);
            this.comparisonRoughTerrainLabel.Name = "comparisonRoughTerrainLabel";
            this.comparisonRoughTerrainLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonRoughTerrainLabel.TabIndex = 149;
            this.comparisonRoughTerrainLabel.Text = "N/A";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(594, 245);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 16);
            this.label20.TabIndex = 148;
            this.label20.Text = "Rough Terrain:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comparisonRampartsLabel
            // 
            this.comparisonRampartsLabel.AutoSize = true;
            this.comparisonRampartsLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonRampartsLabel.Location = new System.Drawing.Point(703, 146);
            this.comparisonRampartsLabel.Name = "comparisonRampartsLabel";
            this.comparisonRampartsLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonRampartsLabel.TabIndex = 147;
            this.comparisonRampartsLabel.Text = "N/A";
            // 
            // comparisonDrawbridgeLabel
            // 
            this.comparisonDrawbridgeLabel.AutoSize = true;
            this.comparisonDrawbridgeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonDrawbridgeLabel.Location = new System.Drawing.Point(703, 171);
            this.comparisonDrawbridgeLabel.Name = "comparisonDrawbridgeLabel";
            this.comparisonDrawbridgeLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonDrawbridgeLabel.TabIndex = 146;
            this.comparisonDrawbridgeLabel.Text = "N/A";
            this.toolTip1.SetToolTip(this.comparisonDrawbridgeLabel, "Assisted [A]");
            // 
            // comparisonSallyPortLabel
            // 
            this.comparisonSallyPortLabel.AutoSize = true;
            this.comparisonSallyPortLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonSallyPortLabel.Location = new System.Drawing.Point(703, 196);
            this.comparisonSallyPortLabel.Name = "comparisonSallyPortLabel";
            this.comparisonSallyPortLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonSallyPortLabel.TabIndex = 145;
            this.comparisonSallyPortLabel.Text = "N/A";
            this.toolTip1.SetToolTip(this.comparisonSallyPortLabel, "Assisted [A]");
            // 
            // comparisonRockWallLabel
            // 
            this.comparisonRockWallLabel.AutoSize = true;
            this.comparisonRockWallLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonRockWallLabel.Location = new System.Drawing.Point(703, 221);
            this.comparisonRockWallLabel.Name = "comparisonRockWallLabel";
            this.comparisonRockWallLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonRockWallLabel.TabIndex = 144;
            this.comparisonRockWallLabel.Text = "N/A";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(621, 170);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(70, 16);
            this.label70.TabIndex = 143;
            this.label70.Text = "Ramparts:";
            this.label70.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(610, 45);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(81, 16);
            this.label71.TabIndex = 142;
            this.label71.Text = "Drawbridge:";
            this.label71.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label71, "Assisted [A]");
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(623, 195);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(68, 16);
            this.label72.TabIndex = 141;
            this.label72.Text = "Sally Port:";
            this.label72.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label72, "Assisted [A]");
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(618, 220);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(73, 16);
            this.label73.TabIndex = 140;
            this.label73.Text = "Rock Wall:";
            this.label73.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // comparisonCrossingRatingLabel
            // 
            this.comparisonCrossingRatingLabel.AutoSize = true;
            this.comparisonCrossingRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonCrossingRatingLabel.Location = new System.Drawing.Point(703, 272);
            this.comparisonCrossingRatingLabel.Name = "comparisonCrossingRatingLabel";
            this.comparisonCrossingRatingLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonCrossingRatingLabel.TabIndex = 139;
            this.comparisonCrossingRatingLabel.Text = "N/A";
            // 
            // comparisonLowBarLabel
            // 
            this.comparisonLowBarLabel.AutoSize = true;
            this.comparisonLowBarLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonLowBarLabel.Location = new System.Drawing.Point(703, 46);
            this.comparisonLowBarLabel.Name = "comparisonLowBarLabel";
            this.comparisonLowBarLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonLowBarLabel.TabIndex = 138;
            this.comparisonLowBarLabel.Text = "N/A";
            // 
            // comparisonPortcullisLabel
            // 
            this.comparisonPortcullisLabel.AutoSize = true;
            this.comparisonPortcullisLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonPortcullisLabel.Location = new System.Drawing.Point(703, 71);
            this.comparisonPortcullisLabel.Name = "comparisonPortcullisLabel";
            this.comparisonPortcullisLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonPortcullisLabel.TabIndex = 137;
            this.comparisonPortcullisLabel.Text = "N/A";
            // 
            // comparisonChevaldeFriseLabel
            // 
            this.comparisonChevaldeFriseLabel.AutoSize = true;
            this.comparisonChevaldeFriseLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonChevaldeFriseLabel.Location = new System.Drawing.Point(703, 96);
            this.comparisonChevaldeFriseLabel.Name = "comparisonChevaldeFriseLabel";
            this.comparisonChevaldeFriseLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonChevaldeFriseLabel.TabIndex = 136;
            this.comparisonChevaldeFriseLabel.Text = "N/A";
            // 
            // comparisonMoatLabel
            // 
            this.comparisonMoatLabel.AutoSize = true;
            this.comparisonMoatLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonMoatLabel.Location = new System.Drawing.Point(703, 121);
            this.comparisonMoatLabel.Name = "comparisonMoatLabel";
            this.comparisonMoatLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonMoatLabel.TabIndex = 135;
            this.comparisonMoatLabel.Text = "N/A";
            // 
            // comparisonScoringRatingLabel
            // 
            this.comparisonScoringRatingLabel.AutoSize = true;
            this.comparisonScoringRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonScoringRatingLabel.Location = new System.Drawing.Point(173, 396);
            this.comparisonScoringRatingLabel.Name = "comparisonScoringRatingLabel";
            this.comparisonScoringRatingLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonScoringRatingLabel.TabIndex = 134;
            this.comparisonScoringRatingLabel.Text = "text";
            // 
            // comparisonChallengeLabel
            // 
            this.comparisonChallengeLabel.AutoSize = true;
            this.comparisonChallengeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonChallengeLabel.Location = new System.Drawing.Point(173, 371);
            this.comparisonChallengeLabel.Name = "comparisonChallengeLabel";
            this.comparisonChallengeLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonChallengeLabel.TabIndex = 133;
            this.comparisonChallengeLabel.Text = "text";
            // 
            // comparisonPickupLabel
            // 
            this.comparisonPickupLabel.AutoSize = true;
            this.comparisonPickupLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonPickupLabel.Location = new System.Drawing.Point(173, 346);
            this.comparisonPickupLabel.Name = "comparisonPickupLabel";
            this.comparisonPickupLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonPickupLabel.TabIndex = 132;
            this.comparisonPickupLabel.Text = "text";
            // 
            // comparisonClimbLabel
            // 
            this.comparisonClimbLabel.AutoSize = true;
            this.comparisonClimbLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonClimbLabel.Location = new System.Drawing.Point(173, 321);
            this.comparisonClimbLabel.Name = "comparisonClimbLabel";
            this.comparisonClimbLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonClimbLabel.TabIndex = 131;
            this.comparisonClimbLabel.Text = "text";
            // 
            // comparisonTeleopHighGoalLabel
            // 
            this.comparisonTeleopHighGoalLabel.AutoSize = true;
            this.comparisonTeleopHighGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonTeleopHighGoalLabel.Location = new System.Drawing.Point(173, 296);
            this.comparisonTeleopHighGoalLabel.Name = "comparisonTeleopHighGoalLabel";
            this.comparisonTeleopHighGoalLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonTeleopHighGoalLabel.TabIndex = 130;
            this.comparisonTeleopHighGoalLabel.Text = "text";
            this.toolTip1.SetToolTip(this.comparisonTeleopHighGoalLabel, "Scoring/Total Attempts");
            // 
            // comparisonTeleopLowGoalLabel
            // 
            this.comparisonTeleopLowGoalLabel.AutoSize = true;
            this.comparisonTeleopLowGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonTeleopLowGoalLabel.Location = new System.Drawing.Point(173, 271);
            this.comparisonTeleopLowGoalLabel.Name = "comparisonTeleopLowGoalLabel";
            this.comparisonTeleopLowGoalLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonTeleopLowGoalLabel.TabIndex = 129;
            this.comparisonTeleopLowGoalLabel.Text = "text";
            this.toolTip1.SetToolTip(this.comparisonTeleopLowGoalLabel, "Scoring/Total Attempts");
            // 
            // comparisonAssistanceRatingLabel
            // 
            this.comparisonAssistanceRatingLabel.AutoSize = true;
            this.comparisonAssistanceRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAssistanceRatingLabel.Location = new System.Drawing.Point(703, 396);
            this.comparisonAssistanceRatingLabel.Name = "comparisonAssistanceRatingLabel";
            this.comparisonAssistanceRatingLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonAssistanceRatingLabel.TabIndex = 128;
            this.comparisonAssistanceRatingLabel.Text = "N/A";
            // 
            // comparisonAssistDrawbridgeLabel
            // 
            this.comparisonAssistDrawbridgeLabel.AutoSize = true;
            this.comparisonAssistDrawbridgeLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAssistDrawbridgeLabel.Location = new System.Drawing.Point(703, 371);
            this.comparisonAssistDrawbridgeLabel.Name = "comparisonAssistDrawbridgeLabel";
            this.comparisonAssistDrawbridgeLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonAssistDrawbridgeLabel.TabIndex = 127;
            this.comparisonAssistDrawbridgeLabel.Text = "N/A";
            // 
            // comparisonAssistSallyPortLabel
            // 
            this.comparisonAssistSallyPortLabel.AutoSize = true;
            this.comparisonAssistSallyPortLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAssistSallyPortLabel.Location = new System.Drawing.Point(703, 346);
            this.comparisonAssistSallyPortLabel.Name = "comparisonAssistSallyPortLabel";
            this.comparisonAssistSallyPortLabel.Size = new System.Drawing.Size(28, 15);
            this.comparisonAssistSallyPortLabel.TabIndex = 126;
            this.comparisonAssistSallyPortLabel.Text = "N/A";
            // 
            // comparisonRoundNumsLabel
            // 
            this.comparisonRoundNumsLabel.AutoSize = true;
            this.comparisonRoundNumsLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonRoundNumsLabel.Location = new System.Drawing.Point(166, 20);
            this.comparisonRoundNumsLabel.Name = "comparisonRoundNumsLabel";
            this.comparisonRoundNumsLabel.Size = new System.Drawing.Size(56, 15);
            this.comparisonRoundNumsLabel.TabIndex = 125;
            this.comparisonRoundNumsLabel.Text = "label27";
            // 
            // comparisonAutoRatingLabel
            // 
            this.comparisonAutoRatingLabel.AutoSize = true;
            this.comparisonAutoRatingLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAutoRatingLabel.Location = new System.Drawing.Point(173, 196);
            this.comparisonAutoRatingLabel.Name = "comparisonAutoRatingLabel";
            this.comparisonAutoRatingLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonAutoRatingLabel.TabIndex = 124;
            this.comparisonAutoRatingLabel.Text = "test";
            // 
            // comparisonAutoLowGoalLabel
            // 
            this.comparisonAutoLowGoalLabel.AutoSize = true;
            this.comparisonAutoLowGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAutoLowGoalLabel.Location = new System.Drawing.Point(173, 171);
            this.comparisonAutoLowGoalLabel.Name = "comparisonAutoLowGoalLabel";
            this.comparisonAutoLowGoalLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonAutoLowGoalLabel.TabIndex = 123;
            this.comparisonAutoLowGoalLabel.Text = "test";
            // 
            // comparisonAutoHighGoalLabel
            // 
            this.comparisonAutoHighGoalLabel.AutoSize = true;
            this.comparisonAutoHighGoalLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonAutoHighGoalLabel.Location = new System.Drawing.Point(173, 146);
            this.comparisonAutoHighGoalLabel.Name = "comparisonAutoHighGoalLabel";
            this.comparisonAutoHighGoalLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonAutoHighGoalLabel.TabIndex = 122;
            this.comparisonAutoHighGoalLabel.Text = "test";
            // 
            // comparisonDefCrossedLabel
            // 
            this.comparisonDefCrossedLabel.AutoSize = true;
            this.comparisonDefCrossedLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonDefCrossedLabel.Location = new System.Drawing.Point(173, 96);
            this.comparisonDefCrossedLabel.Name = "comparisonDefCrossedLabel";
            this.comparisonDefCrossedLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonDefCrossedLabel.TabIndex = 121;
            this.comparisonDefCrossedLabel.Text = "test";
            // 
            // comparisonDefReachedLabel
            // 
            this.comparisonDefReachedLabel.AutoSize = true;
            this.comparisonDefReachedLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comparisonDefReachedLabel.Location = new System.Drawing.Point(173, 71);
            this.comparisonDefReachedLabel.Name = "comparisonDefReachedLabel";
            this.comparisonDefReachedLabel.Size = new System.Drawing.Size(35, 15);
            this.comparisonDefReachedLabel.TabIndex = 120;
            this.comparisonDefReachedLabel.Text = "test";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(69, 395);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(99, 16);
            this.label43.TabIndex = 119;
            this.label43.Text = "Scoring Rating:";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(587, 270);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(106, 16);
            this.label44.TabIndex = 118;
            this.label44.Text = "Crossing Rating:";
            this.label44.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(40, 195);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(128, 16);
            this.label45.TabIndex = 117;
            this.label45.Text = "Autonomous Rating:";
            this.label45.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(570, 395);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(119, 16);
            this.label46.TabIndex = 116;
            this.label46.Text = "Assistance Rating:";
            this.label46.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(632, 70);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 16);
            this.label47.TabIndex = 115;
            this.label47.Text = "Low Bar:";
            this.label47.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(626, 95);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(65, 16);
            this.label48.TabIndex = 114;
            this.label48.Text = "Portcullis:";
            this.label48.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(586, 120);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(105, 16);
            this.label49.TabIndex = 113;
            this.label49.Text = "Cheval de Frise:";
            this.label49.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(650, 145);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 16);
            this.label50.TabIndex = 112;
            this.label50.Text = "Moat:";
            this.label50.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(647, 320);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(84, 16);
            this.label51.TabIndex = 111;
            this.label51.Text = "Assistance";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(571, 20);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(185, 16);
            this.label52.TabIndex = 110;
            this.label52.Text = "Teleop Defence Crossing";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(73, 245);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(115, 16);
            this.label53.TabIndex = 109;
            this.label53.Text = "Teleop Scoring";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(95, 45);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(93, 16);
            this.label54.TabIndex = 108;
            this.label54.Text = "Autonomous";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(96, 370);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(72, 16);
            this.label55.TabIndex = 107;
            this.label55.Text = "Challenge:";
            this.label55.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(122, 320);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(45, 16);
            this.label56.TabIndex = 106;
            this.label56.Text = "Climb:";
            this.label56.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(115, 345);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(52, 16);
            this.label57.TabIndex = 105;
            this.label57.Text = "Pickup:";
            this.label57.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(50, 145);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(118, 16);
            this.label58.TabIndex = 104;
            this.label58.Text = "Scored High Goal:";
            this.label58.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(54, 170);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(114, 16);
            this.label59.TabIndex = 103;
            this.label59.Text = "Scored Low Goal:";
            this.label59.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(52, 95);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(116, 16);
            this.label60.TabIndex = 102;
            this.label60.Text = "Crossed Defence:";
            this.label60.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(46, 70);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(121, 16);
            this.label61.TabIndex = 101;
            this.label61.Text = "Reached Defence:";
            this.label61.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(532, 370);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(161, 16);
            this.label62.TabIndex = 100;
            this.label62.Text = "Assisted with Drawbridge:";
            this.label62.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(545, 345);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(148, 16);
            this.label63.TabIndex = 99;
            this.label63.Text = "Assisted with Sally Port:";
            this.label63.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(43, 295);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(125, 16);
            this.label64.TabIndex = 98;
            this.label64.Text = "Teleop High Goals:";
            this.label64.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label64, "Scoring/Total Attempts");
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(46, 270);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(121, 16);
            this.label65.TabIndex = 97;
            this.label65.Text = "Teleop Low Goals:";
            this.label65.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.toolTip1.SetToolTip(this.label65, "Scoring/Total Attempts");
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(29, 20);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(123, 16);
            this.label66.TabIndex = 96;
            this.label66.Text = "Round Numbers:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.Summary);
            this.tabControl1.Controls.Add(this.Scoring);
            this.tabControl1.Controls.Add(this.Crossing);
            this.tabControl1.Controls.Add(this.Autonomous);
            this.tabControl1.Controls.Add(this.Assistance);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(25, 70);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(1260, 607);
            this.tabControl1.TabIndex = 3;
            // 
            // Scoring
            // 
            this.Scoring.Controls.Add(this.scoringListView);
            this.Scoring.Location = new System.Drawing.Point(4, 25);
            this.Scoring.Name = "Scoring";
            this.Scoring.Padding = new System.Windows.Forms.Padding(3);
            this.Scoring.Size = new System.Drawing.Size(1252, 578);
            this.Scoring.TabIndex = 1;
            this.Scoring.Text = "Scoring";
            this.Scoring.ToolTipText = "Sorts by Scoring Rating";
            this.Scoring.UseVisualStyleBackColor = true;
            // 
            // scoringListView
            // 
            this.scoringListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scoringListView.CheckBoxes = true;
            this.scoringListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scoringListView.Location = new System.Drawing.Point(6, 3);
            this.scoringListView.Name = "scoringListView";
            this.scoringListView.Size = new System.Drawing.Size(1240, 572);
            this.scoringListView.TabIndex = 1;
            this.scoringListView.UseCompatibleStateImageBehavior = false;
            this.scoringListView.View = System.Windows.Forms.View.Details;
            this.scoringListView.SelectedIndexChanged += new System.EventHandler(this.scoringListView_SelectedIndexChanged);
            this.scoringListView.TabIndexChanged += new System.EventHandler(this.RefreshButton_Click);
            // 
            // Crossing
            // 
            this.Crossing.Controls.Add(this.crossingListView);
            this.Crossing.Location = new System.Drawing.Point(4, 25);
            this.Crossing.Name = "Crossing";
            this.Crossing.Size = new System.Drawing.Size(1252, 578);
            this.Crossing.TabIndex = 2;
            this.Crossing.Text = "Crossing";
            this.Crossing.ToolTipText = "Sorts by Defence Crossing Rating";
            this.Crossing.UseVisualStyleBackColor = true;
            // 
            // crossingListView
            // 
            this.crossingListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.crossingListView.CheckBoxes = true;
            this.crossingListView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crossingListView.Location = new System.Drawing.Point(6, 3);
            this.crossingListView.Name = "crossingListView";
            this.crossingListView.Size = new System.Drawing.Size(1240, 572);
            this.crossingListView.TabIndex = 2;
            this.crossingListView.UseCompatibleStateImageBehavior = false;
            this.crossingListView.View = System.Windows.Forms.View.Details;
            this.crossingListView.SelectedIndexChanged += new System.EventHandler(this.crossingListView_SelectedIndexChanged);
            // 
            // Autonomous
            // 
            this.Autonomous.Controls.Add(this.autonomousListView);
            this.Autonomous.Location = new System.Drawing.Point(4, 25);
            this.Autonomous.Name = "Autonomous";
            this.Autonomous.Size = new System.Drawing.Size(1252, 578);
            this.Autonomous.TabIndex = 3;
            this.Autonomous.Text = "Autonomous";
            this.Autonomous.ToolTipText = "Sorts by Autonomous Rating";
            this.Autonomous.UseVisualStyleBackColor = true;
            // 
            // autonomousListView
            // 
            this.autonomousListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.autonomousListView.CheckBoxes = true;
            this.autonomousListView.Location = new System.Drawing.Point(6, 3);
            this.autonomousListView.Name = "autonomousListView";
            this.autonomousListView.Size = new System.Drawing.Size(1240, 572);
            this.autonomousListView.TabIndex = 3;
            this.autonomousListView.UseCompatibleStateImageBehavior = false;
            this.autonomousListView.View = System.Windows.Forms.View.Details;
            this.autonomousListView.SelectedIndexChanged += new System.EventHandler(this.autonomousListView_SelectedIndexChanged);
            // 
            // Assistance
            // 
            this.Assistance.Controls.Add(this.assistanceListView);
            this.Assistance.Location = new System.Drawing.Point(4, 25);
            this.Assistance.Name = "Assistance";
            this.Assistance.Size = new System.Drawing.Size(1252, 578);
            this.Assistance.TabIndex = 4;
            this.Assistance.Text = "Assistance";
            this.Assistance.ToolTipText = "Sorts by Assistance Rating";
            this.Assistance.UseVisualStyleBackColor = true;
            // 
            // assistanceListView
            // 
            this.assistanceListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assistanceListView.CheckBoxes = true;
            this.assistanceListView.Location = new System.Drawing.Point(6, 3);
            this.assistanceListView.Name = "assistanceListView";
            this.assistanceListView.Size = new System.Drawing.Size(1240, 572);
            this.assistanceListView.TabIndex = 3;
            this.assistanceListView.UseCompatibleStateImageBehavior = false;
            this.assistanceListView.View = System.Windows.Forms.View.Details;
            this.assistanceListView.SelectedIndexChanged += new System.EventHandler(this.assistanceListView_SelectedIndexChanged);
            // 
            // viewAllCheckBox
            // 
            this.viewAllCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.viewAllCheckBox.AutoSize = true;
            this.viewAllCheckBox.BackColor = System.Drawing.Color.Transparent;
            this.viewAllCheckBox.ForeColor = System.Drawing.Color.White;
            this.viewAllCheckBox.Location = new System.Drawing.Point(730, 48);
            this.viewAllCheckBox.Name = "viewAllCheckBox";
            this.viewAllCheckBox.Size = new System.Drawing.Size(63, 17);
            this.viewAllCheckBox.TabIndex = 7;
            this.viewAllCheckBox.Text = "View All";
            this.toolTip1.SetToolTip(this.viewAllCheckBox, "View all robots, including excluded.");
            this.viewAllCheckBox.UseVisualStyleBackColor = false;
            this.viewAllCheckBox.CheckedChanged += new System.EventHandler(this.RefreshButton_Click);
            // 
            // RefreshButton
            // 
            this.RefreshButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RefreshButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.RefreshButton.Location = new System.Drawing.Point(970, 42);
            this.RefreshButton.Name = "RefreshButton";
            this.RefreshButton.Size = new System.Drawing.Size(75, 23);
            this.RefreshButton.TabIndex = 8;
            this.RefreshButton.Text = "Refresh";
            this.toolTip1.SetToolTip(this.RefreshButton, "Refreshes robot data.");
            this.RefreshButton.UseVisualStyleBackColor = false;
            this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
            // 
            // InstructionsButton
            // 
            this.InstructionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.InstructionsButton.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.InstructionsButton.FlatAppearance.BorderSize = 3;
            this.InstructionsButton.Location = new System.Drawing.Point(1210, 42);
            this.InstructionsButton.Name = "InstructionsButton";
            this.InstructionsButton.Size = new System.Drawing.Size(75, 23);
            this.InstructionsButton.TabIndex = 16;
            this.InstructionsButton.Text = "Instructions";
            this.toolTip1.SetToolTip(this.InstructionsButton, "Instructions for using this form.");
            this.InstructionsButton.UseVisualStyleBackColor = false;
            this.InstructionsButton.Click += new System.EventHandler(this.InstructionsButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButton.BackColor = System.Drawing.Color.Silver;
            this.ClearButton.Location = new System.Drawing.Point(850, 42);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 82;
            this.ClearButton.Text = "Clear";
            this.toolTip1.SetToolTip(this.ClearButton, "Clears all robot data.");
            this.ClearButton.UseVisualStyleBackColor = false;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // ImportButton
            // 
            this.ImportButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ImportButton.BackColor = System.Drawing.Color.Chartreuse;
            this.ImportButton.Location = new System.Drawing.Point(1090, 42);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(75, 23);
            this.ImportButton.TabIndex = 9;
            this.ImportButton.Text = "Import";
            this.toolTip1.SetToolTip(this.ImportButton, "Import robot data csv");
            this.ImportButton.UseVisualStyleBackColor = false;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // instructionLabel3
            // 
            this.instructionLabel3.AutoSize = true;
            this.instructionLabel3.BackColor = System.Drawing.Color.Transparent;
            this.instructionLabel3.ForeColor = System.Drawing.Color.White;
            this.instructionLabel3.Location = new System.Drawing.Point(191, 37);
            this.instructionLabel3.Name = "instructionLabel3";
            this.instructionLabel3.Size = new System.Drawing.Size(404, 13);
            this.instructionLabel3.TabIndex = 15;
            this.instructionLabel3.Text = "specific information on the robots. The tabs are sorted by their respective categ" +
    "ories.";
            this.instructionLabel3.Visible = false;
            // 
            // instructionLabel2
            // 
            this.instructionLabel2.AutoSize = true;
            this.instructionLabel2.BackColor = System.Drawing.Color.Transparent;
            this.instructionLabel2.ForeColor = System.Drawing.Color.White;
            this.instructionLabel2.Location = new System.Drawing.Point(188, 22);
            this.instructionLabel2.Name = "instructionLabel2";
            this.instructionLabel2.Size = new System.Drawing.Size(674, 13);
            this.instructionLabel2.TabIndex = 14;
            this.instructionLabel2.Text = "To see detailed information on a particul" +
    "ar robot, click on the robot\'s number. Change tabs to see more";
            this.instructionLabel2.Visible = false;
            // 
            // instructionLabel1
            // 
            this.instructionLabel1.AutoSize = true;
            this.instructionLabel1.BackColor = System.Drawing.Color.Transparent;
            this.instructionLabel1.ForeColor = System.Drawing.Color.White;
            this.instructionLabel1.Location = new System.Drawing.Point(188, 9);
            this.instructionLabel1.Name = "instructionLabel1";
            this.instructionLabel1.Size = new System.Drawing.Size(328, 13);
            this.instructionLabel1.TabIndex = 13;
            this.instructionLabel1.Text = "To exclude a robot from the report, check the box then click refresh. To view excluded data, select view all.";
            this.instructionLabel1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1314, 711);
            this.Controls.Add(this.robotDetail);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.RefreshButton);
            this.Controls.Add(this.viewAllCheckBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.ImportButton);
            this.Controls.Add(this.instructionLabel3);
            this.Controls.Add(this.instructionLabel2);
            this.Controls.Add(this.instructionLabel1);
            this.Controls.Add(this.InstructionsButton);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MaximumSize = new System.Drawing.Size(1500, 900);
            this.MinimumSize = new System.Drawing.Size(1320, 700);
            this.Name = "Form1";
            this.Text = "  Reporting";
            this.Summary.ResumeLayout(false);
            this.robotDetail.ResumeLayout(false);
            this.robotDetail.PerformLayout();
            this.robotDetailTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.Scoring.ResumeLayout(false);
            this.Crossing.ResumeLayout(false);
            this.Autonomous.ResumeLayout(false);
            this.Assistance.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Summary;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Scoring;
        private System.Windows.Forms.TabPage Crossing;
        private System.Windows.Forms.TabPage Autonomous;
        private System.Windows.Forms.TabPage Assistance;
        private System.Windows.Forms.ListView summaryListView;
        private System.Windows.Forms.ListView scoringListView;
        private System.Windows.Forms.ListView crossingListView;
        private System.Windows.Forms.ListView autonomousListView;
        private System.Windows.Forms.ListView assistanceListView;
        private System.Windows.Forms.Panel robotDetail;
        private System.Windows.Forms.Button ExitPanelButton;
        private System.Windows.Forms.CheckBox viewAllCheckBox;
        private System.Windows.Forms.Button RefreshButton;
        private System.Windows.Forms.TabControl robotDetailTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label crossingRatingLabel;
        private System.Windows.Forms.Label defenceCrossing1Label;
        private System.Windows.Forms.Label defenceCrossing2Label;
        private System.Windows.Forms.Label defenceCrossing3Label;
        private System.Windows.Forms.Label defenceCrossing4Label;
        private System.Windows.Forms.Label scoringRatingLabel;
        private System.Windows.Forms.Label teleopChallengeLabel;
        private System.Windows.Forms.Label teleopPickupLabel;
        private System.Windows.Forms.Label teleopClimbLabel;
        private System.Windows.Forms.Label teleopHighGoalLabel;
        private System.Windows.Forms.Label teleopLowGoalLabel;
        private System.Windows.Forms.Label assistanceRatingLabel;
        private System.Windows.Forms.Label assistedDrawbridgeLabel;
        private System.Windows.Forms.Label assistedSallyPortLabel;
        private System.Windows.Forms.Label robotRoundLabel;
        private System.Windows.Forms.Label autoRatingLabel;
        private System.Windows.Forms.Label scoredLowLabel;
        private System.Windows.Forms.Label scoredHighLabel;
        private System.Windows.Forms.Label crossedDefenceLabel;
        private System.Windows.Forms.Label reachedDefenceLabel;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label defence1Label;
        private System.Windows.Forms.Label defence2Label;
        private System.Windows.Forms.Label defence3Label;
        private System.Windows.Forms.Label defence4Label;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label robotNumberLabel;
        private System.Windows.Forms.Label comparisonCrossingRatingLabel;
        private System.Windows.Forms.Label comparisonLowBarLabel;
        private System.Windows.Forms.Label comparisonPortcullisLabel;
        private System.Windows.Forms.Label comparisonChevaldeFriseLabel;
        private System.Windows.Forms.Label comparisonMoatLabel;
        private System.Windows.Forms.Label comparisonScoringRatingLabel;
        private System.Windows.Forms.Label comparisonChallengeLabel;
        private System.Windows.Forms.Label comparisonPickupLabel;
        private System.Windows.Forms.Label comparisonClimbLabel;
        private System.Windows.Forms.Label comparisonTeleopHighGoalLabel;
        private System.Windows.Forms.Label comparisonTeleopLowGoalLabel;
        private System.Windows.Forms.Label comparisonAssistanceRatingLabel;
        private System.Windows.Forms.Label comparisonAssistDrawbridgeLabel;
        private System.Windows.Forms.Label comparisonAssistSallyPortLabel;
        private System.Windows.Forms.Label comparisonRoundNumsLabel;
        private System.Windows.Forms.Label comparisonAutoRatingLabel;
        private System.Windows.Forms.Label comparisonAutoLowGoalLabel;
        private System.Windows.Forms.Label comparisonAutoHighGoalLabel;
        private System.Windows.Forms.Label comparisonDefCrossedLabel;
        private System.Windows.Forms.Label comparisonDefReachedLabel;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label comparisonRampartsLabel;
        private System.Windows.Forms.Label comparisonDrawbridgeLabel;
        private System.Windows.Forms.Label comparisonSallyPortLabel;
        private System.Windows.Forms.Label comparisonRockWallLabel;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label comparisonRoughTerrainLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button ImportButton;
        private System.Windows.Forms.Label instructionLabel3;
        private System.Windows.Forms.Label instructionLabel2;
        private System.Windows.Forms.Label instructionLabel1;
        private System.Windows.Forms.Button InstructionsButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Label defenceCrossing5Label;
        private System.Windows.Forms.Label defence5Label;
        private System.Windows.Forms.Label allDefencesCrossedLabel;
        private System.Windows.Forms.Label label21;
    }
}

